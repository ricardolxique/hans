jQuery(document).ready(function() {
    jQuery("#encuesta_id").change(function() {
        var encuesta    =   jQuery(this).val();
        jQuery.ajax({
            type: "POST",
            url: "analisis/loadQuestions",
            data: {encuesta : encuesta},
            dataType: 'html',
            success: function (data) {
                jQuery("#div_lista_preguntas").slideDown();
                jQuery("#lista_preguntas").empty().append(data);
            }
        });
    });
    jQuery("body").on("change", ".list-questions-analisis", function() {

        var pregunta    =   jQuery(this).val();
        jQuery.ajax({
            type: "POST",
            url: "analisis/loadDataInfo",
            data: {pregunta : pregunta},
            dataType: 'json',
            success: function (data) {
                if (data.result == true) {
                    var totales = data.total;
                    var dataSum = 0;
                    for (var i=0;i < totales.length;i++) {
                        dataSum += totales[i]
                    }
                    jQuery("#myChart").show();
                    jQuery("#myText").hide();
                    jQuery('#container').highcharts({
                        chart: {
                            type: 'bar'
                        },
                        credits: {
                            enabled: false
                        },
                        title: {
                            text: data.pregunta
                        },
                        legend: {
                            enabled: false

                        },
                        plotOptions: {
                            series: {
                                colorByPoint: true,
                                borderWidth: 0,
                                animation: {
                                    duration: 3000
                                },
                                dataLabels: {
                                    enabled: true,
                                    formatter: function () {
                                        var pcnt = (this.y / dataSum) * 100;
                                        return Highcharts.numberFormat(pcnt) + '%';
                                    }
                                }
                            },
                            column: {
                                stacking: 'percent'
                            }
                        },
                        series: [{
                            name: 'Respuestas',
                            data: data.total
                        }],
                        xAxis: {
                            categories: data.respuestas,
                            labels: {
                                enabled: true
                            }
                        },
                        yAxis: {
                            title: {
                                text: null
                            },
                            labels: {
                                enabled: false
                            }
                        }
                    });
                } else {
                    jQuery("#myChart").hide();
                    jQuery("#myText").show();
                }
            }
        });
    });
});