var type    = 1;
var datos   = "";
initialize(type, datos);

var MY_MAPTYPE_ID = 'custom_style';
var map;
var popup;

function initialize(type, datos) {

    if (datos != "") {
        var e = document.getElementById("encuesta_id");
        var encuesta = e.options[e.selectedIndex].value;
    }

    if ((screen.width<=1400)) {
        var var_location = new google.maps.LatLng(23.767845, -100.803133);
    } else {
        var var_location = new google.maps.LatLng(25.413900, -101.250623);
    }

    var minZoomLevel = 3;

    var mapOptions = {
        center: var_location,
        zoom:5,
        minZoom: 5,
        streetViewControl: true,
        mapTypeControl : false
    };
    map = new google.maps.Map(document.getElementById("map"),mapOptions);

    var styles = [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"administrative.land_parcel","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"landscape.natural","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"on"},{"color":"#052366"},{"saturation":"-70"},{"lightness":"85"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"simplified"},{"lightness":"-53"},{"weight":"1.00"},{"gamma":"0.98"}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"visibility":"simplified"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry","stylers":[{"saturation":"-18"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#57677a"},{"visibility":"on"}]}];

    map.setOptions({styles: styles});

    var styledMapOptions = {
        name: 'Custom Style'
    };

    var customMapType = new google.maps.StyledMapType(styledMapOptions);
    map.mapTypes.set(MY_MAPTYPE_ID, customMapType);


    var markersTodos = [];

    if (datos != "" && encuesta != "") {
        loadFiltros(encuesta);
        document.getElementById("filtros_mapa").style.display='block';

        if (type == "2") {
            var url = "mapa/loadFiltros";
        } else {
            var url = "mapa/getMarkers";
            var datos = "encuesta=" + encuesta;

        }
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                var xml = xhttp.responseXML;
                console.log(xml);
                markers = xml.getElementsByTagName("marker");
                for (var i = 0; i < markers.length; i++) {
                    var id = markers[i].getAttribute("id");
                    var latitud = markers[i].getAttribute("lat");
                    var longitud = markers[i].getAttribute("lng");
                    var latLng = new google.maps.LatLng(
                        parseFloat(markers[i].getAttribute("lat")),
                        parseFloat(markers[i].getAttribute("lng"))
                    );

                    if (type == "2") {
                        var color = markers[i].getAttribute("color");
                        var pinImage = new google.maps.MarkerImage("http://www.googlemapsmarkers.com/v1/''/" + color + "/"+color+"/FFFFFF/");
                        var marker = new google.maps.Marker({
                            map: map,
                            position: latLng,
                            icon: pinImage,
                            title: id
                        });
                    } else {
                        var marker = new google.maps.Marker({
                            map: map,
                            position: latLng,
                            title: id
                        });
                        limpiarLegend();
                    }
                    markersTodos.push(marker);
                    bindInfoWindow32(marker);

                    latLng = new google.maps.LatLng(latitud, longitud);
                }
                if (type == "2") {
                    loadInfoLegend();
                }
            }
        };
        xhttp.open("POST", url, true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send(datos);
    } else {
        document.getElementById("filtros_mapa").style.display='none';
    }
    google.maps.event.addListener(map, 'zoom_changed', function() {
        if (map.getZoom() < minZoomLevel) map.setZoom(minZoomLevel);
    });
}

jQuery("#apply_filters").click(function(ev) {
    if (jQuery('input[name="filtros[]"]:checked').serialize() != "") {
        var encuesta = jQuery("#encuesta_id").val();
        var datos = jQuery("#mapa_filtros_form").serialize() + '&' + jQuery.param({'encuesta': encuesta});
        var type = "2";
        initialize(type, datos);
    }
});

function loadFiltros(encuesta) {
    jQuery(document).ready(function() {
        jQuery.ajax({
            url: urlBase+"/mapa/loadIndicadores",
            type: 'POST',
            dataType: 'html',
            data: {encuesta : encuesta},
            success: function (result) {
                jQuery("#load_indicadores").empty().append(result);
                jQuery('#myModal').modal('hide');
            }
        });
    });
}

function bindInfoWindow32(marker) {
    google.maps.event.addListener(marker, 'click', function() {
        var id = this.getTitle();
        loadData(id);
    });
}
function loadInfoLegend() {
    jQuery.post( urlBase+"/mapa/loadInfoLegend", function( data ) {
        jQuery("#legend_mapa").empty().append(data);
        jQuery("#legend_mapa").slideDown();
    });
}
function limpiarLegend() {
    jQuery("#legend_mapa").empty();
    jQuery("#legend_mapa").slideUp();
}

function loadData(id) {
    if (id != "") {
        jQuery.post( urlBase+"/mapa/getInfoEncuestas", { id : id }, function( data ) {
            jQuery("#content-encuesta").empty().append(data);
        });
    }
    jQuery('#myModalData').modal('show');
}
function openModal2(titulo) {
    $.post( urlBase+"/mapa/getInfoEncuestado", {titulo:titulo}, function( data ) {
        $("#modal_body_info_encuestado").empty().append(data);
        $('#myModalEncuestado').modal('show');
    });
}