jQuery(document).ready(function($)
{
    $("body").on("click", ".delete-modulo", function(event) {
        event.preventDefault();
        swal({
            title: "Esta seguro?",
            text: "No podras recuperar el módulo eliminado",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar!",
            cancelButtonText: "No, cancelar!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                var inputData   = $('#formDeleteModulo').serialize();
                var dataId      = $('#btnDeleteModulo').attr('data-id');

                $.ajax({
                    url: urlBase+'/modulos/'+dataId,
                    type: 'DELETE',
                    data: inputData,
                    success: function( msg ) {
                        if ( msg.status === 'success' ) {
                            swal("Eliminado!", "El módulo ha sido borrado.", "success");
                            setInterval(function() {
                                window.location.replace(urlBase+"/modulos");
                            }, 500);
                        }
                    },
                    error: function( data ) {
                        if ( data.status === 422 ) {
                            swal("Cancelado", "El módulo no se ha eliminado", "error");
                        }
                    }
                });
                return false;
            } else {
                swal("Cancelado", "El módulo no se eliminara", "error");
            }
        });
    });
});