jQuery(document).ready(function($)
{
    $("body").on("click", ".delete-user", function(event) {
        event.preventDefault();
        swal({
            title: "Esta seguro?",
            text: "No podras recuperar al usuario eliminado",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar!",
            cancelButtonText: "No, cancelar!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                var inputData   = $('#formDeleteUser').serialize();
                var dataId      = $('#btnDeleteUser').attr('data-id');

                $.ajax({
                    url: urlBase+'/usuarios/'+dataId,
                    type: 'DELETE',
                    data: inputData,
                    success: function( msg ) {
                        if ( msg.status === 'success' ) {
                            swal("Eliminado!", "El usuario ha sido borrado.", "success");
                            setInterval(function() {
                                window.location.replace(urlBase+"/usuarios");
                            }, 500);
                        }
                    },
                    error: function( data ) {
                        if ( data.status === 422 ) {
                            swal("Cancelado", "El usuario no se ha eliminado", "error");
                        }
                    }
                });
                return false;

            } else {
                swal("Cancelado", "El usuario no se eliminara", "error");
            }
        });
    });
});