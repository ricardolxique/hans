jQuery(document).ready(function($) {
    $('#table-encuestas').dataTable( {
        "searching": false,
        "bInfo": false
    } );
    $("#table-encuestas_paginate").parent(".col-right").removeClass("col-xs-6").addClass("col-xs-12");
    $("body").on("click", ".view-encuesta", function () {
        var idEncuesta  =   $(this).attr("data-id");
        var indice      =   $(this).attr("data-indice");
        $(".td-tabla").removeClass("td-selected");
        jQuery.ajax({
            type: "POST",
            url: urlBase+"/encuestasanalisis/encuesta",
            data: {idEncuesta : idEncuesta},
            dataType: 'html',
            success: function (data) {
                $("#div-encuesta").empty().append(data);
                $("#td-"+indice).addClass("td-selected");
            }
        });
    });
    $("body").on("click", ".view-galeria", function (ev) {
        ev.preventDefault();
        var idEncuesta  =   $(this).attr("data-id");
        var indice      =   $(this).attr("data-indice");
        $(".td-tabla").removeClass("td-selected");
        jQuery.ajax({
            type: "POST",
            url: urlBase+"/encuestasanalisis/galeria",
            data: {idEncuesta : idEncuesta},
            dataType: 'html',
            success: function (data) {
                $("#div-galeria").empty().append(data);
                $("#modal-galeria").modal("show");
                $("#td-"+indice).addClass("td-selected");
            }
        });
    });
});