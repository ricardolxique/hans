jQuery(document).ready(function($) {
    jQuery("body").on("click", ".aprueba-encuesta", function() {
        var idEncuesta  =   $(this).attr("data-id");
        var div         =   $(this).attr("data-div");
        var estado      =   1;
        procesaEstadoEncuesta(idEncuesta, estado, div);
    });
    jQuery("body").on("click", ".rechaza-encuesta", function() {
        var idEncuesta  =   $(this).attr("data-id");
        var div         =   $(this).attr("data-div");
        var estado      =   0;
        procesaEstadoEncuesta(idEncuesta, estado, div);
    });
    jQuery("body").on("click", ".validate-galery", function() {
        var idEncuesta  =   $(this).attr("data-id");
        jQuery.ajax({
            type: "POST",
            url: "loadFoto",
            data: { idEncuesta : idEncuesta },
            dataType: 'html',
            success: function (dataHtml) {
                jQuery("#modal-valida-fotos").modal("show");
                jQuery("#grid-fotos-validacion").empty().append(dataHtml);
            }
        });
    });
    $("body").on("click", ".rechaza-foto", function(event) {
        event.preventDefault();
        var id      =   $(this).attr("data-id");
        var div     =   $(this).attr("data-div");
        var estado  =   0;
        procesaValidacionGaleria(id, estado, div);
    });
    $("body").on("click", ".aprueba-foto", function(event) {
        event.preventDefault();
        var id      =   $(this).attr("data-id");
        var div     =   $(this).attr("data-div");
        var estado  =   1;
        procesaValidacionGaleria(id, estado, div);
    });
});
function procesaValidacionGaleria(idFoto, estado, div) {
    var parametros    =   {
        idFoto : idFoto,
        estado  : estado
    };
    jQuery.ajax({
        type: "POST",
        url: urlBase+"/revision/revisaFoto",
        data: parametros,
        dataType: 'json',
        success: function (dataJson) {
            if (dataJson.status == true) {
                toastr.success(dataJson.msg, "Bien!");
                jQuery("#aprob-"+div).fadeOut(3000).remove();
            } else {
                toastr.error(dataJson.msg, "Oops!");
            }
        }
    });
}
function procesaEstadoEncuesta(idEncuesta, estado, div) {
    var parametros = {
        idEncuesta: idEncuesta,
        estado: estado
    };
    jQuery.ajax({
        type: "POST",
        url: "revisaEncuesta",
        data: parametros,
        dataType: 'json',
        success: function (dataJson) {
            if (dataJson.status == true) {
                toastr.success(dataJson.msg, "Bien!");
                jQuery("#encuesta-" + div).fadeOut(5000).remove();
                cargaEncuesta(div);
                toastr.clear();
            } else {
                toastr.error(dataJson.msg, "Oops!");
            }
        }
    });
}
function cargaEncuesta(div) {
    jQuery.ajax({
        type: "POST",
        url: "loadQuestion",
        data: { div:div },
        dataType: 'html',
        success: function (dataHtml) {
            jQuery("#div-lista-encuestas").append(dataHtml);
        }
    });
}
