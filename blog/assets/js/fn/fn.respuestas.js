/**
 * Created by lemon on 24/04/16.
 */
jQuery(document).ready(function($)  {
    $("#active_order").bootstrapSwitch({
        'onText': "SI",
        'offText': "NO",
        'state': false

    });
    $('#active_order').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#list-1").addClass("dd");
            $("#list-10").hide();
            $("#list-1").show();
        } else {
            $("#list_preguntas").empty();
            $("#list-1").removeClass("dd");
            $("#list-1").nestable("destroy");
            $("#list-1").hide();
            $("#list-10").show();

            var id_encuesta = $("#encuesta_pregunta_id_value").attr("data-id");
            loadAnswers(id_encuesta);
        }
    });
    $('#list-1').nestable().on('change', updateOutput );

    function updateOutput() {
        var json_text = $('#list-1').nestable('serialize');
        $.ajax({
            type: 'POST',
            url: urlBase+'/respuestas/order',
            data: { json : JSON.stringify(json_text)},
            dataType: "json",
            success: function(data) {
                console.log(data);
            }
        });
    }

    $("body").on("click", "#agregar_respuesta", function(ev) {
        var frm = $('#respuestas_form');
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            dataType: 'json',
            success: function (data) {
                if (data.success == true) {
                    $("#errors").hide();
                    $(frm)[0].reset();
                    swal("Muy bien!", "Respuesta cargada correctamente!", "success")
                    loadAnswers(data.pregunta);
                } else {
                    swal("Oops!", "Al parecer hemos tenido un inconveniente, intentelo nuevamente.", "success");
                }
            },
            error :function( data ) {
                var errors = '';
                for(datos in data.responseJSON){
                    errors += data.responseJSON[datos] + '<br>';
                }
                $('#errors').show().html(errors);
            }
        });
        ev.preventDefault();
    });

    $("body").on("click", ".edit-answer", function(ev) {
        var id = $(this).attr("data-id");
        $.ajax({
            type: "GET",
            url: urlBase+'/respuestas/'+id+'/edit',
            dataType: 'html',
            success: function (data) {
                $("#contentEditAnswer").empty().append(data);
                $("#modalEditAnswer").modal("show");
            }
        });
        ev.preventDefault();
    });

    $("body").on("click", "#update_respuesta", function(ev) {
        var frm = $('#respuesta_update_form');
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            dataType: 'json',
            success: function (data) {
                if (data.success == true) {
                    $("#errors").hide();
                    swal("Muy bien!", "Respuesta actualizada correctamente!", "success");
                    $("#contentEditAnswer").empty();
                    $("#modalEditAnswer").modal("hide");
                    loadAnswers(data.pregunta);

                } else {
                    swal("Oops!", "Al parecer hemos tenido un inconveniente, intentelo nuevamente.", "success");
                }
            },
            error :function( data ) {
                var errors = '';
                for(datos in data.responseJSON){
                    errors += data.responseJSON[datos] + '<br>';
                }
                $('#errors_update').show().html(errors);
            }
        });
        ev.preventDefault();
    });
    $("body").on("click", ".delete-answer", function(event) {
        var dataId      = $(this).attr('data-id');
        event.preventDefault();
        swal({
            title: "Esta seguro?",
            text: "No podras recuperar la respuesta eliminada",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar!",
            cancelButtonText: "No, cancelar!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url: urlBase+'/respuestas/'+dataId,
                    type: 'DELETE',
                    data: dataId,
                    success: function( data ) {
                        if (data.success == true) {
                            swal("Eliminado!", "La respuesta ha sido eliminada.", "success");
                            loadAnswers(data.pregunta);
                        }
                    },
                    error: function( data ) {
                        if ( data.status === 422 ) {
                            swal("Cancelado", "La respuesta no se ha eliminado", "error");
                        }
                    }
                });
                return false;

            } else {
                swal("Cancelado", "La respuesta no se eliminara", "error");
            }
        });
    });
    $("body").on("click", ".back-preguntas", function() {
        var dd    =   $(this).attr("data-id");
        $.ajax({
            type: "POST",
            url: urlBase+'/respuestas/backPreguntas',
            data: {dd : dd},
            dataType: 'json',
            success: function(data) {
                if (data.result) {
                    window.location.href = urlBase+'/preguntas';
                }
            }
        });
    });
    function loadAnswers(id_pregunta) {
        $.ajax({
            type: "GET",
            url: urlBase+'/respuestas/'+id_pregunta,
            dataType: 'html',
            success: function(data) {
                $("#list_respuesta").empty().append(data);
                $('#list-1').nestable().on('change', updateOutput );
            }
        });
    }
});