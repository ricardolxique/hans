jQuery(document).ready(function($)  {
    $("body").on("change", "#encuesta_id", function() {
        var id_encuesta = $(this).val();
        loadCombo(id_encuesta);
    });
    $("body").on("click", "#agregar_pregunta", function(ev) {
        var frm         =   $('#indicadores_form');
        var coo         =   $('#pregunta_id').val();

        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            dataType: 'json',
            success: function (data) {
                if (data.success == true) {
                    $("#pregunta_id option[value='"+coo+"']").remove();
                    $("#indicadores_form")[0].reset();
                    swal("Muy bien!", "Pregunta cargada correctamente!", "success")
                    loadQuestions(data.encuesta);
                } else {
                    
                    swal("Oops!", "Al parecer has excedido el máximo de indicadores permitidos, elimina alguna e intentalo nuevamente.", "error");
                }
            },
            error :function( data ) {
                var errors = '';
                for(datos in data.responseJSON){
                    errors += data.responseJSON[datos] + '<br>';
                }
                $('#errors').show().html(errors);
            }
        });
        ev.preventDefault();
    });
    $("body").on("click", ".delete-quote", function(event) {
        var dataId      = $(this).attr('data-id');
        event.preventDefault();
        swal({
            title: "Esta seguro?",
            text: "Desea eliminar a esta pregunta?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar!",
            cancelButtonText: "No, cancelar!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url: urlBase+'/indicadores/'+dataId,
                    type: 'DELETE',
                    data: dataId,
                    success: function( data ) {
                        if (data.success == true) {
                            swal("Eliminada!", "La pregunta ha sido eliminado.", "success");
                            loadQuestions(data.encuesta);
                            loadCombo(data.encuesta);
                        }
                    },
                    error: function( data ) {
                        if ( data.status === 422 ) {
                            swal("Cancelado", "La pregunta no se ha eliminado", "error");
                        }
                    }
                });
                return false;
            } else {
                swal("Cancelado", "La pregunta no se eliminara", "error");
            }
        });
    });
    function loadCombo(id_encuesta) {
        $.ajax({
            type: "GET",
            url: urlBase+'/indicadores/'+id_encuesta,
            dataType: 'html',
            success: function(data) {
                $("#agrega_pregunta").empty().append(data);
                loadQuestions(id_encuesta);
            }
        });
    }
    function loadQuestions(id_encuesta) {
        $.ajax({
            type: "GET",
            url: urlBase+'/indicadores/loadList/'+id_encuesta,
            dataType: 'html',
            success: function(data) {
                $("#list_preguntas").empty().append(data);
            }
        });
    }
});