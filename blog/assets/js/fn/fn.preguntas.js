jQuery(document).ready(function($)  {
    $("body").on("change", "#encuesta_id", function() {
        var id_encuesta = $(this).val();
        loadPreguntas(id_encuesta);
    });

    $("body").on("click", "#agregar_pregunta", function(ev) {
        var frm = $('#pregunta_form');
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            dataType: 'json',
            success: function (data) {
                if (data.success == true) {
                    $("#pregunta_form")[0].reset();
                    $("#errors").hide();
                    swal("Muy bien!", "Pregunta cargada correctamente!", "success")
                    loadQuestions(data.encuesta);
                } else {
                    swal("Oops!", "Al parecer hemos tenido un inconveniente, intentelo nuevamente.", "success");
                }
            },
            error :function( data ) {
                var errors = '';
                for(datos in data.responseJSON){
                    errors += data.responseJSON[datos] + '<br>';
                }
                $('#errors').show().html(errors);
            }
        });
        ev.preventDefault();
    });

    $("body").on("click", ".edit-quote", function(ev) {
        var id = $(this).attr("data-id");
        $.ajax({
            type: "GET",
            url: urlBase+'/preguntas/'+id+'/edit',
            dataType: 'html',
            success: function (data) {
                $("#contentEditQuote").empty().append(data);
                $("#modalEditQuote").modal("show");
            }
        });
        ev.preventDefault();
    });

    $("body").on("click", "#update_pregunta", function(ev) {
        var frm = $('#pregunta_update_form');
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            dataType: 'json',
            success: function (data) {
                if (data.success == true) {
                    $("#errors").hide();
                    swal("Muy bien!", "Pregunta actualizada correctamente!", "success");
                    $("#contentEditQuote").empty();
                    $("#modalEditQuote").modal("hide");
                    loadQuestions(data.encuesta);

                } else {
                    swal("Oops!", "Al parecer hemos tenido un inconveniente, intentelo nuevamente.", "success");
                }
            },
            error :function( data ) {
                var errors = '';
                for(datos in data.responseJSON){
                    errors += data.responseJSON[datos] + '<br>';
                }
                $('#errors_update').show().html(errors);
            }
        });
        ev.preventDefault();
    });
    $("body").on("click", ".delete-quote", function(event) {
        var dataId      = $(this).attr('data-id');
        event.preventDefault();
        swal({
            title: "Esta seguro?",
            text: "No podras recuperar la pregunta eliminado",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar!",
            cancelButtonText: "No, cancelar!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                console.log(dataId);
                $.ajax({
                    url: urlBase+'/preguntas/'+dataId,
                    type: 'DELETE',
                    data: dataId,
                    success: function( data ) {
                        if (data.success == true) {
                            swal("Eliminado!", "La pregunta ha sido eliminado.", "success");
                            loadQuestions(data.encuesta);
                        }
                    },
                    error: function( data ) {
                        if ( data.status === 422 ) {
                            swal("Cancelado", "La pregunta no se ha eliminado", "error");
                        }
                    }
                });
                return false;

            } else {
                swal("Cancelado", "La pregunta no se eliminara", "error");
            }
        });
    });
    function loadQuestions(id_encuesta) {
        $.ajax({
            type: "GET",
            url: urlBase+'/preguntas/loadList/'+id_encuesta,
            dataType: 'html',
            success: function(data) {
                $("#list_preguntas").empty().append(data);
                $("#active_order").bootstrapSwitch({
                    'onText': "SI",
                    'offText': "NO",
                    'state': false

                });
                $('#list-1').nestable().on('change', updateOutput );

                $('#active_order').on('switchChange.bootstrapSwitch', function (event, state) {
                    if (state) {
                        $("#list-1").addClass("dd");
                        $("#list-10").hide();
                        $("#list-1").show();
                    } else {
                        $("#list_preguntas").empty();
                        $("#list-1").removeClass("dd");
                        $("#list-1").nestable("destroy");
                        $("#list-1").hide();
                        $("#list-10").show();
                        loadQuestions(id_encuesta);
                    }
                });

            }
        });
        var updateOutput = function(e) {
            var json_text = $('#list-1').nestable('serialize');
            $.ajax({
                type: 'POST',
                url: urlBase+'/preguntas/order',
                data: { json : JSON.stringify(json_text)},
                dataType: "json",
                success: function(data) {
                    console.log(data);
                }
            });
        };
    }
    function loadPreguntas(encuestaId) {
        $.ajax({
            type: "GET",
            url: urlBase+'/preguntas/'+encuestaId,
            dataType: 'html',
            success: function(data) {
                $("#crea_preguntas").empty().append(data);
                loadQuestions(encuestaId);
            }
        });
    }
});