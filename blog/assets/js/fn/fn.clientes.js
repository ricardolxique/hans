jQuery(document).ready(function($)
{
    $("body").on("click", ".delete-client", function(event) {
        event.preventDefault();
        swal({
            title: "Esta seguro?",
            text: "No podras recuperar el cliente eliminado",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar!",
            cancelButtonText: "No, cancelar!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                var inputData   = $('#formDeleteClient').serialize();
                var dataId      = $('#btnDeleteClient').attr('data-id');

                $.ajax({
                    url: urlBase+'/clientes/'+dataId,
                    type: 'DELETE',
                    data: inputData,
                    success: function( msg ) {
                        if ( msg.status === 'success' ) {
                            swal("Eliminado!", "El cliente ha sido borrado.", "success");
                            setInterval(function() {
                                window.location.replace(urlBase+"/clientes");
                            }, 500);
                        }
                    },
                    error: function( data ) {
                        if ( data.status === 422 ) {
                            swal("Cancelado", "El cliente no se ha eliminado", "error");
                        }
                    }
                });
                return false;

            } else {
                swal("Cancelado", "El cliente no se eliminara", "error");
            }
        });
    });
});