jQuery(document).ready(function($)  {
    $("body").on("click", "#agregar_operador", function(ev) {
        var frm         =   $('#operacion_form');
        var coo         =   $('#operacion_coordinador_id').val();
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            dataType: 'json',
            success: function (data) {
                if (data.success == true) {
                    $("#usuario_id option[value='"+coo+"']").remove();
                    $(frm)[0].reset();
                    swal("Muy bien!", "Usuario cargado correctamente!", "success");
                    loadUsuarios();
                } else {
                    swal("Oops!", "Al parecer hemos tenido un inconveniente, intentelo nuevamente.", "success");
                }
            },
            error :function( data ) {
                var errors = '';
                for(datos in data.responseJSON){
                    errors += data.responseJSON[datos] + '<br>';
                }
                $('#errors').show().html(errors);
            }
        });
        ev.preventDefault();
    });

    $("body").on("click", ".edit_numero_encuestas", function(ev) {
        var id = $(this).attr("data-id");
        $.ajax({
            type: "GET",
            url: urlBase+'/operacion/'+id+'/edit',
            dataType: 'html',
            success: function (data) {
                $("#contentEditUsuario").empty().append(data);
                $("#modalEditUsuario").modal("show");
            }
        });
        ev.preventDefault();
    });

    $("body").on("click", "#update_usuario", function(ev) {
        var frm = $('#operacion_update_form');
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            dataType: 'json',
            success: function (data) {
                if (data.success == true) {
                    swal("Muy bien!", "Usuario actualizada correctamente!", "success");
                    $("#contentEditQuote").empty();
                    $("#modalEditQuote").modal("hide");
                    loadUsuarios();

                } else {
                    swal("Oops!", "Al parecer hemos tenido un inconveniente, intentelo nuevamente.", "success");
                }
            },
            error :function( data ) {
                var errors = '';
                for(datos in data.responseJSON){
                    errors += data.responseJSON[datos] + '<br>';
                }
                $('#errors_update').show().html(errors);
            }
        });
        ev.preventDefault();
    });

    $("body").on("click", ".delete-usuario", function(event) {
        var dataId      = $(this).attr('data-id');
        event.preventDefault();
        swal({
            title: "Esta seguro?",
            text: "Desea eliminar a este usuario?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar!",
            cancelButtonText: "No, cancelar!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url: urlBase+'/operacion/'+dataId,
                    type: 'DELETE',
                    data: dataId,
                    success: function( data ) {
                        if (data.success == true) {
                            swal("Eliminado!", "El usuario ha sido eliminado.", "success");
                            loadUsuarios();
                        }
                    },
                    error: function( data ) {
                        if ( data.status === 422 ) {
                            swal("Cancelado", "El usuario no se ha eliminado", "error");
                        }
                    }
                });
                return false;
            } else {
                swal("Cancelado", "El usuario no se eliminara", "error");
            }
        });
    });
    function loadUsuarios() {
        setInterval(function() {
            location.reload();
        }, 200);
    }
});
