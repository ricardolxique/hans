jQuery(document).ready(function($)
{
    $("body").on("click", ".delete-profile", function(event) {
        event.preventDefault();
        swal({
            title: "Esta seguro?",
            text: "No podras recuper el perfil eliminado",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar!",
            cancelButtonText: "No, cancelar!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                var inputData   = $('#formDeletePerfil').serialize();
                var dataId      = $('#btnDeletePerfil').attr('data-id');

                $.ajax({
                    url: urlBase+'/perfiles/'+dataId,
                    type: 'DELETE',
                    data: inputData,
                    success: function( msg ) {
                        if ( msg.status === 'success' ) {
                            swal("Eliminado!", "El perfil ha sido borrado.", "success");
                            setInterval(function() {
                                window.location.replace(urlBase+"/perfiles");
                            }, 500);
                        }
                    },
                    error: function( data ) {
                        if ( data.status === 422 ) {
                            swal("Cancelado", "El perfil no se eliminara", "error");
                        }
                    }
                });
                return false;

            } else {
                swal("Cancelado", "El perfil no se eliminara", "error");
            }
        });
    });
});