jQuery(document).ready(function() {
    jQuery("body").on("change", "#modulo_id", function() {
        var modulo_id = jQuery(this).val();
        jQuery.ajax({
            type: "GET",
            url: urlBase+'/permisos/loadSecciones/'+modulo_id,
            dataType: 'html',
            success: function(data) {
                jQuery("#combo_secciones").empty().append(data);
            }
        });
    });
    jQuery("body").on("change", "#seccion_id", function() {
        var seccion_id = jQuery(this).val();
        jQuery.ajax({
            type: "GET",
            url: urlBase+'/permisos/loadPerfiles/'+seccion_id,
            dataType: 'html',
            success: function(data) {
                jQuery("#combo_perfiles").empty().append(data);
            }
        });
    });
    jQuery("body").on("change", ".permiso_value", function() {
        if(jQuery(this).prop("checked")) {
            var active  =   20;
        } else {
            var active  =   10;
        }

        var name        =   jQuery(this).attr("name");
        var value       =   jQuery(this).val();
        var section     =   jQuery("#seccion_id").val();
        if (name != "" && value != "" && section != "" && active != "") {
            console.log("aca");
            jQuery.ajax({
                type: "POST",
                url: urlBase + '/permisos/changePermisos',
                data: { "name" : name, "value" : value, "section" : section, "active" : active },
                dataType: 'json',
                success: function (data) {
                    
                }
            });
        }
    });

    jQuery("body").on("click", "#change-permisons", function() {
        jQuery(".change-check-permissons").show();
        jQuery(".show-icon-permissons").hide();
    });
});