jQuery(document).ready(function($)
{
    $("body").on("click", ".delete-poll", function(event) {
        event.preventDefault();
        swal({
            title: "Esta seguro?",
            text: "No podras recuperar la encuesta eliminada",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar!",
            cancelButtonText: "No, cancelar!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                var inputData   = $('#formDeletePoll').serialize();
                var dataId      = $('#btnDeletePoll').attr('data-id');

                $.ajax({
                    url: urlBase+'/encuestas/'+dataId,
                    type: 'DELETE',
                    data: inputData,
                    success: function( msg ) {
                        if ( msg.status === 'success' ) {
                            swal("Eliminado!", "La encuesta ha sido eliminada.", "success");
                            setInterval(function() {
                                window.location.replace(urlBase+"/encuestas");
                            }, 500);
                        }
                    },
                    error: function( data ) {
                        if ( data.status === 422 ) {
                            swal("Cancelado", "La encuesta no se ha eliminado", "error");
                        }
                    }
                });
                return false;

            } else {
                swal("Cancelado", "La encuesta no se eliminara", "error");
            }
        });
    });
    $("body").on("change", ".acciones", function() {
        var link    =   $(this).val();
        console.log(link);
        window.location.replace(link);
    });
});