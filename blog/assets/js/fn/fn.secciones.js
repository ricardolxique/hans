jQuery(document).ready(function($)
{
    $("body").on("click", ".delete-seccion", function(event) {
        event.preventDefault();
        swal({
            title: "Esta seguro?",
            text: "No podras recuperar la sección eliminado",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar!",
            cancelButtonText: "No, cancelar!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                var inputData   = $('#formDeleteSeccion').serialize();
                var dataId      = $('#btnDeleteSeccion').attr('data-id');

                $.ajax({
                    url: urlBase+'/secciones/'+dataId,
                    type: 'DELETE',
                    data: inputData,
                    success: function( msg ) {
                        if ( msg.status === 'success' ) {
                            swal("Eliminado!", "La sección ha sido eliminada.", "success");
                            setInterval(function() {
                                window.location.replace(urlBase+"/secciones/"+msg.cat);
                            }, 500);
                        }
                    },
                    error: function( data ) {
                        if ( data.status === 422 ) {
                            swal("Cancelado", "La sección no se ha eliminado", "error");
                        }
                    }
                });
                return false;
            } else {
                swal("Cancelado", "La sección no se eliminara", "error");
            }
        });
    });
});