jQuery(document).ready(function() {
    jQuery("#encuesta_id").change(function () {
        var id_encuesta = jQuery(this).val();
        jQuery.ajax({
            type: "POST",
            url: urlBase+'/reportes/loadPreguntas',
            data: { id_encuesta : id_encuesta },
            dataType: 'html',
            success: function (data) {
                jQuery("#load_preguntas").slideDown();
                jQuery("#combo_preguntas").empty().append(data);
                jQuery('#my-select').multiSelect();
                jQuery('#select-all').click(function(){
                    jQuery('#my-select').multiSelect('select_all');
                    return false;
                });
                jQuery('#deselect-all').click(function(){
                    jQuery('#my-select').multiSelect('deselect_all');
                    return false;
                });
            }
        });
    });
    jQuery("body").on("click", "#generar_reporte", function () {
        jQuery(".combo_download").slideUp();
        jQuery("#link_download").attr("href", "#");
        jQuery("#link_download_sav").attr("href", "#");
        var encuesta_id =   jQuery("#encuesta_id").val();
        var preguntas   =   jQuery("#my-select").val();

        if (preguntas != "" && encuesta_id != "" && preguntas != null) {
            show_loading_bar({
                pct: 100,
                delay: 6.7,
            });
            jQuery("#loading-canvas").show();
            jQuery.ajax({
                type: "POST",
                url: "reportes/generaReportes",
                data: { encuesta_id : encuesta_id, preguntas : preguntas },
                dataType: "json",
                success: function (data) {
                    if (data.result == true) {
                        swal("Bien!", "Tu reporte ha sido creado, da click en el siguiente link para descargar tu reporte.", "success");
                        jQuery(".combo_download").slideDown();
                        jQuery("#link_download").attr("href", data.linkExcel);
                        jQuery("#link_download_sav").attr("href", data.linkSAV);
                        jQuery("#loading-canvas").hide();
                    } else {
                        swal("Oops!", "Debes seleccionar algunas preguntas para generar tu reporte.", "warning");
                        jQuery("#loading-canvas").hide();
                    }
                }
            });
        } else {
            swal("Oops!", "Debes seleccionar algunas preguntas para generar tu reporte.", "warning");
        }
    });
});