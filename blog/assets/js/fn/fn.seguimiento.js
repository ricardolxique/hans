jQuery(document).ready(function() {
    jQuery("#encuesta_id").change(function () {
        var encuesta = jQuery(this).val();
        jQuery.ajax({
            type: "POST",
            url: "seguimiento/loadSeguimiento",
            data: {encuesta: encuesta},
            dataType: 'html',
            success: function (data) {
                jQuery("#div_avance").empty().append(data);
            }
        });
    });
});