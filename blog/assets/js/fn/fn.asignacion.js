jQuery(document).ready(function($)  {
    $("body").on("click", "#agregar_coordinador", function(ev) {
        var frm         =   $('#asignacion_form');
        var coo         =   $('#usuario_id').val();
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            dataType: 'json',
            success: function (data) {
                if (data.success == true) {
                    $("#usuario_id option[value='"+coo+"']").remove();
                    $(frm)[0].reset();
                    swal("Muy bien!", "Coordinador cargado correctamente!", "success");
                    loadCoordinador(data.encuesta);
                } else {
                    swal("Oops!", "Al parecer hemos tenido un inconveniente, intentelo nuevamente.", "success");
                }
            },
            error :function( data ) {
                var errors = '';
                for(datos in data.responseJSON){
                    errors += data.responseJSON[datos] + '<br>';
                }
                $('#errors').show().html(errors);
            }
        });
        ev.preventDefault();
    });

    $("body").on("click", ".delete-coordinator", function(event) {
        var dataId      = $(this).attr('data-id');
        event.preventDefault();
        swal({
            title: "Esta seguro?",
            text: "Desea eliminar a este coordinador y a todos sus usuarios asignados?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar!",
            cancelButtonText: "No, cancelar!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url: urlBase+'/asignacion/'+dataId,
                    type: 'DELETE',
                    data: dataId,
                    success: function( data ) {
                        if (data.success == true) {
                            swal("Eliminado!", "El coordinador ha sido eliminado.", "success");
                            loadCoordinador(data.encuesta);
                        }
                    },
                    error: function( data ) {
                        if ( data.status === 422 ) {
                            swal("Cancelado", "El coordinador no se ha eliminado", "error");
                        }
                    }
                });
                return false;
            } else {
                swal("Cancelado", "El coordinador no se eliminara", "error");
            }
        });
    });
    function loadCoordinador() {
        setInterval(function() {
            location.reload();
        }, 200);
    }
});