jQuery(document).ready(function ($) {
    $("body").on("click", ".view-image", function(event) {
        event.preventDefault();
        var src = $(this).attr("data-src");
        $("#img-preview").attr("src", src);
        $("#modalViewImage").modal("show");
    });
    $("body").on("click", ".rechaza-encuesta", function(event) {
        event.preventDefault();
        var id      =   $(this).attr("data-id");
        var div     =   $(this).attr("data-div");
        var estado  =   0;
        procesaValidacionGaleria(id, estado, div);
    });
    $("body").on("click", ".aprueba-encuesta", function(event) {
        event.preventDefault();
        var id      =   $(this).attr("data-id");
        var div     =   $(this).attr("data-div");
        var estado  =   1;
        procesaValidacionGaleria(id, estado, div);
    });
});
function procesaValidacionGaleria(idFoto, estado, div) {
    var parametros    =   {
        idFoto : idFoto,
        estado  : estado
    };
    jQuery.ajax({
        type: "POST",
        url: "revisa",
        data: parametros,
        dataType: 'json',
        success: function (dataJson) {
            if (dataJson.status == true) {
                toastr.success(dataJson.msg, "Bien!");
                jQuery("#div-"+div).fadeOut(3000).remove();
                cargaFoto(div);
            } else {
                toastr.error(dataJson.msg, "Oops!");
            }
        }
    });
}
function cargaFoto(div) {
    jQuery.ajax({
        type: "POST",
        url: "loadFoto",
        data: { div:div },
        dataType: 'html',
        success: function (dataHtml) {
            jQuery("#grid-fotos-validacion").append(dataHtml);
        }
    });
}