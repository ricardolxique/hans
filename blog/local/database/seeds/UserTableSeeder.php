<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder 
{
    public function run(){
        DB::table('users')->insert([
            'name'  => 'admin',
            'username'  => 'admin',
            'email'     => 'admin@admin.com',
            'password' => Hash::make('admin') // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
        ]);
    }
}