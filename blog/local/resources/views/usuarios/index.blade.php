@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
@endsection

@section('title', 'Usuarios')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Administrador de usuarios</h3>
            <hr>
        </div>
    </div>
    @if(in_array("2", $arrayPermisos))
        <div class="row">
            <div class="col-md-2 col-md-offset-10">
                {!!  Html::link('usuarios/create', "Crear Usuario", array('class' => 'btn btn-blue pull-right')) !!}
            </div>
        </div>
    @endif
    <br/>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered datatable" id="table-1">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Teléfono</th>
                    <th>Perfil</th>
                    <th>Username</th>
                    @if(in_array("3", $arrayPermisos))
                        <th></th>
                        <th width="5%"></th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @forelse ($usuarios as $u)
                    <tr>
                        <td>{{ $u->nombre.' '.$u->apellidos  }}</td>
                        <td>{{ $u->email  }}</td>
                        <td>{{ $u->telefono  }}</td>
                        <td>{{ $u->perfiles->nombre  }}</td>
                        <td>{{ $u->username  }}</td>
                        @if(in_array("3", $arrayPermisos))
                            <td>{!!  Html::link('usuarios/'.Crypt::encrypt($u->id).'/edit', "Editar", array('class' => 'btn btn-primary center-link')) !!}</td>
                            <td>{!!  Html::link('usuarios/changePassword/'.Crypt::encrypt($u->id), "Cambiar password", array('class' => 'btn btn-primary center-link')) !!}</td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="3">{{ 'No existen usuarios cargados' }}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('assetsJs')
    {!! HTML::script('assets/js/jquery.dataTables.min.js') !!}
    {!! HTML::script('assets/js/datatables/TableTools.min.js') !!}
    {!! HTML::script('assets/js/dataTables.bootstrap.js') !!}
    {!! HTML::script('assets/js/datatables/jquery.dataTables.columnFilter.js') !!}
    {!! HTML::script('assets/js/datatables/lodash.min.js') !!}
    {!! HTML::script('assets/js/datatables/responsive/js/datatables.responsive.js') !!}
    {!! HTML::script('assets/js/sweetalert.min.js') !!}
    {!! HTML::script('assets/js/fn.datatables.js') !!}

@endsection