@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
@endsection

@section('title', 'Editar usuarios')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4>Editar usuario</h4>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-md-offset-10">
            {!!  Html::link('usuarios/', " Regresar", array('class' => 'btn btn-blue pull-right')) !!}
        </div>
    </div>
    <div class="row" style="margin-top: 25px;">
        <div class="col-md-12">
            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
            @if(Session::has('status'))
                <div class="alert alert-success">
                    {{ Session::get('status') }}
                </div>
            @endif
            {!! Form::open([
                'method'=> 'PATCH',
                'route' => ['usuarios.update', Crypt::encrypt($usuario->id)]
            ]) !!}

                <div class="form-group">
                    {!! Form::label('nombre', 'Nombre del usuario:', ['class' => 'control-label']) !!}
                    {!! Form::text('nombre', $usuario->nombre, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('apellidos', 'Apellidos:', ['class' => 'control-label']) !!}
                    {!! Form::text('apellidos', $usuario->apellidos, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
                    {!! Form::text('email', $usuario->email, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('telefono', 'Teléfono:', ['class' => 'control-label']) !!}
                    {!! Form::text('telefono', $usuario->telefono, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('username', 'Username:', ['class' => 'control-label']) !!}
                    {!! Form::text('username', $usuario->username, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('idTipoUsuario', 'Tipo de perfil:', ['class' => 'control-label']) !!}
                    {!! Form::select('idTipoUsuario', $perfiles, $usuario->idTipoUsuario, ['class' => 'form-control', 'placeholder' => '-- Selecciona --']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::submit('Actualizar usuario', ['class' => 'btn btn-primary pull-left']) !!}
                </div>
            {!! Form::close() !!}
            @if(in_array("4", $arrayPermisos))
                {!! Form::open(['method' => 'DELETE', 'id' => 'formDeleteUser', 'route' => ['usuarios.destroy', Crypt::encrypt($usuario->id)]]) !!}
                    <div class="col-md-6">
                        {!! Form::button( 'Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger pull-right delete-user', 'id' => 'btnDeleteUser', 'data-id' => Crypt::encrypt($usuario->id) ] ) !!}
                    </div>
                {!! Form::close() !!}
            @endif
        </div>
    </div>
@endsection
@section('assetsJs')
    {!! HTML::script('assets/js/sweetalert.min.js') !!}
    {!! HTML::script('assets/js/fn/fn.usuarios.js') !!}
@endsection