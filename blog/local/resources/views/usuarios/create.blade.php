@extends('template/index')

@section('title', 'Usuarios')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4>Crear usuario</h4>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-md-offset-10">
            {!!  Html::link('usuarios/', " Regresar", array('class' => 'btn btn-blue pull-right')) !!}
        </div>
    </div>
    <div class="row" style="margin-top: 25px;">
        <div class="col-md-12">
            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
            @if(Session::has('status'))
                <div class="alert alert-success">
                    {{ Session::get('status') }}
                </div>
            @endif
            {!! Form::open([
                'route' => 'usuarios.store'
            ]) !!}

                <div class="form-group">
                    {!! Form::label('nombre', 'Nombre del usuario:', ['class' => 'control-label']) !!}
                    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('apellidos', 'Apellidos:', ['class' => 'control-label']) !!}
                    {!! Form::text('apellidos', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('telefono', 'Teléfono:', ['class' => 'control-label']) !!}
                    {!! Form::text('telefono', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('username', 'Username:', ['class' => 'control-label']) !!}
                    {!! Form::text('username', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('password', 'Password:', ['class' => 'control-label']) !!}
                    {!! Form::password('password', ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('password_repeat', 'Repetir password:', ['class' => 'control-label']) !!}
                    {!! Form::password('password_repeat', ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('idTipoUsuario', 'Tipo de perfil:', ['class' => 'control-label']) !!}
                    {!! Form::select('idTipoUsuario', $perfiles, null, ['class' => 'form-control', 'placeholder' => '-- Seleccione --']) !!}
                </div>
                {!! Form::submit('Crear nuevo usuario', ['class' => 'btn btn-primary pull-right']) !!}

            {!! Form::close() !!}
        </div>
    </div>
@endsection