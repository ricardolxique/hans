@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
@endsection

@section('title', 'Perfiles')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Administrador de perfiles</h3>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-md-offset-10">
            {!!  Html::link('perfiles/create', "Crear Perfil", array('class' => 'btn btn-blue pull-right')) !!}
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered datatable" id="table-1">
                <thead>
                    <tr>
                        <th width="90%">Nombre</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($perfiles as $perfil)
                        <tr>
                            <td>{{ $perfil->nombre  }}</td>
                            <td>{!!  Html::link('perfiles/'.Crypt::encrypt($perfil->id).'/edit', "Editar", array('class' => 'btn btn-primary center-link')) !!}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="2">{{ 'No existen perfiles cargados' }}</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('assetsJs')
    {!! HTML::script('assets/js/jquery.dataTables.min.js') !!}
    {!! HTML::script('assets/js/datatables/TableTools.min.js') !!}
    {!! HTML::script('assets/js/dataTables.bootstrap.js') !!}
    {!! HTML::script('assets/js/datatables/jquery.dataTables.columnFilter.js') !!}
    {!! HTML::script('assets/js/datatables/lodash.min.js') !!}
    {!! HTML::script('assets/js/datatables/responsive/js/datatables.responsive.js') !!}
    {!! HTML::script('assets/js/sweetalert.min.js') !!}
    {!! HTML::script('assets/js/fn.datatables.js') !!}

@endsection