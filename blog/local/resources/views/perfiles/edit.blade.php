@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
@endsection

@section('title', 'Agregar perfil')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4>Editar perfil</h4>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-md-offset-10">
            {!!  Html::link('perfiles/', " Regresar", array('class' => 'btn btn-blue pull-right')) !!}
        </div>
    </div>
    <div class="row" style="margin-top: 25px;">
        <div class="col-md-12">
            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
            @if(Session::has('status'))
                <div class="alert alert-success">
                    {{ Session::get('status') }}
                </div>
            @endif
            {!! Form::open([
                'method'=> 'PATCH',
                'route' => ['perfiles.update', Crypt::encrypt($perfil->id)]
            ]) !!}
                <div class="form-group">
                    {!! Form::label('nombre', 'Nombre del perfil:', ['class' => 'control-label']) !!}
                    {!! Form::text('nombre', $perfil->nombre, ['class' => 'form-control']) !!}
                </div>

                 <div class="col-md-6">
                     {!! Form::submit('Actualizar', ['class' => 'btn btn-primary pull-left']) !!}
                 </div>
            {!! Form::close() !!}
            {!! Form::open(['method' => 'DELETE', 'id' => 'formDeletePerfil', 'route' => ['perfiles.destroy', Crypt::encrypt($perfil->id)]]) !!}
                <div class="col-md-6">
                    {!! Form::button( 'Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger pull-right delete-profile', 'id' => 'btnDeletePerfil', 'data-id' => Crypt::encrypt($perfil->id) ] ) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
@section('assetsJs')
    {!! HTML::script('assets/js/sweetalert.min.js') !!}
    {!! HTML::script('assets/js/fn/fn.perfiles.js') !!}
@endsection