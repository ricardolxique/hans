@extends('template/index')

@section('title', 'Agregar perfil')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4>Agregar perfil</h4>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-md-offset-10">
            {!!  Html::link('perfiles/', " Regresar", array('class' => 'btn btn-blue pull-right')) !!}
        </div>
    </div>
    <div class="row" style="margin-top: 25px;">
        <div class="col-md-12">
            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
            @if(Session::has('status'))
                <div class="alert alert-success">
                    {{ Session::get('status') }}
                </div>
            @endif
            {!! Form::open([
                'route' => 'perfiles.store'
            ]) !!}

            <div class="form-group">
                {!! Form::label('nombre', 'Nombre del perfil:', ['class' => 'control-label']) !!}
                {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
            </div>

            {!! Form::submit('Crear nuevo perfil', ['class' => 'btn btn-primary pull-right']) !!}

            {!! Form::close() !!}
        </div>
    </div>
@endsection