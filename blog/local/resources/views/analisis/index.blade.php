@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
    {!! HTML::style('assets/js/switch/css/bootstrap3/bootstrap-switch.min.css') !!}
@endsection

@section('title', 'Cargar Preguntas')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Analisis de resultados</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p class="text-left">Seleccione una encuesta y posteriormente un pregunta para cargar la grafica de resultados.</p>
        </div>
        <div class="col-md-5">
            {!! Form::select('encuestas', $encuestas, null, ['id' => 'encuesta_id', 'class' => 'form-control', 'placeholder' => '-- Seleccione --']) !!}
        </div>
        <div class="col-md-1"></div>
        <div id="div_lista_preguntas" style="display: none;" class="col-md-5">
            <div class="row">
                <div id="lista_preguntas" class="col-md-12"></div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 25px;">
        <div class="col-md-12">
            <div id="div_grafica" class="col-md-12">
                <div id="myChart" class="row" style="display: none;">
                    <div id="container"  style="min-width: 310px; max-width: 100%; height: 400px; margin: 0 auto">

                    </div>
                </div>
                <h5 id="myText" class="text-center" style="display: none">No existen datos para esta gráfica</h5>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="modalEditQuote" aria-hidden="false" style="display: none;">
        <div class="modal-dialog" style="width: 60%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Editar pregunta</h4>
                </div>
                <div id="contentEditQuote" class="modal-body" style="overflow: auto;"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="update_pregunta" class="btn btn-info">Actualizar pregunta</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('assetsJs')
    {!! HTML::script('http://code.highcharts.com/highcharts.js') !!}
    {!! HTML::script('assets/js/modules/exporting.js') !!}
    {!! HTML::script('assets/js/fn/fn.analisis.js') !!}
    {!! HTML::script('assets/js/sweetalert.min.js') !!}
@endsection