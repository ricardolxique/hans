@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
    {!! HTML::style('assets/js/switch/css/bootstrap3/bootstrap-switch.min.css') !!}
@endsection

@section('title', 'Agregar módulo')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4>Agregar módulo</h4>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-md-offset-10">
            {!!  Html::link('modulos/', " Regresar", array('class' => 'btn btn-blue pull-right')) !!}
        </div>
    </div>
    <div class="row" style="margin-top: 25px;">
        <div class="col-md-12">
            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
            @if(Session::has('status'))
                <div class="alert alert-success">
                    {{ Session::get('status') }}
                </div>
            @endif
            {!! Form::open([
                'route' => 'modulos.store'
            ]) !!}

            <div class="form-group">
                <div class="col-md-12">
                    {!! Form::label('modulo', 'Nombre del módulo:', ['class' => 'control-label']) !!}
                    {!! Form::text('modulo', null, ['class' => 'form-control']) !!}
                </div>
                <br/>
                <div class="col-md-12">
                    {!! Form::label('icon', 'Icono para este módulo:', ['class' => 'control-label']) !!}
                    {!! Form::text('icon', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            {!! Form::submit('Crear nuevo módulo', ['class' => 'btn btn-primary pull-right']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
@section('assetsJs')
    {!! HTML::script('assets/js/switch/js/bootstrap-switch.min.js') !!}
@endsection