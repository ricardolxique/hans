@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
@endsection

@section('title', 'Editar cliente')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4>Editar módulo</h4>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-md-offset-10">
            {!!  Html::link('modulos/', " Regresar", array('class' => 'btn btn-blue pull-right')) !!}
        </div>
    </div>
    <div class="row" style="margin-top: 25px;">
        <div class="col-md-12">
            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
            @if(Session::has('status'))
                <div class="alert alert-success">
                    {{ Session::get('status') }}
                </div>
            @endif
            {!! Form::open([
                'method'=> 'PATCH',
                'route' => ['modulos.update', Crypt::encrypt($modulos->id)]
            ]) !!}

            <div class="form-group">
                <div class="col-md-12">
                    {!! Form::label('modulo', 'Nombre del módulo:', ['class' => 'control-label']) !!}
                    {!! Form::text('modulo', $modulos->modulo, ['class' => 'form-control']) !!}
                </div>
                <br/>
                <div class="col-md-12">
                    {!! Form::label('icon', 'Icono para este módulo:', ['class' => 'control-label']) !!}
                    {!! Form::text('icon', $modulos->icon, ['class' => 'form-control']) !!}
                </div>
                <div class="col-md-12" style="margin-top: 10px;">
                    {!! Form::submit('Actualizar módulo', ['class' => 'btn btn-primary pull-right']) !!}
                </div>
                {!! Form::close() !!}
                {!! Form::open(['method' => 'DELETE', 'id' => 'formDeleteModulo', 'route' => ['modulos.destroy', Crypt::encrypt($modulos->id)]]) !!}
                <div class="col-md-6" style="margin-top: 10px;">
                    {!! Form::button( 'Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger pull-right delete-modulo', 'id' => 'btnDeleteModulo', 'data-id' => Crypt::encrypt($modulos->id) ] ) !!}
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>
@endsection
@section('assetsJs')
    {!! HTML::script('assets/js/sweetalert.min.js') !!}
    {!! HTML::script('assets/js/fn/fn.modulos.js') !!}
@endsection