@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
    {!! HTML::style('assets/js/switch/css/bootstrap3/bootstrap-switch.min.css') !!}
@endsection

@section('title', 'Indicadores')

@section('content')
    <h3>Administrador de indicadores</h3>
    <div class="row" style="margin-top: 20px;">
        <div class="col-md-6">
            <p style="height: 35px;">Selecciona un estudio para comenzar.</p>
            {!! Form::select('encuestas', $encuestas, null, ['id' => 'encuesta_id', 'class' => 'form-control', 'placeholder' => '-- Seleccione --']) !!}
        </div>
        <div class="col-md-6">
            @if(in_array("2", $arrayPermisos))
                {!! Form::open([
                    'id'        =>  'indicadores_form',
                    'route'     =>  'indicadores.store',
                    'method'    =>  'POST'
                ]) !!}
                <div id="agrega_pregunta"></div>
                {!! Form::close() !!}
            @endif
        </div>
    </div>
    <hr>
    <div class="row" style="padding-top: 20px;">
        <div class="col-md-12">
            <div id="list_preguntas"></div>
        </div>
    </div>
@endsection

@section('assetsJs')
    {!! HTML::script('assets/js/fn/fn.indicadores.js') !!}
    {!! HTML::script('assets/js/sweetalert.min.js') !!}
@endsection