<div class="row">
    <div class="col-md-12">
        @forelse($preguntas as $p)
            <div class="bs-callout">
                <div class="col-md-10"><h5 class="text-left">{{ $p->encuestasPreguntas->pregunta  }}</h5></div>
                <div class="col-md-2">
                    @if(in_array("4", $arrayPermisos))
                        {!! Form::button( ' Eliminar', ['type' => 'submit', 'class' => 'btn btn-red btn-sm delete-quote', 'id' => 'btnDeleteQuote', 'data-id' => Crypt::encrypt($p->id) ] ) !!}
                    @endif
                </div>
            </div>
        @empty
            <h4 class="text-center">No contamos con indicadores disponibles para este estudio.</h4>
        @endforelse
    </div>
</div>