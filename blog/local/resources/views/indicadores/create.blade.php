<div class="col-md-12">
    <p style="height: 35px;;">De la lista de preguntas que se muestra a continuación selecciona tus indicadores para este estudio, recuerdas que solo puedes elegir 5 preguntas.</p>
    <div id="errors" class="alert alert-danger" style="display: none;"></div>
</div>
<div class="col-md-12">
    {!! Form::hidden('encuesta_id', Crypt::encrypt($id)) !!}
    {!! Form::select('pregunta', $preguntas, null, ['id' => 'pregunta_id', 'class' => 'form-control', 'placeholder' => '-- Seleccione --']) !!}
</div>
<div class="col-md-12">
    {!!  Form::submit("Agregar", array('id' => 'agregar_pregunta', 'class' => 'btn btn-blue pull-right', 'style' => 'margin-top: 20px;')) !!}
</div>
