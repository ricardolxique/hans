<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>HANS - Login</title>
        {!! HTML::style('assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css') !!}
        {!! HTML::style('assets/css/font-icons/entypo/css/entypo.css') !!}
        {!! HTML::style('https://www.google.com/fonts#QuickUsePlace:quickUse/Family:Muli') !!}
        {!! HTML::style('assets/css/bootstrap.css') !!}
        {!! HTML::style('assets/css/neon-core.css') !!}
        {!! HTML::style('assets/css/neon-theme.css') !!}
        {!! HTML::style('assets/css/neon-forms.css') !!}
        {!! HTML::style('assets/css/custom.css') !!}
    </head>
    <body class="page-body login-page login-form-fall">
        <div class="login-container">
            <div class="login-form">
                <div class="login-content">
                    <a href="login" class="logo">
                        <img src="assets/images/imarkers.png" width="100%" />
                    </a>
                    <br/><br/>
                    @if(Session::has('mensaje_error'))
                    <div class="form-login-error" style="display: block;">
                        <h3>Mensaje login</h3>
                        <p>{!! Session::get('mensaje_error') !!}</p>
                    </div>
                    @endif
                    {!! Form::open(array('url' => '/login', 'id' => 'form_login')) !!}
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="entypo-user"></i>
                            </div>
                            {!! Form::text('username', Input::old('username'), array('id' => 'username', 'placeholder' => 'Username', 'class' => 'form-control')) !!}
                        </div>
                        <br/>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="entypo-key"></i>
                                </div>
                                {!! Form::password('password', array('id' => 'password', 'placeholder' => 'Password', 'class' => 'form-control')) !!}
                            </div>
                            <br/>
                            <div class="form-group">
                                {!! Form::submit('Entrar', array('class' => 'btn btn-primary btn-block btn-login text-center')) !!}
                            </div>    
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var baseurl = '';
    </script>
    {!! HTML::script('assets/js/jquery-1.11.0.min.js') !!}
    {!! HTML::script('assets/js/gsap/main-gsap.js') !!}
    {!! HTML::script('assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js') !!}
    {!! HTML::script('assets/js/bootstrap.js') !!}
    {!! HTML::script('assets/js/joinable.js') !!}
    {!! HTML::script('assets/js/resizeable.js') !!}
    {!! HTML::script('assets/js/neon-api.js') !!}
    {!! HTML::script('assets/js/jquery.validate.min.js') !!}
	{!! HTML::script('assets/js/neon-login.js') !!}

	<!-- JavaScripts initializations and stuff -->
    {!! HTML::script('assets/js/neon-custom.js') !!}
    
	<!-- Demo Settings -->
    {!! HTML::script('assets/js/neon-demo.js') !!}
</html>