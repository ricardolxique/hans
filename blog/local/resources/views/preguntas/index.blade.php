@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
    {!! HTML::style('assets/js/switch/css/bootstrap3/bootstrap-switch.min.css') !!}
@endsection

@section('title', 'Cargar Preguntas')

@section('content')
    <h3>Administrador de preguntas</h3>
    <div class="row">
        <div class="col-md-4">
            {!! Form::label('encuestas', 'Selecciona una encuesta:', ['class' => 'control-label']) !!}
        </div>
        <div class="col-md-8">
            {!! Form::select('encuestas', $encuestas, $idEncuesta, ['id' => 'encuesta_id', 'class' => 'form-control', 'placeholder' => '-- Seleccione --']) !!}
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            @if(in_array("2", $arrayPermisos))
                {!! Form::open([
                    'id'        =>  'pregunta_form',
                    'route'     =>  'preguntas.store',
                    'method'    =>  'POST'
                ]) !!}
                <div id="crea_preguntas"></div>
                {!! Form::close() !!}
            @endif
        </div>
    </div>
    <hr>
    <div class="row" style="padding-top: 20px;">
        <div class="col-md-12">
            <div id="list_preguntas"></div>
        </div>
    </div>
    <div class="modal fade in" id="modalEditQuote" aria-hidden="false" style="display: none;">
        <div class="modal-dialog" style="width: 60%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Editar pregunta</h4>
                </div>
                <div id="contentEditQuote" class="modal-body" style="overflow: auto;"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="update_pregunta" class="btn btn-info">Actualizar pregunta</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('assetsJs')
    @if ($idEncuesta != null)
        <script>
            var idEncuesta  =   '{{ $idEncuesta  }}';
            jQuery("#encuesta_id").val(idEncuesta);
            jQuery(document).ready(function() {
                loadPreguntas(idEncuesta);
                function loadPreguntas(encuestaId) {
                    jQuery.ajax({
                        type: "GET",
                        url: urlBase+'/preguntas/'+encuestaId,
                        dataType: 'html',
                        success: function(data) {
                            jQuery("#crea_preguntas").empty().append(data);
                            loadQuestions(encuestaId);
                        }
                    });
                }
                function loadQuestions(id_encuesta) {
                    jQuery.ajax({
                        type: "GET",
                        url: urlBase+'/preguntas/loadList/'+id_encuesta,
                        dataType: 'html',
                        success: function(data) {
                            jQuery("#list_preguntas").empty().append(data);
                            jQuery("#active_order").bootstrapSwitch({
                                'onText': "SI",
                                'offText': "NO",
                                'state': false

                            });
                            jQuery('#list-1').nestable().on('change', updateOutput );

                            jQuery('#active_order').on('switchChange.bootstrapSwitch', function (event, state) {
                                if (state) {
                                    jQuery("#list-1").addClass("dd");
                                    jQuery("#list-10").hide();
                                    jQuery("#list-1").show();
                                } else {
                                    jQuery("#list_preguntas").empty();
                                    jQuery("#list-1").removeClass("dd");
                                    jQuery("#list-1").nestable("destroy");
                                    jQuery("#list-1").hide();
                                    jQuery("#list-10").show();
                                    loadQuestions(id_encuesta);
                                }
                            });

                        }
                    });
                    var updateOutput = function(e) {
                        var json_text = jQuery('#list-1').nestable('serialize');
                        jQuery.ajax({
                            type: 'POST',
                            url: urlBase+'/preguntas/order',
                            data: { json : JSON.stringify(json_text)},
                            dataType: "json",
                            success: function(data) {
                                console.log(data);
                            }
                        });
                    };
                }
            });
        </script>
    @endif
    {!! HTML::script('assets/js/fn/fn.preguntas.js') !!}
    {!! HTML::script('assets/js/sweetalert.min.js') !!}
    {!! HTML::script('assets/js/jquery.nestable.js') !!}
    {!! HTML::script('assets/js/switch/js/bootstrap-switch.min.js') !!}
@endsection