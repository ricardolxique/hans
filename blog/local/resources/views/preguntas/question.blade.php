<div class="row">
    <div class="col-md-12">
        @if (!empty($preguntas))
            <div style="padding-bottom: 60px;">
                <h4>Preguntas</h4>
                <div class="col-md-5">
                    <h5>Para cambiar el orden de las preguntas active el combo y solo arrastre hasta colocar su pregunta en el orden deseado.</h5>
                </div>
                <div class="col-md-offset-3 col-md-4">
                    @if(in_array("3", $arrayPermisos))
                        <div class="form-group">
                            <p class="col-sm-6 control-label"><b>Ordenar preguntas</b></p>
                            <div class="col-sm-6">
                                <div class="make-switch">
                                    <input type="checkbox" id="active_order" value="1" name="active_order">
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div id="list-10" class="nested-list with-margins">
                <ul class="dd-list">
                    <?php $i = 1; ?>
                    @foreach($preguntas as $p)
                        <li class="dd-item" data-id="{{ Crypt::encrypt($p->id) }}">
                            <div class="dd-handle" style="height: 50px;">
                                <div class="col-md-6">
                                    <h5>{{ $p->pregunta  }}</h5>
                                </div>
                                <div class="col-md-2">
                                    {{ $p->tipoPreguntas->nombre  }}
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="col-md-4">
                                    @if(in_array("1", $arrayPermisosRespuestas))
                                        @if($p->pregunta_tipo_id > 1)
                                        {!!  Html::link('respuestas/loadList/'.Crypt::encrypt($p->id), "Respuestas", array('class' => 'btn btn-primary btn-sm answers_quote')) !!}
                                        @endif
                                    @endif
                                    </div>
                                    @if(in_array("3", $arrayPermisos))
                                    <div class="col-md-3">
                                        <button type="button" class="btn btn-blue btn-sm edit-quote" data-id="{{ Crypt::encrypt($p->id) }}">
                                            Editar
                                        </button>
                                    </div>
                                    @endif
                                    @if(in_array("4", $arrayPermisos))
                                        <div class="col-md-3">
                                            {!! Form::button( ' Eliminar', ['type' => 'submit', 'class' => 'btn btn-red btn-sm delete-quote', 'id' => 'btnDeleteQuote', 'data-id' => Crypt::encrypt($p->id) ] ) !!}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </li>
                        <?php $i++; ?>
                    @endforeach
                </ul>
            </div>
            <div id="list-1" class="nested-list with-margins" style="display: none;">
                <ul class="dd-list" style="list-style: initial; list-style-type: decimal;">
                    <?php $i = 1; ?>
                @foreach($preguntas as $p)
                    <li class="dd-item" data-id="{{ Crypt::encrypt($p->id) }}">
                        <div class="dd-handle" style="height: 50px;">
                            <div class="col-md-12">
                                {{ $p->pregunta  }}
                            </div>
                        </div>
                    </li>
                     <?php $i++; ?>
                @endforeach
                </ul>
            </div>
        @else
            <h4 class="text-center">No existen preguntas cargadas en esta encuesta</h4>
        @endif
    </div>
</div>