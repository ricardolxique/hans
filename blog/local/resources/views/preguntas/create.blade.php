<h4>Agregar pregunta</h4>
<div class="col-md-12">
    <div id="errors" class="alert alert-danger" style="display: none;"></div>
</div>
<div class="col-md-9">
    {!! Form::hidden('encuesta_id', Crypt::encrypt($id)) !!}
    {!! Form::label('pregunta', 'Pregunta:', ['class' => 'control-label']) !!}
    {!! Form::text('pregunta', null, ['id'  =>  'pregunta', 'class' => 'form-control', 'placeholder' => 'Escribe tu pregunta', 'required' => '', 'data-parsley-type' => 'email']) !!}
</div>
<div class="col-md-3">
    {!! Form::label('pregunta_tipo_id', 'Tipo de pregunta:', ['class' => 'control-label']) !!}
    {!! Form::select('pregunta_tipo_id', $tipoPreguntas, null, ['id' => 'pregunta_tipo_id', 'class' => 'form-control', 'placeholder' => '-- Seleccione --']) !!}
</div>
<div class="col-md-12">
    {!!  Form::submit("Agregar pregunta", array('id' => 'agregar_pregunta', 'class' => 'btn btn-blue pull-right', 'style' => 'margin-top: 20px;')) !!}
</div>
