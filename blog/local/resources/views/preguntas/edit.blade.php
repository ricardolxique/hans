{!! Form::open([
    'id'    =>  'pregunta_update_form',
    'method'=> 'PATCH',
    'route' => ['preguntas.update', Crypt::encrypt($pregunta->id)]
]) !!}
    <div class="col-md-12">
        <div id="errors_update" class="alert alert-danger" style="display: none;"></div>
    </div>
    <div class="col-md-9">
        {!! Form::label('pregunta', 'Pregunta:', ['class' => 'control-label']) !!}
        {!! Form::text('pregunta', $pregunta->pregunta, ['id'  =>  'pregunta', 'class' => 'form-control', 'placeholder' => 'Escribe tu pregunta', 'required' => '', 'data-parsley-type' => 'email']) !!}
    </div>
    <div class="col-md-3">
        {!! Form::label('pregunta_tipo_id', 'Tipo de pregunta:', ['class' => 'control-label']) !!}
        {!! Form::select('pregunta_tipo_id', $tipoPreguntas, Crypt::encrypt($pregunta->pregunta_tipo_id), ['id' => 'pregunta_tipo_id', 'class' => 'form-control', 'placeholder' => '-- Seleccione --']) !!}
    </div>
{!! Form::close() !!}
