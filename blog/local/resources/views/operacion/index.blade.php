@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
@endsection

@section('title', 'Encuestas')

@section('content')
    <div class="row">
        <div class="col-md-2 col-md-offset-10">
            {!!  Html::link('asignacion/'.Crypt::encrypt($coordinador->encuesta_id), " Regresar", array('class' => 'btn btn-blue pull-right')) !!}
        </div>
    </div>
    <h3>Administrador de operadores</h3>
    <h4>Coordinador: {{ $coordinador->usuarios->nombre  }} </h4>
    @if(in_array("2", $arrayPermisos))
        {!! Form::open([
            'id'        =>  'operacion_form',
            'route'     =>  'operacion.store',
            'method'    =>  'POST'
        ]) !!}
        <div class="row">
            <div class="col-md-12">
                <div id="errors" class="alert alert-danger" style="display: none;"></div>
            </div>
            <div class="col-md-8">
                {!! Form::hidden('operacion_coordinador_id', Crypt::encrypt($coordinador->id)) !!}
                {!! Form::label('usuario_id', 'Seleccione operador:', ['class' => 'control-label']) !!}
                {!! Form::select('usuario_id', $operadores, null, ['id' => 'usuario_id', 'class' => 'form-control', 'placeholder' => '-- Seleccione --']) !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('encuestas_asignadas', 'Encuestas asignadas (solo número):', ['class' => 'control-label']) !!}
                {!! Form::text('encuestas_asignadas', null, ['id' => 'encuestas_asignadas', 'class' => 'form-control', 'placeholder' => '0']) !!}
            </div>
            <div class="col-md-12">
                {!!  Form::submit("Agregar operador", array('id' => 'agregar_operador', 'class' => 'btn btn-blue pull-right', 'style' => 'margin-top: 20px;')) !!}
            </div>
        </div>
    {!! Form::close() !!}
    @endif
    <div id="div_operadores" class="row" style="margin-top: 50px;">
        <div class="col-md-12">
            <table class="table table-bordered datatable" id="table-1">
                <thead>
                <tr>
                    <th width="80%">Operador</th>
                    <th>Encuestas asignadas</th>
                    @if(in_array("3", $arrayPermisos))
                        <th></th>
                    @endif
                    @if(in_array("4", $arrayPermisos))
                        <th></th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach ($usuarios as $us)
                    <tr>
                        <td>{{ $us["nombre"] }}</td>
                        <td>{{ $us["asignadas"] }}</td>
                        @if(in_array("3", $arrayPermisos))
                            <td>{!!  Html::link('#', "Editar", array('class' => 'btn btn-primary center-link edit_numero_encuestas', 'data-id' => Crypt::encrypt($us["id"]))) !!}</td>
                        @endif
                        @if(in_array("4", $arrayPermisos))
                            <td>{!!  Form::button( ' Eliminar', ['type' => 'submit', 'class' => 'btn btn-red btn-sm delete-usuario', 'id' => 'btnDeleteUsuario', 'data-id' => Crypt::encrypt($us["id"]) ] ) !!}</td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade in" id="modalEditUsuario" aria-hidden="false" style="display: none;">
        <div class="modal-dialog" style="width: 60%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Editar</h4>
                </div>
                <div id="contentEditUsuario" class="modal-body" style="overflow: auto;"></div>
            </div>
        </div>
    </div>
@endsection

@section('assetsJs')
    {!! HTML::script('assets/js/jquery.dataTables.min.js') !!}
    {!! HTML::script('assets/js/datatables/TableTools.min.js') !!}
    {!! HTML::script('assets/js/dataTables.bootstrap.js') !!}
    {!! HTML::script('assets/js/datatables/jquery.dataTables.columnFilter.js') !!}
    {!! HTML::script('assets/js/datatables/lodash.min.js') !!}
    {!! HTML::script('assets/js/datatables/responsive/js/datatables.responsive.js') !!}
    {!! HTML::script('assets/js/sweetalert.min.js') !!}
    {!! HTML::script('assets/js/fn.datatables.js') !!}
    {!! HTML::script('assets/js/fn/fn.operacion.js') !!}

@endsection