{!! Form::open([
        'id'    =>  'operacion_update_form',
        'method'=> 'PATCH',
        'route' => ['operacion.update', Crypt::encrypt($usuario->id)]
    ]) !!}
<div class="row">
    <div class="col-md-12">
        <div id="errors" class="alert alert-danger" style="display: none;"></div>
    </div>
    <div class="col-md-12">
        {!! Form::label('encuestas_asignadas', 'Encuestas asignadas (solo número):', ['class' => 'control-label']) !!}
        {!! Form::text('encuestas_asignadas', $usuario->encuestas_asignadas, ['id' => 'encuestas_asignadas', 'class' => 'form-control', 'placeholder' => '0']) !!}
    </div>
    <div class="col-md-12">
        {!!  Form::submit("Actualizar", array('id' => 'update_usuario', 'class' => 'btn btn-blue pull-right', 'style' => 'margin-top: 20px;')) !!}
    </div>
</div>
{!! Form::close() !!}