@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}

@endsection

@section('title', 'Encuestas analisis | Encuestas')

@section('content')
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 20px;">
            <h3>{{ $encuesta->nombre  }}</h3>
        </div>
        <div class="col-md-offset-10 col-md-2">
            {!!  Html::link('encuestasanalisis', "Lista de estudios", array('class' => 'btn btn-blue center-link')) !!}
        </div>
    </div>

    <div class="row">
        @if (count($arrayEncuestas) > 0)
            <div class="col-md-4">
                <table class="table datatable" id="table-encuestas">
                    <thead>
                    <tr>
                        <th style="background: #FFF;"></th>
                        <th style="background: #FFF; width: 10%;"></th>
                        <th style="background: #FFF; width: 20%;"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                    @foreach($arrayEncuestas as $e)
                        <tr>
                            <td id="td-{{ $i }}" class="td-tabla">Encuesta {{ $i }}</td>
                            <td>{!!  Html::link('#', "Respuestas", array('class' => 'btn btn-salmon center-link view-encuesta', 'data-id' => $e["id"], 'data-indice' => $i)) !!}</td>
                            <td>
                                @if($e["fotos"])
                                    {!!  Html::link('#', "Galeria", array('class' => 'btn btn-salmon center-link view-galeria', 'data-id' => $e["id"], 'data-indice' => $i)) !!}
                                @endif
                            </td>
                        </tr>
                    <?php $i++; ?>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div id="div-encuesta" class="col-md-8">

            </div>
            <div class="modal fade in" id="modal-galeria" aria-hidden="false" style="display: none;">
                <div class="modal-dialog" style="width: 60%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Editar pregunta</h4>
                        </div>
                        <div id="div-galeria" class="modal-body" style="overflow: auto;"></div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="col-md-12">
                <h3 class="text-center">No contamos con estudios disponibles</h3>
            </div>
        @endif
    </div>
@endsection

@section('assetsJs')
    {!! HTML::script('assets/js/jquery.dataTables.min.js') !!}
    {!! HTML::script('assets/js/datatables/TableTools.min.js') !!}
    {!! HTML::script('assets/js/dataTables.bootstrap.js') !!}
    {!! HTML::script('assets/js/datatables/jquery.dataTables.columnFilter.js') !!}
    {!! HTML::script('assets/js/datatables/lodash.min.js') !!}
    {!! HTML::script('assets/js/datatables/responsive/js/datatables.responsive.js') !!}
    {!! HTML::script('assets/js/sweetalert.min.js') !!}
    {!! HTML::script('assets/js/fn.datatables.js') !!}
    {!! HTML::script('assets/js/fn/fn.encuestasanalisis.js') !!}
@endsection