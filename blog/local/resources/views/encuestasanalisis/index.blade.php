@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
@endsection

@section('title', 'Encuestas analisis')

@section('content')
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 20px;">
            <h3>Analisis de encuestas</h3>
        </div>
    </div>
    <div class="row">
        @forelse($arrayEncuestas as $e)
            <div class="col-sm-3">
                <?php if ($e["porcentaje"] < 35): $avance = "tile-red"; elseif ($e["porcentaje"] < 99): $avance = "tile-orange"; else: $avance = "tile-green"; endif; ?>
                <div class="tile-progress <?= $avance ?> ">
                    <div class="tile-header" style="padding-bottom: 20px;">
                        <h4 style="color: #fff">{{ $e["encuesta"] }}</h4>
                        <span>{{ $e["cliente"] }}</span>
                        <span class="text-right">Inicio: {{ $e["fechaInicio"] }}</span>
                        <span class="text-right">Fin: {{ $e["fechaFin"] }}</span>
                    </div>

                    <div class="tile-progressbar">
                        <span data-fill="{{ $e["porcentaje"] }}%" style="width: {{ $e["porcentaje"] }}%;"></span>
                    </div>

                    <div class="tile-footer">
                        <h4>
                            <span class="pct-counter">{{ $e["porcentaje"] }}</span>% avance
                        </h4>

                        <span>{{ $e["numeroEncuestas"] }} de {{ $e["numero"] }} encuestas</span>
                    </div>
                    <div class="col-md-12" style="padding: 10px;">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-6">
                                <a href="{{ URL::to('encuestasanalisis/encuestas')."/".$e["id"] }}" class="btn btn-white" style="width: 100%;"><i class="fa fa-file-text"></i> Encuestas </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <div class="col-md-12">
                <h3 class="text-center">No contamos con estudios disponibles</h3>
            </div>
        @endforelse
    </div>
@endsection

@section('assetsJs')
    {!! HTML::script('assets/js/fn/fn.encuestasanalisis.js') !!}

@endsection