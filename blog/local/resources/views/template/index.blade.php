<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Hans" />
        <meta name="author" content="" />

        <title>Hans - @yield('title')</title>


        {!! HTML::style('assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css') !!}
        {!! HTML::style('assets/css/font-icons/entypo/css/entypo.css') !!}
        {!! HTML::style('assets/css/font-icons/font-awesome/css/font-awesome.min.css') !!}
        {!! HTML::style('http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic') !!}
        {!! HTML::style('assets/css/bootstrap.css') !!}
        {!! HTML::style('assets/css/neon-core.css') !!}
        {!! HTML::style('assets/css/neon-theme.css') !!}
        {!! HTML::style('assets/css/neon-forms.css') !!}
        {!! HTML::style('assets/css/custom.css') !!}
        {!! HTML::style('assets/css/style.css') !!}
                <!-- Scripts for Section -->
        @section('assetsCss')
        @show
        {!! HTML::script('assets/js/jquery-1.11.0.min.js') !!}

        <script>$.noConflict();</script>
        <script>var urlBase = '{!! url('/') !!}'; </script>

        <!--[if lt IE 9]>{!! HTML::script('assets/js/ie8-responsive-file-warning.js') !!}<![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        {!! HTML::script('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') !!}
        {!! HTML::script('https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js') !!}
        <![endif]-->
    </head>
    <body class="page-body" data-url="http://neon.dev">

    <div class="page-container sidebar-collapsed">
        <div class="sidebar-menu">
            <div class="sidebar-menu-inner">
                <header class="logo-env">
                    <!-- logo collapse icon -->
                    <div class="sidebar-collapse">
                        <a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                            <i class="entypo-menu"></i>
                        </a>
                    </div>
                    <!-- logo -->

                    <div class="logo">
                        <a href="index.html">
                            <img src="{{ asset('assets/images/imarkers.png') }}" style="width: 75%; margin-top: -10px;" alt="" />
                        </a>
                    </div>

                    <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                    <div class="sidebar-mobile-menu visible-xs">
                        <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                            <i class="entypo-menu"></i>
                        </a>
                    </div>
                </header>
                <?php  //var_dump(session('modulosMenu')); ?>
                <ul id="main-menu" class="main-menu">
                    <?php $modulos  =   session('modulosMenu'); ?>
                    <?php foreach($modulos as $m): ?>
                    <li class="root-level has-sub">
                        <a href="#">
                            <i class="<?= $m->catModulos->icon ?>"></i>
                            <span class="title"><?= $m->catModulos->modulo ?></span>
                        </a>
                        <ul>
                            <?php $secciones  =   session('seccionesMenu'); ?>
                            <?php foreach($secciones as $key => $seccion): ?>
                                <?php if($key == $m->catModulos->id): ?>
                                    <?php foreach($seccion as $s): ?>
                                        <li>
                                            <a href="{{ URL::to($s->url) }}"><?= $s->nombre ?></a>
                                        </li>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                    <?php  endforeach; ?>
                    <li>
                        <a href="{{ URL::to('logout') }}">
                            <i class="entypo-logout right"></i>
                            <span class="title"> Salir</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-content">

            <div class="row">

                <div class="col-md-12 col-sm-8 clearfix">
                    <ul class="user-info pull-right pull-none-xsm">
                        <li class="profile-info dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="{{ asset('assets/images/logomini.png') }}" alt="" class="img-circle" width="44" />
                                <?php $tipodeUsuario  =   session('tipoDeUsuario'); ?>
                                <b>{{  Auth::user()->nombre." ".Auth::user()->apellidos }}</b> | {{ $tipodeUsuario->nombre }}
                            </a>

                            <ul class="dropdown-menu">

                                <li class="caret"></li>

                                <li>
                                    <a href="extra-timeline.html">
                                        <i class="entypo-user"></i>
                                        Edit Profile
                                    </a>
                                </li>

                                <li>
                                    <a href="mailbox.html">
                                        <i class="entypo-mail"></i>
                                        Inbox
                                    </a>
                                </li>

                                <li>
                                    <a href="extra-calendar.html">
                                        <i class="entypo-calendar"></i>
                                        Calendar
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <i class="entypo-clipboard"></i>
                                        Tasks
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- LogOut -->



            </div>
            <hr>
            <div style="margin-top: 0px;">
                @yield('content')
            </div>

            <footer class="main">
                &copy; 2016 <strong>iMarkers Agency</strong></a>
            </footer>
        </div>
    </div>

    <!-- Demo Settings -->
    {!! HTML::script('assets/js/gsap/main-gsap.js') !!}
    {!! HTML::script('assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js') !!}
    {!! HTML::script('assets/js/bootstrap.js') !!}
    {!! HTML::script('assets/js/joinable.js') !!}
    {!! HTML::script('assets/js/resizeable.js') !!}
    {!! HTML::script('assets/js/neon-api.js') !!}
    {!! HTML::script('assets/js/parsley.js') !!}


            <!-- JavaScripts initializations and stuff -->
    {!! HTML::script('assets/js/neon-custom.js') !!}

            <!-- Demo Settings -->
    {!! HTML::script('assets/js/neon-demo.js') !!}

    <!-- Scripts for Section -->
    @section('assetsJs')
    @show

    </body>
</html>