@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
@endsection

@section('title', 'Encuestas')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered datatable" id="table-1">
                <thead>
                <tr>
                    <th>Encuesta</th>
                    <th>Asignadas</th>
                    <th width="15%"></th>
                </tr>
                </thead>
                <tbody>
                @forelse ($encuestas as $encuesta)
                    <tr>
                        <td>{{ $encuesta->nombre }}</td>
                        <td>100/150</td>
                        <td>{!!  Html::link('asignacion/'.Crypt::encrypt($encuesta->id), "Asignar usuarios", array('class' => 'btn btn-primary center-link')) !!}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7">{{ 'No existen encuestas cargadas' }}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('assetsJs')
    {!! HTML::script('assets/js/jquery.dataTables.min.js') !!}
    {!! HTML::script('assets/js/datatables/TableTools.min.js') !!}
    {!! HTML::script('assets/js/dataTables.bootstrap.js') !!}
    {!! HTML::script('assets/js/datatables/jquery.dataTables.columnFilter.js') !!}
    {!! HTML::script('assets/js/datatables/lodash.min.js') !!}
    {!! HTML::script('assets/js/datatables/responsive/js/datatables.responsive.js') !!}
    {!! HTML::script('assets/js/sweetalert.min.js') !!}
    {!! HTML::script('assets/js/fn.datatables.js') !!}

@endsection