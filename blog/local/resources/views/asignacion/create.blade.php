@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
@endsection

@section('title', 'Encuestas')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Administrador de coordinadores</h3>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-md-offset-10">
            {!!  Html::link('encuestas/', " Regresar", array('class' => 'btn btn-blue pull-right')) !!}
        </div>
    </div>
    <h4>[ {{ $encuesta->nombre  }} ]</h4>
    @if(in_array("2", $arrayPermisos))
        {!! Form::open([
            'id'        =>  'asignacion_form',
            'route'     =>  'asignacion.store',
            'method'    =>  'POST'
        ]) !!}
        <div class="row">
            <div class="col-md-12">
                <div id="errors" class="alert alert-danger" style="display: none;"></div>
            </div>
            <div class="col-md-12">
                {!! Form::hidden('encuesta_id', Crypt::encrypt($id)) !!}
                {!! Form::label('usuario_id', 'Seleccione coordinador:', ['class' => 'control-label']) !!}
                {!! Form::select('usuario_id', $coordinadores, null, ['id' => 'usuario_id', 'class' => 'form-control', 'placeholder' => '-- Seleccione --']) !!}
            </div>
            <div class="col-md-12">
                {!!  Form::submit("Agregar coordinador", array('id' => 'agregar_coordinador', 'class' => 'btn btn-blue pull-right', 'style' => 'margin-top: 20px;')) !!}
            </div>
        </div>
        {!! Form::close() !!}
    @endif
    <div id="div_coordinadores" class="row" style="margin-top: 50px;">
        <div class="col-md-12">
            <table class="table table-bordered datatable" id="table-1">
                <thead>
                <tr>
                    <th width="80%">Coordinador</th>
                    @if(in_array("1", $arrayPermisosEncuestadores))
                        <th></th>
                    @endif
                    @if(in_array("4", $arrayPermisos))
                        <th></th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach ($usuarios as $us)
                    <tr>
                        <td>{{ $us["nombre"] }}</td>
                        @if(in_array("1", $arrayPermisosEncuestadores))
                            <td>{!!  Html::link('operacion/'.Crypt::encrypt($us["id"]).'', "Asignar encuestadores", array('class' => 'btn btn-primary center-link asignar_encuestadores')) !!}</td>
                        @endif
                        @if(in_array("4", $arrayPermisos))
                            <td>{!! Form::button( ' Eliminar', ['type' => 'submit', 'class' => 'btn btn-red btn-sm delete-coordinator', 'id' => 'btnDeleteCoordinator', 'data-id' => Crypt::encrypt($us["id"]) ] ) !!}</td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('assetsJs')
    {!! HTML::script('assets/js/jquery.dataTables.min.js') !!}
    {!! HTML::script('assets/js/datatables/TableTools.min.js') !!}
    {!! HTML::script('assets/js/dataTables.bootstrap.js') !!}
    {!! HTML::script('assets/js/datatables/jquery.dataTables.columnFilter.js') !!}
    {!! HTML::script('assets/js/datatables/lodash.min.js') !!}
    {!! HTML::script('assets/js/datatables/responsive/js/datatables.responsive.js') !!}
    {!! HTML::script('assets/js/sweetalert.min.js') !!}
    {!! HTML::script('assets/js/fn.datatables.js') !!}
    {!! HTML::script('assets/js/fn/fn.asignacion.js') !!}

@endsection