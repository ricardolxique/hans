<div class="col-md-6">
    <table class="table table-bordered datatable" id="table-1">
        <thead>
        <tr>
            <th width="80%">Coordinador</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @forelse ($usuarios as $us)
            <tr>
                <td>{{ $us->usuarios->nombre }}</td>
                <td>{!! Html::link('asignacion/encuestadores/'.Crypt::encrypt($us->id).'', "Asignar encuestadores", array('class' => 'btn btn-primary center-link')) !!}</td>
                <td>{!! Form::button( ' Eliminar', ['type' => 'submit', 'class' => 'btn btn-red btn-sm delete-coordinator', 'id' => 'btnDeleteCoordinator', 'data-id' => Crypt::encrypt($us->id) ] ) !!}</td>
            </tr>
        @empty
            <tr>
                <td colspan="3">{{ 'No existen coordinadores cargados' }}</td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>
<div id="show_encuestadores" class="col-md-6">

</div>