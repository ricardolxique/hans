@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
@endsection

@section('title', 'Editar cliente')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4>Editar cliente</h4>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-md-offset-10">
            {!!  Html::link('clientes/', " Regresar", array('class' => 'btn btn-blue pull-right')) !!}
        </div>
    </div>
    <div class="row" style="margin-top: 25px;">
        <div class="col-md-12">
            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
            @if(Session::has('status'))
                <div class="alert alert-success">
                    {{ Session::get('status') }}
                </div>
            @endif
            {!! Form::open([
                'method'=> 'PATCH',
                'route' => ['clientes.update', Crypt::encrypt($cliente->id)]
            ]) !!}

                <div class="form-group">
                    {!! Form::label('nombre', 'Nombre:', ['class' => 'control-label']) !!}
                    {!! Form::text('nombre', $cliente->nombre, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('apellidos', 'Apellidos:', ['class' => 'control-label']) !!}
                    {!! Form::text('apellidos', $cliente->apellidos, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('numero_telefonico', 'Numero telefonico:', ['class' => 'control-label']) !!}
                    {!! Form::text('numero_telefonico', $cliente->numero_telefonico, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('email', 'Correo electronico:', ['class' => 'control-label']) !!}
                    {!! Form::text('email', $cliente->email, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('empresa', 'Empresa:', ['class' => 'control-label']) !!}
                    {!! Form::text('empresa', $cliente->empresa, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('rfc', 'R.F.C:', ['class' => 'control-label']) !!}
                    {!! Form::text('rfc', $cliente->rfc, ['class' => 'form-control']) !!}
                </div>
                <hr>
                <div class="form-group">
                    {!! Form::label('calle', 'Calle:', ['class' => 'control-label']) !!}
                    {!! Form::text('calle', $cliente->calle, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('numero_interior', 'Número interior:', ['class' => 'control-label']) !!}
                    {!! Form::text('numero_interior', $cliente->numero_interior, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('numero_exterior', 'Número exterior:', ['class' => 'control-label']) !!}
                    {!! Form::text('numero_exterior', $cliente->numero_exterior, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('colonia', 'Colonia:', ['class' => 'control-label']) !!}
                    {!! Form::text('colonia', $cliente->colonia, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('delegacion', 'Delegación o municipio:', ['class' => 'control-label']) !!}
                    {!! Form::text('delegacion_municipio', $cliente->delegacion_municipio, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('estado', 'Estado:', ['class' => 'control-label']) !!}
                    {!! Form::select('idEstado', $estados, $cliente->idEstado, ['class' => 'form-control', 'placeholder' => '-- Seleccione --']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('codigo_postal', 'Codigo postal:', ['class' => 'control-label']) !!}
                    {!! Form::text('codigo_postal', $cliente->codigo_postal, ['class' => 'form-control', 'maxlength' => '5']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::submit('Actualizar cliente', ['class' => 'btn btn-primary pull-left']) !!}
                </div>
            {!! Form::close() !!}
            @if(in_array("4", $arrayPermisos))
                {!! Form::open(['method' => 'DELETE', 'id' => 'formDeleteUser', 'route' => ['clientes.destroy', Crypt::encrypt($cliente->id)]]) !!}
                    <div class="col-md-6">
                        {!! Form::button( 'Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger pull-right delete-client', 'id' => 'btnDeleteClient', 'data-id' => Crypt::encrypt($cliente->id) ] ) !!}
                    </div>
                {!! Form::close() !!}
            @endif
        </div>
    </div>
@endsection
@section('assetsJs')
    {!! HTML::script('assets/js/sweetalert.min.js') !!}
    {!! HTML::script('assets/js/fn/fn.clientes.js') !!}
@endsection