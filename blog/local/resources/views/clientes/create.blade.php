@extends('template/index')

@section('title', 'Agregar cliente')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4>Agregar cliente</h4>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-md-offset-10">
            {!!  Html::link('clientes/', " Regresar", array('class' => 'btn btn-blue pull-right')) !!}
        </div>
    </div>
    <div class="row" style="margin-top: 25px;">
        <div class="col-md-12">
            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
            @if(Session::has('status'))
                <div class="alert alert-success">
                    {{ Session::get('status') }}
                </div>
            @endif
            {!! Form::open([
                'route' => 'clientes.store'
            ]) !!}

                <div class="form-group">
                    {!! Form::label('nombre', 'Nombre:', ['class' => 'control-label']) !!}
                    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('apellidos', 'Apellidos:', ['class' => 'control-label']) !!}
                    {!! Form::text('apellidos', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('numero_telefonico', 'Numero telefonico:', ['class' => 'control-label']) !!}
                    {!! Form::text('numero_telefonico', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('email', 'Correo electronico:', ['class' => 'control-label']) !!}
                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('empresa', 'Empresa:', ['class' => 'control-label']) !!}
                    {!! Form::text('empresa', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('rfc', 'R.F.C:', ['class' => 'control-label']) !!}
                    {!! Form::text('rfc', null, ['class' => 'form-control']) !!}
                </div>
                <hr>
                <div class="form-group">
                    {!! Form::label('calle', 'Calle:', ['class' => 'control-label']) !!}
                    {!! Form::text('calle', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('numero_interior', 'Número interior:', ['class' => 'control-label']) !!}
                    {!! Form::text('numero_interior', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('numero_exterior', 'Número exterior:', ['class' => 'control-label']) !!}
                    {!! Form::text('numero_exterior', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('colonia', 'Colonia:', ['class' => 'control-label']) !!}
                    {!! Form::text('colonia', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('delegacion', 'Delegación o municipio:', ['class' => 'control-label']) !!}
                    {!! Form::text('delegacion_municipio', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('estado', 'Estado:', ['class' => 'control-label']) !!}
                    {!! Form::select('idEstado', $estados, null, ['class' => 'form-control', 'placeholder' => '-- Seleccione --']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('codigo_postal', 'Codigo postal:', ['class' => 'control-label']) !!}
                    {!! Form::text('codigo_postal', null, ['class' => 'form-control', 'maxlength' => '5']) !!}
                </div>
            {!! Form::submit('Crear nuevo cliente', ['class' => 'btn btn-primary pull-right']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection