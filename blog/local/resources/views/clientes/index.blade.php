@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
@endsection

@section('title', 'Clientes')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Administrador de clientes</h3>
            <hr>
        </div>
    </div>
    @if(in_array("2", $arrayPermisos))
        <div class="row">
            <div class="col-md-2 col-md-offset-10">
                {!!  Html::link('clientes/create', "Crear Cliente", array('class' => 'btn btn-blue pull-right')) !!}
            </div>
        </div>
    @endif
    <br/>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered" id="table-1">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Empresa</th>
                    <th>Dirección</th>
                    <th>RFC</th>
                    @if(in_array("3", $arrayPermisos))
                        <th></th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach ($clientes as $cliente)
                    <tr>
                        <td>{{ $cliente->nombre." ".$cliente->apellidos  }}</td>
                        <td>{{ $cliente->empresa  }}</td>
                        <td>
                            {{
                                $cliente->calle." "
                                .$cliente->numero_interior." "
                                .$cliente->numero_exterior." "
                                .$cliente->colonia." "
                                .$cliente->delegacion_municipio." "
                                .$cliente->estados->estado." "
                                .$cliente->codigo_postal
                             }}
                        </td>
                        <td>
                            {{ $cliente->rfc  }}
                        </td>
                        @if(in_array("3", $arrayPermisos))
                            <td>{!!  Html::link('clientes/'.Crypt::encrypt($cliente->id).'/edit', "Editar", array('class' => 'btn btn-primary center-link')) !!}</td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('assetsJs')
    {!! HTML::script('assets/js/jquery.dataTables.min.js') !!}
    {!! HTML::script('assets/js/datatables/TableTools.min.js') !!}
    {!! HTML::script('assets/js/dataTables.bootstrap.js') !!}
    {!! HTML::script('assets/js/datatables/jquery.dataTables.columnFilter.js') !!}
    {!! HTML::script('assets/js/datatables/lodash.min.js') !!}
    {!! HTML::script('assets/js/datatables/responsive/js/datatables.responsive.js') !!}
    {!! HTML::script('assets/js/sweetalert.min.js') !!}
    {!! HTML::script('assets/js/fn.datatables.js') !!}

@endsection