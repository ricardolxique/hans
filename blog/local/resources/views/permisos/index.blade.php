@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
@endsection

@section('title', 'Perfiles')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Administrador de permisos</h3>
            <h4>Selecciona un módulo para comenzar a asignar permisos</h4>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            {!! Form::label('modulo_id', 'Módulo', ['class' => 'control-label']) !!}
            {!! Form::select('modulo_id', $modulo, null, ['id' => 'modulo_id', 'class' => 'form-control', 'placeholder' => '-- Seleccione --']) !!}
        </div>
        <div id="combo_secciones" class="col-md-6">

        </div>
    </div>
    <hr>
    <div class="row">
        <div id="combo_perfiles" class="col-md-12">

        </div>
    </div>

@endsection

@section('assetsJs')
    {!! HTML::script('assets/js/jquery.dataTables.min.js') !!}
    {!! HTML::script('assets/js/datatables/TableTools.min.js') !!}
    {!! HTML::script('assets/js/dataTables.bootstrap.js') !!}
    {!! HTML::script('assets/js/datatables/jquery.dataTables.columnFilter.js') !!}
    {!! HTML::script('assets/js/datatables/lodash.min.js') !!}
    {!! HTML::script('assets/js/datatables/responsive/js/datatables.responsive.js') !!}
    {!! HTML::script('assets/js/sweetalert.min.js') !!}
    {!! HTML::script('assets/js/fn.datatables.js') !!}
    {!! HTML::script('assets/js/fn/fn.permisos.js') !!}
@endsection