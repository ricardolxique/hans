@if(count($permisos_perfiles) > 0)
    <p>Para cambiar los permisos en los perfiles, da click sobre el icono y confirma la acción para actualizar el permiso en el perfil</p>
@else
    <p>Para asignar los permisos en los perfiles, selecciona el permiso para cada perfil, los cambios se guardaran automaticamente</p>
@endif
@if(count($permisos_perfiles) > 0)
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-md-3 col-md-offset-9">
            <button type="button" id="change-permisons" class="btn btn-info pull-right">Cambiar permisos</button>
        </div>
    </div>
@endif
<table class="table table-bordered datatable" id="table-1">
    <thead>
    <tr>
        <th width="30%"><h5 class="text-center">Perfil</h5></th>
        @foreach($permisos as $p)
        <th>
            <h5 class="text-center">{{ $p->titulo  }}</h5>
        </th>
        @endforeach
    </tr>
    </thead>
    <tbody>
        @foreach($perfiles as $pe)
            <tr>
                <td width="30%"><p class="text-center">{{ $pe->nombre  }}</p></td>
                @foreach($permisos as $p)
                    <td>
                    @forelse($permisos_perfiles as $pp)
                        @if ($pe->id == $pp["perfil"])
                            @if ($p->id == $pp["permiso"])
                                @if ($pp["result"] == true)
                                    <p class="text-center show-icon-permissons" style="color: #46cd43;"><i class="fa fa-2x fa-check-circle"></i> </p>
                                @else
                                     <p class="text-center show-icon-permissons" style="color: #ff3030;"><i class="fa fa-2x fa-times-circle"></i> </p>
                                @endif
                                <p class="text-center">{!! Form::checkbox(Crypt::encrypt($p->id), Crypt::encrypt($pe->id), $pp["result"], array("style" => "height: 30px; font-size: 20px; display: none;", "class" => "change-check-permissons permiso_value")) !!}</p>
                            @endif
                        @endif
                    @empty
                         <p class="text-center">{!! Form::checkbox(Crypt::encrypt($p->id), Crypt::encrypt($pe->id), false, array("style" => "height: 30px; font-size: 20px;", "class" => "permiso_value")) !!}</p>
                    @endforelse
                    </td>
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>