<div class="row">
    <div class="col-md-12">
        @if(!empty($preguntas) && count($preguntas) > 0)
            <div class="col-md-12">
                <div class="col-md-3"><a href='#' id='select-all'>Seleccionar todo</a></div>
                <div class="col-md-3"><a href='#' id='deselect-all'>Deseleccionar todo</a></div>
            </div>
            <br/>
            <select multiple="multiple" id="my-select" name="my-select[]">
                @foreach($preguntas as $p)
                    <option value='{{ Crypt::encrypt($p->id)  }}'>{{ $p->pregunta  }}</option>
                @endforeach
            </select>
        @else
            <h4 class="text-center">Este estudio no cuenta con preguntas disponibles.</h4>
        @endif
    </div>
</div>