<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered datatable" id="table-1" style="width: 100%">
            <thead>
            <tr>
                @foreach($resultPregunta as $p)
                <th>{{ $p["pregunta"]  }}</th>
                @endforeach
            </tr>
            </thead>
            <tbody>
                @foreach($encuestasTotales as $et)
                <tr>
                    @foreach($resultPregunta as $p)
                        @foreach($resultado as $r)
                            @if($p["id"] == $r->id_pregunta && $et->id == $r->encuestas_contestadas_id)
                                <td>{{ $r->respuesta  }}</td>
                            @endif
                        @endforeach
                    @endforeach
                </tr>
                @endforeach
            </tbody>
         </table>
    </div>
</div>