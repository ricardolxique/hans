@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
    {!! HTML::style("assets/js/select2/select2-bootstrap.css") !!}
    {!! HTML::style("assets/js/select2/select2.css") !!}
@endsection

@section('title', 'Encuestas')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Generador de reportes</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p class="text-left">Seleccione una encuesta y a continuación las preguntas de tu elección para generar el reporte en "xls".</p>
        </div>
        <div class="col-md-8">
            {!! Form::select('encuesta_id', $encuestas, null, ['id' => 'encuesta_id', 'class' => 'form-control', 'placeholder' => '-- Seleccione --']) !!}
        </div>
        <br/><br/><br/>
        <div id="load_preguntas" style="display: none;">
            <div id="combo_preguntas" class="col-md-10 col-md-offset-2">

            </div>
            <div class="col-md-2 col-md-offset-2">
                <input type="button" id="generar_reporte" class="btn btn-green btn-lg" value="Generar reporte" style="margin-top: 20px;">
            </div>
            <div class="col-md-2 col-md-offset-4 combo_download" style="display: none;">
                <a id="link_download" target="_blank" class="btn btn-blue btn-lg" href="#" style="margin-top: 20px;">Descargar reporte</a>
            </div>
            <div class="col-md-2 col-md-offset-4 combo_download" style="display: none;">
                <a id="link_download_sav" target="_blank" class="btn btn-blue btn-lg" href="#" style="margin-top: 20px;">Descargar reporte SAV</a>
            </div>
        </div>
    </div>
    <div id="loading-canvas" class="canvas_back" tabindex="-1" style="opacity: 1.08; display: none;"></div>
@endsection
@section('assetsJs')
    {!! HTML::script('assets/js/fn/fn.reportes.js') !!}
    {!! HTML::script('assets/js/jquery.multi-select.js') !!}
    {!! HTML::script('assets/js/sweetalert.min.js') !!}
@endsection