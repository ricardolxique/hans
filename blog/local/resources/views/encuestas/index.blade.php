@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
@endsection

@section('title', 'Encuestas')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Administrador de estudios</h3>
        </div>
    </div>
    <div class="row">
        @if(in_array("2", $arrayPermisos))
            <div class="col-md-4 col-md-offset-4">
                {!!  Html::link('encuestas/create', "+ Crear estudio", array('class' => 'btn btn-success center-link')) !!}
            </div>
        @endif
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12">
            <table class="table datatable" id="table-1">
                <thead>
                <tr>
                    <th><p>Estudio</p></th>
                    <th><p>Cliente</p></th>
                    <th><p>Total</p></th>
                    <th><p>Fecha inicio</p></th>
                    <th><p>Fecha fin</p></th>
                    <th><p class="text-center">Supervisado</p></th>
                    <th style="width: 15%;"></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($encuestas as $encuesta)
                    <tr>
                        <td>{{ $encuesta->nombre }}</td>
                        <td>{{ $encuesta->clientes->empresa }}</td>
                        <td>{{ $encuesta->numeroEncuestas  }}</td>
                        <?php list($fecha, $hora) = explode(" ", $encuesta->fechaInicio); ?>
                        <td>{{ $fecha }}</td>
                        <?php list($fecha, $hora) = explode(" ", $encuesta->fechaFin); ?>
                        <td>{{ $fecha }}</td>
                        <td><?php if ($encuesta->tipo_estudio_id == 2): ?><p class="text-center" style="color: #ee5f5b;"><i class="fa fa-2x fa-crosshairs"></i></p> <?php endif; ?></td>
                        <td class="font-row-options" >
                            <select class="acciones form-control" title="- Acciones -" style="text-align: center; text-align-last: center;width: 100%">
                                <option value="">- Acción -</option>
                                @if(in_array("3", $arrayPermisos))
                                    <option value="{{ URL::to('encuestas/')."/".Crypt::encrypt($encuesta->id).'/edit' }}">Editar</option>
                                @endif
                                @if(in_array("1", $arrayPermisosCoordinadores))
                                    <option value="{{ URL::to('asignacion/')."/".Crypt::encrypt($encuesta->id) }}">Asignar</option>
                                @endif
                                @if($encuesta->tipo_estudio_id == 2)
                                    @if(in_array("1", $arrayPermisosSupervision))
                                        <option value="{{ URL::to('revision/')."/".Crypt::encrypt($encuesta->id) }}">Validar encuestas</option>
                                    @endif
                                @endif
                            </select>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('assetsJs')
    {!! HTML::script('assets/js/jquery.dataTables.min.js') !!}
    {!! HTML::script('assets/js/datatables/TableTools.min.js') !!}
    {!! HTML::script('assets/js/dataTables.bootstrap.js') !!}
    {!! HTML::script('assets/js/datatables/jquery.dataTables.columnFilter.js') !!}
    {!! HTML::script('assets/js/datatables/lodash.min.js') !!}
    {!! HTML::script('assets/js/datatables/responsive/js/datatables.responsive.js') !!}
    {!! HTML::script('assets/js/sweetalert.min.js') !!}
    {!! HTML::script('assets/js/fn.datatables.js') !!}
    {!! HTML::script('assets/js/fn/fn.encuestas.js') !!}

@endsection