@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
@endsection

@section('title', 'Editar encuesta')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4>Editar encuesta</h4>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-md-offset-10">
            {!!  Html::link('encuestas/', " Regresar", array('class' => 'btn btn-blue pull-right')) !!}
        </div>
    </div>
    <div class="row" style="margin-top: 25px;">
        <div class="col-md-12">
            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
            @if(Session::has('status'))
                <div class="alert alert-success">
                    {{ Session::get('status') }}
                </div>
            @endif
            {!! Form::open([
                'method'=> 'PATCH',
                'route' => ['encuestas.update', Crypt::encrypt($encuesta->id)]
            ]) !!}

            <div class="form-group">
                {!! Form::label('nombre', 'Nombre:', ['class' => 'control-label']) !!}
                {!! Form::text('nombre', $encuesta->nombre, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('numeroEncuestas', 'Numero de encuestas:', ['class' => 'control-label']) !!}
                {!! Form::text('numeroEncuestas', $encuesta->numeroEncuestas, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('fechaInicio', 'Fecha de inicio:', ['class' => 'control-label']) !!}
                <div class="input-group">
                    {!! Form::text('fechaInicio', $encuesta->fechaInicio, ['class' => 'form-control datepicker', 'data-format' => 'yyyy-mm-dd']) !!}
                    <div class="input-group-addon"><a href="#"><i class="entypo-calendar"></i></a></div>
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('fechaFin', 'Fecha de termino:', ['class' => 'control-label']) !!}
                <div class="input-group">
                    {!! Form::text('fechaFin', $encuesta->fechaFin, ['class' => 'form-control datepicker', 'data-format' => 'yyyy-mm-dd']) !!}
                    <div class="input-group-addon"><a href="#"><i class="entypo-calendar"></i></a></div>
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('observaciones', 'Observaciones:', ['class' => 'control-label']) !!}
                {!! Form::textarea('observaciones', $encuesta->observaciones, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('idCliente', 'Cliente:', ['class' => 'control-label']) !!}
                {!! Form::select('idCliente', $clientes, $encuesta->idCliente, ['class' => 'form-control', 'placeholder' => '-- Seleccione --']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('tipo_estudio_id', 'Tipo de estudio:', ['class' => 'control-label']) !!}
                {!! Form::select('tipo_estudio_id', $arrayTipoEstudios, $encuesta->tipo_estudio_id, ['class' => 'form-control', 'placeholder' => '-- Seleccione --']) !!}
            </div>
            <div class="col-md-6">
                {!! Form::submit('Actualizar encuesta', ['class' => 'btn btn-primary pull-left']) !!}
            </div>
            {!! Form::close() !!}
            @if(in_array("4", $arrayPermisos))
                {!! Form::open(['method' => 'DELETE', 'id' => 'formDeletePoll', 'route' => ['encuestas.destroy', Crypt::encrypt($encuesta->id)]]) !!}
                <div class="col-md-6">
                    {!! Form::button( 'Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger pull-right delete-poll', 'id' => 'btnDeletePoll', 'data-id' => Crypt::encrypt($encuesta->id) ] ) !!}
                </div>
                {!! Form::close() !!}
            @endif
        </div>
    </div>
@endsection
@section('assetsJs')
    {!! HTML::script('assets/js/bootstrap-datepicker.js') !!}
    {!! HTML::script('assets/js/sweetalert.min.js') !!}
    {!! HTML::script('assets/js/fn/fn.encuestas.js') !!}
@endsection