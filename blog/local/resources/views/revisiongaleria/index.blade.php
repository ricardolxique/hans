@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
    {!! HTML::style('assets/css/style.css') !!}
@endsection

@section('title', 'Encuestas')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Validación de galeria</h3>
            <h4>{{ $encuesta->nombre }}</h4>
        </div>
    </div>
    <div id="grid-fotos-validacion" class="row">
        <?php $i = 1  ?>
        @forelse($fotosEncuestas as $fe)
        <div id="div-<?= $i ?>" class="col-md-4 div-img-gray">
            <div class="row">
                <div class="col-md-12" style="margin-bottom: 5px;">
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#" class="btn btn-red rechaza-encuesta" data-div="{{ $i }}" data-id="{{ $fe["id_foto"] }}" style="width: 100%;">
                                Rechazar
                            </a>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-5">
                            <a href="#" class="btn btn-success aprueba-encuesta" data-div="{{ $i }}" data-id="{{ $fe["id_foto"] }}" style="width: 100%;">
                                Aprobar
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <img src="data:image/png;base64,{{ $fe["imagen"] }}" style="width: 100%"; >
                </div>
                <div class="col-md-6 col-md-offset-3">
                    <a href="#" data-src="data:image/png;base64,{{ $fe["imagen"] }}" class="view-image"><h5 class="text-center p-valida-img"><i class="fa fa-search"></i> Ver imagen</h5></a>
                </div>
            </div>
        </div>
        <?php $i++ ?>
        @empty
            <div class="col-md-12">
                <h4>No hay fotos</h4>
            </div>
        @endforelse
    </div>
    <div class="modal fade in" id="modalViewImage" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <br/>
                </div>
                <div class="modal-body">
                    <img id="img-preview" src="" class="img-responsive center-img">
                </div>
            </div>
        </div>
    </div>
@endsection
@section('assetsJs')
    {!! HTML::script('assets/js/toastr.js') !!}
    {!! HTML::script('assets/js/fn/fn.revisiongaleria.js') !!}
@endsection