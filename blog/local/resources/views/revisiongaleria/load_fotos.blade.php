@foreach($fotosEncuestas as $fe)
    <div id="div-{{ $div }}" class="col-md-4 div-img-gray">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px;">
                <div class="row">
                    <div class="col-md-5">
                        <a href="#" class="btn btn-red rechaza-encuesta" data-div="{{ $div }}" data-id="{{ $fe["id_foto"] }}" style="width: 100%;">
                            Rechazar
                        </a>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-5">
                        <a href="#" class="btn btn-success aprueba-encuesta" data-div="{{ $div }}" data-id="{{ $fe["id_foto"] }}" style="width: 100%;">
                            Aprobar
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <img src="data:image/png;base64,{{ $fe["imagen"] }}" style="width: 100%"; >
            </div>
            <div class="col-md-6 col-md-offset-3">
                <a href="#" data-src="data:image/png;base64,{{ $fe["imagen"] }}" class="view-image"><h5 class="text-center p-valida-img"><i class="fa fa-search"></i> Ver imagen</h5></a>
            </div>
        </div>
    </div>
@endforeach