@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
@endsection

@section('title', 'Secciones')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Secciones del menú [ {{ $modulo->modulo  }}  ]</h3>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-md-offset-8">
            {!!  Html::link('secciones/create', "Crear sección", array('class' => 'btn btn-blue pull-right')) !!}
        </div>
        <div class="col-md-2">
            {!!  Html::link('modulos', "Regresar", array('class' => 'btn btn-red pull-right')) !!}
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered" id="table-1">
                <thead>
                <tr>
                    <th>Sección</th>
                    <th width="10%"></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($secciones as $seccion)
                    <tr>
                        <td>{{ $seccion->nombre  }}</td>
                        <td>{!!  Html::link('secciones/'.Crypt::encrypt($seccion->id).'/edit', "Editar", array('class' => 'btn btn-primary center-link')) !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('assetsJs')
    {!! HTML::script('assets/js/jquery.dataTables.min.js') !!}
    {!! HTML::script('assets/js/datatables/TableTools.min.js') !!}
    {!! HTML::script('assets/js/dataTables.bootstrap.js') !!}
    {!! HTML::script('assets/js/datatables/jquery.dataTables.columnFilter.js') !!}
    {!! HTML::script('assets/js/datatables/lodash.min.js') !!}
    {!! HTML::script('assets/js/datatables/responsive/js/datatables.responsive.js') !!}
    {!! HTML::script('assets/js/sweetalert.min.js') !!}
    {!! HTML::script('assets/js/fn.datatables.js') !!}

@endsection