@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
    {!! HTML::style('assets/js/switch/css/bootstrap3/bootstrap-switch.min.css') !!}
@endsection

@section('title', 'Agregar módulo')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4>Actualizar sección</h4>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-md-offset-10">
            {!!  Html::link('secciones/'.Crypt::encrypt($seccion->cat_catalogos_id), " Regresar", array('class' => 'btn btn-blue pull-right')) !!}
        </div>
    </div>
    <div class="row" style="margin-top: 25px;">
        <div class="col-md-12">
            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
            @if(Session::has('status'))
                <div class="alert alert-success">
                    {{ Session::get('status') }}
                </div>
            @endif
            {!! Form::open([
                'method'=> 'PATCH',
                'route' => ['secciones.update', Crypt::encrypt($seccion->id)]
            ]) !!}
            <div class="form-group">
                <div class="col-md-12">
                    {!! Form::label('nombre', 'Nombre de la sección:', ['class' => 'control-label']) !!}
                    {!! Form::text('nombre', $seccion->nombre, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    {!! Form::label('url', 'Url:', ['class' => 'control-label']) !!}
                    {!! Form::text('url', $seccion->url, ['class' => 'form-control']) !!}
                </div>
                <div class="col-md-5">
                    {!! Form::hidden('cat_catalogos_id', Crypt::encrypt($seccion->cat_catalogos_id), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-6" style="margin-top: 10px;">
                {!! Form::submit('Actualizar sección', ['class' => 'btn btn-primary pull-left']) !!}
            </div>
            {!! Form::close() !!}
            {!! Form::open(['method' => 'DELETE', 'id' => 'formDeleteSeccion', 'route' => ['secciones.destroy', Crypt::encrypt($seccion->id)]]) !!}
            <div class="col-md-6" style="margin-top: 10px;">
                {!! Form::button( 'Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger pull-right delete-seccion', 'id' => 'btnDeleteSeccion', 'data-id' => Crypt::encrypt($seccion->id) ] ) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
@section('assetsJs')
    {!! HTML::script('assets/js/sweetalert.min.js') !!}
    {!! HTML::script('assets/js/fn/fn.secciones.js') !!}
@endsection