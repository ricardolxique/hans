@forelse($encuestas as $e)
    <div id="encuesta-{{ $div }}" class="col-md-4">
        <div class="panel panel-default panel-encuesta" style="margin-bottom: 5px; padding: 5px;">
            <div class="row">
                <div class="col-md-5">
                    <a href="#" class="btn btn-red rechaza-encuesta" data-div="{{ $div }}" data-id="{{ $e["id_encuesta_contestada"] }}" style="width: 100%;">
                        Rechazar
                    </a>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-5">
                    <a href="#" class="btn btn-success aprueba-encuesta" data-div="{{ $div }}" data-id="{{ $e["id_encuesta_contestada"] }}" style="width: 100%;">
                        Aprobar
                    </a>
                </div>
            </div>
            <div class="row" style="margin-top: 15px;">
                <div class="col-md-12" style="border-right: 1px solid #FFFFFF;">
                    <p class="text-white text-center">Fecha de emisión: <span style="font-size: 12px; font-weight: 600">{{ $e["fecha_emision"]  }}</span></p>
                </div>
            </div>
            <div class="row" style="margin-top: 5px; padding: 5px;">
                <div class="col-md-4">
                    @if($e["latitud"] != "0" && $e["latitud"] != "0")
                        <p class="text-right text-white"><i class="fa fa-map-marker fa-2x"></i></p>
                    @endif
                </div>
                <div class="col-md-6">
                    @if($e["foto"] != "0")
                        <a href="#" class="btn btn-info btn-icon icon-left validate-galery" data-id="{{ $e["id_encuesta_contestada"] }}" style="width: 100%;">
                            Aprobar fotos <i class="fa fa-camera"></i>
                        </a>
                    @endif
                </div>
            </div>
            <div class="row" style="margin-top: 5px; border-top: 1px solid #f4f4f4; padding: 5px;">
                <div class="col-md-12" style="border-right: 1px solid #FFFFFF;">
                    <ol>
                        @forelse($e["preguntas"] as $pr)
                            <li>
                                <h5 class="text-white">{{ $pr["pregunta"]  }}</h5>
                                <p class="text-white">{{ $pr["respuesta"]  }}</p>
                            </li>
                        @empty
                            <h5 class="text-center text-white">No contamos con preguntas para esta encuesta</h5>
                        @endforelse
                    </ol>
                </div>
            </div>
        </div>
    </div>
@empty
    <div class="col-md-4">

    </div>
@endforelse