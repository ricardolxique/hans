<div class="col-md-12">
    <div class="row">
        <?php $i = 0; ?>
        @foreach($encuestasFotos as $f)
            <div class="col-md-6" style="padding: 10px; background: #F3F3F3;">
                @if($f->estado == 2)
                <div id="aprob-<?php echo $i; ?>" class="col-md-12">
                    <div class="col-md-5">
                        <a href="#" class="btn btn-red rechaza-foto" data-div="{{ $i }}" data-id="{{ Crypt::encrypt($f->id) }}" style="width: 100%;">
                            Rechazar
                        </a>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-5">
                        <a href="#" class="btn btn-success aprueba-foto" data-div="{{ $i }}" data-id="{{ Crypt::encrypt($f->id) }}" style="width: 100%;">
                            Aprobar
                        </a>
                    </div>
                    <br/><br/>
                </div>
                @endif
                <div class="col-md-12">
                    <img src="data:image/png;base64,{{ $f->imagen }}" style="width: 100%"; >
                    <div class="carousel-caption">
                        {{ $f->nombre }}
                    </div>
                </div>
            </div>
            <?php $i++; ?>
        @endforeach
    </div>
</div>