@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
    <style>
        .panel-encuesta {
            background: #1e3140;
            border: 1px solid #ffffff;
        }
        .text-white {
            color: #FFFFFF;
        }
    </style>
@endsection

@section('title', 'Validación de encuestas')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Validación de encuestas</h3>
            <h4>{{ $encuesta->nombre }}</h4>
            <p class="text-justify">Puede aprobar o rechazar encuestas, si la encuesta muestra la opcion "Aprobar fotos" recuerde validar las fotos.</p>
        </div>
    </div>
    <div id="div-lista-encuestas" class="row" style="margin-top: 20px;">
         <?php $i = 1  ?>
        @forelse($encuestas as $e)
            <div id="encuesta-<?= $i ?>" class="col-md-4">
                <div class="panel panel-default panel-encuesta" style="margin-bottom: 5px; padding: 5px;">
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#" class="btn btn-red rechaza-encuesta" data-div="{{ $i }}" data-id="{{ $e["id_encuesta_contestada"] }}" style="width: 100%;">
                                Rechazar
                            </a>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-5">
                            <a href="#" class="btn btn-success aprueba-encuesta" data-div="{{ $i }}" data-id="{{ $e["id_encuesta_contestada"] }}" style="width: 100%;">
                                Aprobar
                            </a>
                        </div>

                    </div>
                    <div class="row" style="margin-top: 15px;">
                        <div class="col-md-12" style="border-right: 1px solid #FFFFFF;">
                            <p class="text-white text-center">Fecha de emisión: <span style="font-size: 12px; font-weight: 600">{{ $e["fecha_emision"]  }}</span></p>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 5px; padding: 5px;">
                        <div class="col-md-4">
                            @if($e["latitud"] != "0" && $e["latitud"] != "0")
                                <p class="text-right text-white"><i class="fa fa-map-marker fa-2x"></i></p>
                            @endif
                        </div>
                        <div class="col-md-6">
                            @if($e["foto"] != "0")
                                <a href="#" class="btn btn-info btn-icon icon-left validate-galery" data-id="{{ $e["id_encuesta_contestada"] }}" style="width: 100%;">
                                    Aprobar fotos <i class="fa fa-camera"></i>
                                </a>
                            @endif
                        </div>
                    </div>
                    <div class="row" style="margin-top: 5px; border-top: 1px solid #f4f4f4; padding: 5px;">
                        <div class="col-md-12" style="border-right: 1px solid #FFFFFF;">
                            <ol>
                                @forelse($e["preguntas"] as $pr)
                                    <li>
                                        <h5 class="text-white">{{ $pr["pregunta"]  }}</h5>
                                        <p class="text-white">{{ $pr["respuesta"]  }}</p>
                                    </li>
                                @empty
                                    <h5 class="text-center text-white">No contamos con preguntas para esta encuesta</h5>
                                @endforelse
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <?php $i++ ?>
        @empty
            <div class="col-md-12">
                <h4 class="text-center">No contamos con encuestas para aprobación, intente mas tarde.</h4>
            </div>
        @endforelse
    </div>
    <div class="modal fade in" id="modal-valida-fotos" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" style="width: 100%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <br/>
                </div>
                <div id="grid-fotos-validacion" class="modal-body">

                </div>
            </div>
        </div>
    </div>
@endsection
@section('assetsJs')
    {!! HTML::script('assets/js/toastr.js') !!}
    {!! HTML::script('assets/js/fn/fn.revision.js') !!}
@endsection