@extends('template/index')

@section('assetsCss')
@endsection

@section('title', 'Mapa')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Analisis geografico</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p class="text-left">Seleccione una encuesta para cargar los resultados geograficos, recuerda que puedes aplicar filtros especificos al mapa.</p>
        </div>
        <div class="col-md-12">
            <div class="col-md-8">
                {!! Form::select('encuestas', $encuestas, null, ['id' => 'encuesta_id', 'class' => 'form-control', 'placeholder' => '-- Seleccione --', 'onchange' => 'initialize()']) !!}
            </div>
            <div class="col-md-2" id="filtros_mapa" style="display: none">
                <button type="button" style="width:100%;" class="btn btn-success open_filters" data-toggle="modal" data-target="#myModal">Filtros</button>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 25px;">
        <div id="legend_mapa" class="col-xs-3 col-sm-2 col-md-2 col-lg-2" style="height: 200px; position: absolute; z-index: 9; margin-left: 1%; margin-top: 4%; display: none; background: #111; opacity: 0.9;">
        </div>
    </div>
    <div class="row" style="padding-top: 20px;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="map" style="height:650px; width:100%; position:fixed;"></div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Catálogo de filtros</h4>
                </div>
                <div class="modal-body">
                    <p>Seleccione los filtros</p>
                    {!! Form::open([
                        'id'        =>  'mapa_filtros_form',
                    ]) !!}
                    <div id="load_indicadores"></div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="initialize()">Limpiar filtros</button>
                    <button type="button" id="apply_filters" class="btn btn-primary">Aplicar filtros</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModalData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Resultados de la encuesta</h4>
                </div>
                <div id="content-encuesta" class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('assetsJs')
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXKK_7Gf8xTjT2ILe5fgh6K56zyp9uPac"></script>
    {!! HTML::script('assets/js/fn/fn.mapa.js') !!}

@endsection