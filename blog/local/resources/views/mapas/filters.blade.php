<div class="row">
    <div class="col-md-12">
        @forelse($indicadores as $in)
            <div class="col-md-6">
                <h5>{{ $in["pregunta"] }}</h5>
                @foreach($in["respuestas"] as $re)
                    <div class="checkbox">
                        <label>
                            <input value="{{ $in["id_pregunta"]."__".Crypt::encrypt($re->id) }}" name="filtros[]" type="checkbox">{{ $re->respuesta  }}
                        </label>
                    </div>
                @endforeach
            </div>
        @empty
            <h4>Esta encuesta no cuenta con indicadores disponibles.</h4>
        @endforelse
    </div>
</div>