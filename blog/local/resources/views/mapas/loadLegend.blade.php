<div class="row">
    <ul class="list-group">
        <?php foreach($colores as $k => $c): ?>
        <li class="list-group-item" style="background: #<?= $k ?>; color:#eaeaea; padding: 5px 15px; border: none;">
            <?= $c ?>
        </li>
        <?php endforeach; ?>
    </ul>
</div>