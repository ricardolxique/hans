<div class="row">
    @if (count($fotos) > 0 )
    <div class="col-md-12">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php $i = 0; ?>
                @foreach($fotos as $f)
                    <li data-target="#carousel-example-generic" data-slide-to="<?= $i; ?>" class="<?php echo ($i == 0) ? "active" : "" ; ?>"></li>
                <?php $i++; ?>
                @endforeach
            </ol>
            <div class="carousel-inner" role="listbox">
                <?php $i = 0; ?>
                @foreach($fotos as $f)
                <div class="item <?php echo ($i == 0) ? "active" : "" ; ?>">
                    <img src="data:image/png;base64,{{ $f["imagen"] }}" style="width: 100%"; >
                    <div class="carousel-caption">
                        {{ $f["nombre"] }}
                    </div>
                </div>
                <?php $i++; ?>
                @endforeach
            </div>
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    @endif
    <div class="col-md-12">
        @forelse($preguntas as $p)
            <div class="col-md-6">
                <h4 class="text-left">{{ $p["pregunta"] }}</h4>
                <p class="text-left">{{ $p["respuesta"] }}</p>
            </div>
        @empty
            <div class="col-md-12">
                <p class="text-center">No contamos con datos para esta encuesta.</p>
            </div>
        @endforelse
    </div>
</div>