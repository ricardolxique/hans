<div class="row">
    @forelse($coordinadores as $c)
    <div class="col-md-10 col-md-offset-1 div-seguimiento">
        <h4>{{ $c->usuarios->nombre  }}</h4>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4 col-md-offset-1"><h5>Operador</h5></div>
                    <div class="col-md-2"><h5>Asignadas</h5></div>
                    <div class="col-md-5"><h5>Concretadas / Avance</h5></div>
                </div>
            </div>
        @foreach($resultados as $r)
            @if($r->coordinador_id == $c->usuario_id)
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4 col-md-offset-1">
                        {{  $r->nombre }}
                    </div>
                    <div class="col-md-2">
                        {{  $r->encuestas_asignadas }}
                    </div>
                    <div class="col-md-5">
                        {{  $r->total_contestadas }}
                        <?php  $promedio = ($r->total_contestadas * 100)/$r->encuestas_asignadas  ?>
                        <div class="progress">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100" style="width: {{ round($promedio, 0)  }}%">
                                <span class="sr-only">{{ round($promedio, 0)  }}% Complete (success)</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        @endforeach
        </div>
    </div>
    @empty

        <h4 class="text-center"> Este estudio aun no cuenta con un seguimiento disponible</h4>
    @endforelse
</div>