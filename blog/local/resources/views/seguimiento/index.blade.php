@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
@endsection

@section('title', 'Seguimiento de coordinadores')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Seguimiento de coordinadores</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p class="text-left">Seleccione una encuesta para cargar los resultados de desempeño por encuestador.</p>
        </div>
        <div class="col-md-8">
            {!! Form::select('encuestas', $encuestas, null, ['id' => 'encuesta_id', 'class' => 'form-control', 'placeholder' => '-- Seleccione --']) !!}
        </div>
    </div>
    <div id="div_avance" class="row" style="margin-top: 25px;">

    </div>
@endsection
@section('assetsJs')
    {!! HTML::script('assets/js/fn/fn.seguimiento.js') !!}
    {!! HTML::script('assets/js/sweetalert.min.js') !!}
@endsection