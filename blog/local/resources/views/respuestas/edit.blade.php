{!! Form::open([
    'id'    =>  'respuesta_update_form',
    'method'=> 'PATCH',
    'route' => ['respuestas.update', Crypt::encrypt($respuesta->id)]
]) !!}
<div class="col-md-12">
    <div id="errors_update" class="alert alert-danger" style="display: none;"></div>
</div>
<div class="col-md-12">
    {!! Form::label('respuesta', 'Respuesta:', ['class' => 'control-label']) !!}
    {!! Form::text('respuesta', $respuesta->respuesta, ['id'  =>  'pregunta', 'class' => 'form-control', 'placeholder' => 'Escribe tu respuesta', 'required' => '']) !!}
</div>
<div class="col-md-12">
    {!! Form::label('salto', 'Salto a:', ['class' => 'control-label']) !!}
    {!! Form::select('salto', $listPreguntas, $respuesta->salto, ['id' => 'salto', 'class' => 'form-control', 'placeholder' => '-- Seleccione --']) !!}
</div>
{!! Form::close() !!}
