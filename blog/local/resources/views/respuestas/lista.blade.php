<div id="list-10" class="nested-list with-margins">
    <ul class="dd-list">
        @if(count($respuestas) > 0 && $respuestas != null)
            <?php $i = 0; ?>
            @foreach($respuestas as $r)
                <li class="dd-item" data-id="{{ Crypt::encrypt($r->id) }}">
                    <div class="dd-handle" style="height: 50px;">

                        <div class="col-md-6">
                            {{ $r->respuesta  }}
                        </div>
                        <div class="col-md-4">
                            {{ ($r->salto != null) ? "Salto a : ".$r->respuestaSaltos->pregunta :  "No salto"  }}
                        </div>
                        <div class="col-md-2">
                            <div class="col-md-6">
                                {!! Form::button( 'Editar', ['type' => 'button', 'class' => 'btn btn-blue btn-sm edit-answer pull-right', 'data-id' => Crypt::encrypt($r->id)] ) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::button( ' Eliminar', ['type' => 'button', 'class' => 'btn btn-red btn-sm delete-answer pull-right', 'id' => 'btnDeleteAnswer', 'data-id' => Crypt::encrypt($r->id) ] ) !!}
                            </div>
                        </div>
                    </div>
                </li>
                <?php $i++; ?>
            @endforeach
        @else
            <h4 class="text-center">No encontramos respuestas cargadas en esta pregunta.</h4>
        @endif
    </ul>
</div>
<div id="list-1" class="nested-list with-margins" style="display: none;">
    <ul class="dd-list" style="list-style: initial; list-style-type: decimal;">
        <?php $i = 1; ?>
        @foreach($respuestas as $r)
            <li class="dd-item" data-id="{{ Crypt::encrypt($r->id) }}">
                <div class="dd-handle" style="height: 50px;">
                    <div class="col-md-12">
                        {{ $r->respuesta  }}
                    </div>
                </div>
            </li>
            <?php $i++; ?>
        @endforeach
    </ul>
</div>