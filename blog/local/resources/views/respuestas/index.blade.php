@extends('template/index')

@section('assetsCss')
    {!! HTML::style('assets/css/sweetalert.css') !!}
    {!! HTML::style('assets/js/switch/css/bootstrap3/bootstrap-switch.min.css') !!}
@endsection

@section('title', 'Cargar Respuestas')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4>[ {{ $pregunta->pregunta  }} ]</h4>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-md-offset-10">
            {!!  Html::link('#', " Regresar", array( 'data-id' => Crypt::encrypt($pregunta->encuesta_id), 'class' => 'btn btn-blue pull-right back-preguntas')) !!}
        </div>
    </div>
    <div class="row">
        @if(in_array("2", $arrayPermisos))
            <div class="col-md-12">
                {!! Form::open([
                    'id'        =>  'respuestas_form',
                    'route'     =>  'respuestas.store',
                    'method'    =>  'POST'
                ]) !!}
                <h4>Agregar respuesta</h4>
                <div class="col-md-12">
                    <div id="errors" class="alert alert-danger" style="display: none;"></div>
                </div>
                <div class="col-md-6">
                    {!! Form::hidden('encuesta_pregunta_id', Crypt::encrypt($pregunta->id), ['id' => 'encuesta_pregunta_id_value', 'data-id' => Crypt::encrypt($pregunta->id)]) !!}
                    {!! Form::label('respuesta', 'Respuesta:', ['class' => 'control-label']) !!}
                    {!! Form::text('respuesta', null, ['id'  =>  'respuesta', 'class' => 'form-control', 'placeholder' => 'Escribe tu respuesta', 'required' => '']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::label('salto', 'Salto de pregunta:', ['class' => 'control-label']) !!}
                    {!! Form::select('salto', $listPreguntas, null, ['id' => 'salto_pregunta', 'class' => 'form-control', 'placeholder' => '-- Seleccione --']) !!}
                </div>
                <div class="col-md-12">
                    {!!  Form::submit("Agregar respuesta", array('id' => 'agregar_respuesta', 'class' => 'btn btn-blue pull-right', 'style' => 'margin-top: 20px;')) !!}
                </div>
                {!! Form::close() !!}
            </div>
        @endif
    </div>
    <hr>
    <div class="row" style="padding-top: 20px;">
        <div class="col-md-12">
            <div style="padding-bottom: 60px;">
                <h4>Lista de respuestas</h4>
                @if(in_array("3", $arrayPermisos))
                    <div class="col-md-5">
                        <h5>Para cambiar el orden activa el combo y despues solo arrastre y coloca en el orden que desee la respuesta.</h5>
                    </div>
                    <div class="col-md-offset-3 col-md-4">
                        <div class="form-group">
                            <p class="col-sm-6 control-label"><b>Ordenar respuestas</b></p>
                            <div class="col-sm-6">
                                <div class="make-switch">

                                    <input type="checkbox" id="active_order" value="{{ Crypt::encrypt($pregunta->id)  }}" name="active_order">
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div id="list_respuesta">
                <div id="list-10" class="nested-list with-margins">
                    <ul class="dd-list">
                        @if(count($respuestas) > 0 && $respuestas != null)
                            <?php $i = 0; ?>
                            @foreach($respuestas as $r)
                                <li class="dd-item" data-id="{{ Crypt::encrypt($r->id) }}">
                                    <div class="dd-handle" style="height: 50px;">
                                        <div class="col-md-6">
                                            {{ $r->respuesta  }}
                                        </div>
                                        <div class="col-md-4">
                                            {{ ($r->salto != null) ? "Salto a : ".$r->respuestaSaltos->pregunta :  "No salto"  }}
                                        </div>
                                        <div class="col-md-2">
                                            <div class="col-md-6">
                                                @if(in_array("3", $arrayPermisos))
                                                    {!! Form::button( 'Editar', ['type' => 'button', 'class' => 'btn btn-blue btn-sm edit-answer pull-right', 'data-id' => Crypt::encrypt($r->id)] ) !!}
                                                @endif
                                            </div>
                                            <div class="col-md-6">
                                                @if(in_array("4", $arrayPermisos))
                                                    {!! Form::button( ' Eliminar', ['type' => 'button', 'class' => 'btn btn-red btn-sm delete-answer pull-right', 'id' => 'btnDeleteAnswer', 'data-id' => Crypt::encrypt($r->id)] ) !!}
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php $i++; ?>
                            @endforeach
                        @else
                            <h4 class="text-center">No encontramos respuestas cargadas en esta pregunta.</h4>
                        @endif
                    </ul>
                </div>
                <div id="list-1" class="nested-list with-margins" style="display: none;">
                    <ul class="dd-list" style="list-style: initial; list-style-type: decimal;">
                        <?php $i = 1; ?>
                        @foreach($respuestas as $r)
                            <li class="dd-item" data-id="{{ Crypt::encrypt($r->id) }}">
                                <div class="dd-handle" style="height: 50px;">
                                    <div class="col-md-12">
                                        {{ $r->respuesta  }}
                                    </div>
                                </div>
                            </li>
                            <?php $i++; ?>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="modalEditAnswer" aria-hidden="false" style="display: none;">
        <div class="modal-dialog" style="width: 60%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Editar respuesta</h4>
                </div>
                <div id="contentEditAnswer" class="modal-body" style="overflow: auto;"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="update_respuesta" class="btn btn-info">Actualizar respuesta</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('assetsJs')
    {!! HTML::script('assets/js/fn/fn.respuestas.js') !!}
    {!! HTML::script('assets/js/sweetalert.min.js') !!}
    {!! HTML::script('assets/js/jquery.nestable.js') !!}
    {!! HTML::script('assets/js/switch/js/bootstrap-switch.min.js') !!}
@endsection