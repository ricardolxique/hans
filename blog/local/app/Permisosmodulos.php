<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permisosmodulos extends Model
{
    protected $table = 'permisos_modulos';

    protected $fillable = [
        'cat_modulos_id',
        'tipousuarios_id',
        'mostrar'
    ];

    public function catModulos()
    {
        return $this->belongsTo(Catmodulos::class, 'cat_modulo_id');
    }
    public function tipoUsuario()
    {
        return $this->belongsTo(Tiposusuarios::class, 'tipousuarios_id');
    }
}
