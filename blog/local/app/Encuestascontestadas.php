<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Encuestascontestadas extends Model
{
    protected $table = 'encuestascontestadas';

    protected $fillable = [
        'id',
        'operacion_usuario_id',
        'encuestas_id',
        'recibida_servidor',
        'emitida_movil',
        'latitud',
        'longitud'
    ];

    public function operacionUsuario()
    {
        return $this->belongsTo(Operacionesusuarios::class, 'operacion_usuario_id');
    }

    public function fotosEncuesta() {
        return $this->belongsTo(Encuestasfotos::class, 'encuestas_contestadas_id');
    }

    public function scopeActive($query)
    {
        return $query->where('activo', 1);
    }
    public function scopeValidada($query)
    {
        return $query->where('estado', 1);
    }

}
