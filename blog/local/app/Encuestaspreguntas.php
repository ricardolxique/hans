<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Encuestaspreguntas extends Model
{
    protected $table = 'encuestapreguntas';

    protected $fillable = [
        'encuesta_id',
        'pregunta_tipo_id',
        'pregunta',
        'orden',
        'activo',
    ];

    public function tipoPreguntas()
    {
        return $this->belongsTo(Tipopreguntas::class, 'pregunta_tipo_id');
    }
    
    public function scopeActive($query)
    {
        return $query->where('activo', 1);
    }
    public function scopeFinPregunta($query)
    {
        return $query->where('pregunta_tipo_id', '<>', 4);
    }
}
