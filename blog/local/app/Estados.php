<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estados extends Model
{
    protected $table = 'cat_estados';

    protected $fillable = [
        'estado',
    ];
}
