<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipopreguntas extends Model
{
    protected $table = 'tipopreguntas';

    protected $fillable = [
        'nombre',
    ];

}
