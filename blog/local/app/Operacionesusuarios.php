<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operacionesusuarios extends Model
{
    protected $table = 'operaciones_usuarios';

    protected $fillable = [
        'operacion_coordinador_id',
        'usuario_id',
        'encuestas_asignadas'
    ];

    public function operacion_coordinador()
    {
        return $this->belongsTo(Operacionescoordinadores::class, 'operacion_coordinador_id');
    }

    public function usuarios()
    {
        return $this->belongsTo(Usuarios::class, 'usuario_id');
    }
    
    public function scopeActive($query)
    {
        return $query->where('activo', 1);
    }
}
