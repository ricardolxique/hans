<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Encuestasindicadores extends Model
{
    protected $table = 'encuestasindicadores';

    protected $fillable = [
        'encuesta_id',
        'pregunta_id',
        'usuario_created'
    ];

    public function encuestasPreguntas()
    {
        return $this->belongsTo(Encuestaspreguntas::class, 'pregunta_id');
    }

}
