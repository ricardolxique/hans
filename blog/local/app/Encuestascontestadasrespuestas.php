<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Encuestascontestadasrespuestas extends Model
{
    protected $table = 'encuestascontestadasrespuestas';

    protected $fillable = [
        'id',
        'encuestas_contestadas_id',
        'encuestas_preguntas_id',
        'encuestas_respuestas_id',
        'texto_abierta'
    ];

    public function Encuestascontestadas()
    {
        return $this->belongsTo(Encuestascontestadas::class, 'encuestas_contestadas_id');
    }

    public function Encuestaspreguntas()
    {
        return $this->belongsTo(Encuestaspreguntas::class, 'encuestas_preguntas_id');
    }

    public function Encuestasrespuestas()
    {
        return $this->belongsTo(Encuestasrespuestas::class, 'encuestas_respuestas_id');
    }

    public function scopeActive($query)
    {
        return $query->where('activo', 1);
    }
}
