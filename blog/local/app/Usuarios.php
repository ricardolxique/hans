<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuarios extends Model
{
    protected $table = 'usuarios';

    protected $fillable = [
        'nombre',
        'idTipoUsuario'
    ];

    public function perfiles()
    {
        return $this->belongsTo(Perfiles::class, 'idTipoUsuario');
    }

    public function scopeActive($query)
    {
        return $query->where('activo', 1);
    }

}
