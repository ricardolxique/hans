<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    protected $table = 'clientes';

    protected $fillable = [
        'nombre',
        'apellidos',
        'numero_telefonico',
        'email',
        'empresa',
        'calle',
        'numero_interior',
        'numero_exterior',
        'colonia',
        'delegacion_municipio',
        'idEstado',
        'codigo_postal',
        'rfc'
    ];

    public function estados()
    {
        return $this->belongsTo(Estados::class, 'idEstado');
    }

    public function scopeActive($query)
    {
        return $query->where('activo', 1);
    }
}
