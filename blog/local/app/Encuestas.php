<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Encuestas extends Model
{
    protected $table = 'encuestas';

    protected $fillable = [
        'nombre',
        'numeroEncuestas',
        'fechaInicio',
        'fechaFin',
        'observaciones',
        'activo',
        'idCliente'
    ];

    public function clientes()
    {
        return $this->belongsTo(Clientes::class, 'idCliente');
    }

    public function tipoEstudios()
    {
        return $this->belongsTo(Cattipoestudios::class, 'tipo_estudio_id');
    }

    public function scopeActive($query)
    {
        return $query->where('activo', 1);
    }
}
