<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catsecciones extends Model
{
    protected $table = 'cat_secciones';

    protected $fillable = [
        'id',
        'nombre',
        'cat_catalogos_id'
    ];
    public function scopeActive($query)
    {
        return $query->where('activo', 1);
    }
}
