<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Encuestasfotos extends Model
{
    protected $table = 'encuestasfotos';

    protected $fillable = [
        'id',
        'encuestas_contestadas_id',
        'nombre',
        'imagen',
        'estado',
        'activo'
    ];

    public function encuestasContestadas()
    {
        return $this->belongsTo(Encuestascontestadas::class, 'encuestas_contestadas_id');
    }

    public function scopeActive($query)
    {
        return $query->where('activo', 1);
    }

    public function scopeValidado($query)
    {
        return $query->where('estado', 1);
    }
}
