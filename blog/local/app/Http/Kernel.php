<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
        ],

        'api' => [
            'throttle:60,1',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'administrador' => \App\Http\Middleware\AdministradorMiddleware::class,
        'analisis' => \App\Http\Middleware\AnalisisMiddleware::class,
        'asignacion' => \App\Http\Middleware\AsignacionMiddleware::class,
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'clientes' => \App\Http\Middleware\ClientesMiddleware::class,
        'encuestas' => \App\Http\Middleware\EncuestasMiddleware::class,
        'encuestasanalisis' => \App\Http\Middleware\EncuestasanalisisMiddleware::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'indicadores' => \App\Http\Middleware\IndicadoresMiddleware::class,
        'mapa' => \App\Http\Middleware\MapaMiddleware::class,
        'operacion' => \App\Http\Middleware\OperacionMiddleware::class,
        'preguntas' => \App\Http\Middleware\PreguntasMiddleware::class,
        'reportes' => \App\Http\Middleware\ReportesMiddleware::class,
        'revision' => \App\Http\Middleware\RevisionMiddleware::class,
        'revisiongaleria' => \App\Http\Middleware\RevisiongaleriaMiddleware::class,
        'seguimiento' => \App\Http\Middleware\SeguimientoMiddleware::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'usuarios' => \App\Http\Middleware\UsuariosMiddleware::class,

    ];
}
