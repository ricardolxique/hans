<?php
    /* LOGIN */
    Route::get('login', 'AuthController@showLogin');
    Route::post('login', 'AuthController@postLogin');

    /* LIGAS LOGUEADAS */
    Route::group(['middleware' => 'auth'], function() {
        /* LIGAS GENERALES */
        Route::get('/', 'Home\IndexController@index');
        Route::get('logout', 'AuthController@logOut');

        /* MODULO ADMINISTRADOR */
        Route::group(['middleware' => 'administrador'], function() {
            Route::resource('modulos', 'ModulosController');
            Route::resource('secciones', 'SeccionesController');
            Route::resource('permisos', 'PermisosController');
            Route::get('permisos/loadSecciones/{id}', 'PermisosController@loadSecciones');
            Route::get('permisos/loadPerfiles/{id}', 'PermisosController@loadPerfiles');
            Route::post('permisos/changePermisos', 'PermisosController@changePermisos');
            Route::resource('perfiles', 'PerfilesController');
        });
        /* ACCESO USUARIOS */
        Route::group(['middleware' => 'usuarios'], function() {
            Route::resource('usuarios', 'UsuariosController');
            Route::get('usuarios/changePassword/{id}', 'UsuariosController@changePassword');
            Route::match(array('PUT', 'PATCH'), "/usuarios/updPassword/{id}", array(
                'uses' => 'UsuariosController@updPassword',
                'as' => 'usuarios.updPass'
            ));
        });
        /* ACCESO CLIENTES */
        Route::group(['middleware' => 'clientes'], function() {
            Route::resource('clientes', 'ClientesController');
        });
        /* ACCESO ENCUESTAS */
        Route::group(['middleware' => 'encuestas'], function() {
            Route::resource('encuestas', 'EncuestasController');
            Route::resource('asignacion', 'OperacionescoordinadoresController');
            Route::resource('operacion', 'OperacionesusuariosController');
        });
        /* ACCESO PREGUNTAS Y RESPUESTAS */
        Route::group(['middleware' => 'preguntas'], function() {
            Route::resource('preguntas', 'PreguntasController');
            Route::get('preguntas/loadList/{id}', 'PreguntasController@loadList');
            Route::post('preguntas/order', 'PreguntasController@order');

            //Lista de preguntas
            Route::resource('respuestas', 'RespuestasController');
            Route::get('respuestas/loadList/{id}', 'RespuestasController@loadList');
            Route::post('respuestas/order', 'RespuestasController@order');
            Route::post('respuestas/backPreguntas', 'RespuestasController@backPreguntas');
        });


        /* ACCESO MAPA */
        Route::group(['middleware' => 'mapa'], function() {
            Route::resource('mapa', 'MapaController');
            Route::post('mapa/getMarkers', 'MapaController@getMarkers');
            Route::post('mapa/loadFiltros', 'MapaController@loadFiltros');
            Route::post('mapa/loadIndicadores', 'MapaController@loadIndicadores');
            Route::post('mapa/loadInfoLegend', 'MapaController@loadInfoLegend');
            Route::post('mapa/getInfoEncuestas', 'MapaController@getInfoEncuestas');
        });
        /* ACCESO INDICADORES */
        Route::group(['middleware' => 'indicadores'], function() {
            Route::resource('indicadores', 'IndicadoresController');
            Route::get('indicadores/loadList/{id}', 'IndicadoresController@loadList');
        });
        /* ACCESO ANALISIS */
        Route::group(['middleware' => 'analisis'], function() {
            Route::resource('analisis', 'AnalisisController');
            Route::post('analisis/loadQuestions', 'AnalisisController@loadQuestions');
            Route::post('analisis/loadDataInfo', 'AnalisisController@loadDataInfo');
        });
        /* ACCESO SEGUIMIENTO */
        Route::group(['middleware' => 'seguimiento'], function() {
            Route::resource('seguimiento', 'SeguimientoController');
            Route::post('seguimiento/loadSeguimiento', 'SeguimientoController@loadSeguimiento');
        });
        /* ACCESO REPORTES */
        Route::group(['middleware' => 'reportes'], function() {
            Route::get('reportes', 'ReportesController@index');
            Route::post('reportes/loadPreguntas', 'ReportesController@loadPreguntas');
            Route::post('reportes/generaReportes', 'ReportesController@generaReportes');
            Route::get('reportes/download', 'ReportesController@downloadReporte');
            Route::get('reportes/downloadSAV', 'ReportesController@downloadReporteSAV');
        });
        /* ACCESO REVISIÓN */
        Route::group(['middleware' => 'revision'], function() {
            Route::resource('revision', 'RevisionController');
            Route::post('revision/revisaEncuesta', 'RevisionController@revisaEncuesta');
            Route::post('revision/loadQuestion', 'RevisionController@loadQuestion');
            Route::post('revision/loadFoto', 'RevisionController@loadFoto');
            Route::post('revision/revisaFoto', 'RevisionController@revisaGaleria');

        });
        /* ACCESO REVISIÓN DE GALERIA */
        /*
        Route::group(['middleware' => 'revisiongaleria'], function() {
            Route::resource('revisionGaleria', 'RevisiongaleriaController');
            Route::post('revisionGaleria/revisa', 'RevisiongaleriaController@revisaGaleria');
            Route::post('revisionGaleria/loadFoto', 'RevisiongaleriaController@loadFoto');

        });
        */
        /* ACCESO REVISIÓN DE GALERIA */
        Route::group(['middleware' => 'encuestasanalisis'], function() {
            Route::resource('encuestasanalisis', 'EncuestasanalisisController');
            Route::get('encuestasanalisis/encuestas/{id}', 'EncuestasanalisisController@encuestas');
            Route::post('encuestasanalisis/galeria', 'EncuestasanalisisController@galeria');
            Route::post('encuestasanalisis/encuesta', 'EncuestasanalisisController@encuesta');
        });
    });