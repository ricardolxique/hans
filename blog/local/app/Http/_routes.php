<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::pattern('id', '\d+');
/*
Route::get('admin/news/{id}', function ($id) {
    
    return 'Accediendo a la noticia:' . $id;
    
});
*/


Route::resource('admin/news', 'AdminNewsController');

/*
Route::get('admin/news', function() {
    return View::make('hello/layout');
});

Route::get('admin/news/section', function() {
    return View::make('hello/sections/section');
});
*/

// Definiendo url dinamica y apuntando a otro controlador
/*
Route::get('admin/news', 'AdminNewsController@getList');

Route::get('admin/news/{id}', 'AdminNewsController@getDetails');
*/

/*
// Atando el controlador a la URL
Route::controller('admin/news', 'AdminNewsController');
*/

Route::get('admin/news/create', function() {
    return 'Admin / News / Create';
});

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
