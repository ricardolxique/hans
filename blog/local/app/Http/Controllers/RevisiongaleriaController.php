<?php

namespace App\Http\Controllers;

use App\Encuestas;
use App\Encuestascontestadas;
use App\Encuestasfotos;
use App\Permisosperfiles;
use Auth;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Crypt;

class RevisiongaleriaController extends Controller
{

    public function index()
    {

    }
    public function show($id, Request $request) {
        $request->session()->forget('idsEncuestaGaleria');
        $idEncuesta                 =   Crypt::decrypt($id);
        $moduloEncuestaSupervision  =   18;
        $arrayPermisosSupervision   =   $this->getPermits($moduloEncuestaSupervision);
        $encuesta                   =   Encuestas::active()->find($idEncuesta);
        $request->session()->put('idEncuestaRevisionGaleria', $idEncuesta);

        $fotosEncuestas  =   $this->getFotosEncuestasContestadas($idEncuesta, 2, 9, $request);

        return view('revisiongaleria.index', [
            'encuesta'                      =>  $encuesta,
            'fotosEncuestas'                =>  $fotosEncuestas,
            'arrayPermisosSupervision'      =>  $arrayPermisosSupervision
        ]);
    }

    public function loadFoto(Request $request) {
        $div    =      $request->div;
        if ($request->session()->has('idEncuestaRevisionGaleria')) {
            $idEncuesta     =   $request->session()->get('idEncuestaRevisionGaleria');
            $fotosEncuestas      =   $this->getFotosEncuestasContestadas($idEncuesta, 2, 1, $request);

            return view('revisiongaleria.load_fotos', [
                'fotosEncuestas'            =>  $fotosEncuestas,
                'div'                       =>  $div
            ]);
        }
    }

    private function getFotosEncuestasContestadas($idEncuesta, $estado, $limite, $request) {
        $idsFinales         =   array();
        $inQuery            =   "";
        $encuestasFotosArray    =   array();

        if ($request->session()->has('idsEncuestaGaleria')) {
            $idsPasados     =   $request->session()->get('idsEncuestaGaleria');
            if (!empty($idsPasados) && count($idsPasados) > 0) {
                foreach ($idsPasados as $ds) {
                    $ids[] = Crypt::decrypt($ds);
                }
                $inQuery = "and ef.id NOT IN (" . implode(",", $ids) . ") ";
            }
        }

        $encuestasFotos       =   DB::select('call getFotosEncuestasContestadasPorOperador(?, ?, ?, ?)',array($idEncuesta, $estado, $limite, $inQuery));

        if (count($encuestasFotos) > 0) {
            foreach ($encuestasFotos as $ef) {
                $encuestasFotosArray[] = array(
                    "id_foto"           =>  Crypt::encrypt($ef->id),
                    "nombre"            =>  $ef->nombre,
                    "imagen"            =>  $ef->imagen,
                );

                $idsFinales[]           =   Crypt::encrypt($ef->id);
            }
            if ($request->session()->has('idsEncuestaGaleria')) {
                $idsSaved           =   $request->session()->get('idsEncuestaGaleria');
                $idsFinales         =   array_merge($idsSaved, $idsFinales);
            }
            $request->session()->put('idsEncuestaGaleria', $idsFinales);
        }

        return $encuestasFotosArray;
    }

    public function revisaGaleria() {
        $idFoto         =   Crypt::decrypt($_POST["idFoto"]);
        $estado         =   ($_POST["estado"] == "1") ? "1" : "3";
        $status         =   false;

        $fotos   =   Encuestasfotos::active()->find($idFoto);

        if (count($fotos) > 0) {
            if ($fotos->estado == 2) {
                $fotos->estado   =   $estado;
                $fotos->save();
                $msg = "Imagen validada correctamente";
                $status     =   true;
            } else {
                $msg    =   "Esta imagen ya fue validada.";
            }
        } else {
            $msg    =   "Hemos tenido un contratiempo, intentelo nuevamente.";
        }

        return response(['msg' => $msg, 'status' => $status]);
    }

    private function getPermits($seccion) {
        $idTipoUsuario  =   Auth::user()->idTipoUsuario;
        $arrayPermisos  =   array();

        $permisos   =   Permisosperfiles::where('tipousuarios_id', '=', $idTipoUsuario)->where('cat_secciones_id', '=', $seccion)->get();

        if (!empty($permisos) && count($permisos) > 0 ) {
            foreach ($permisos as $p) {
                $arrayPermisos[]    =   $p->cat_permisos_id;
            }
        }
        return $arrayPermisos;
    }
}