<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Encuestaspreguntas;
use App\Encuestas;
use App\Tipopreguntas;
use App\Permisosperfiles;
use Auth;

use App\Http\Requests;
use Illuminate\Support\Facades\Crypt;

class PreguntasController extends Controller
{

    public function index(Request $request)
    {
        $idEncuesta =   null;
        if ($request->session()->has('idPreguntaLoad')) {
            $id_encuesta = Crypt::decrypt($request->session()->get('idPreguntaLoad'));
            $request->session()->forget('idPreguntaLoad');
        } else {
            $id_encuesta = null;
        }
        $arrayPermisos  =   $this->getPermits();
        $encuestas  =   array("" => "-- Seleccione una encuesta--");
        $encuesta   =   Encuestas::where('activo', '=', 1)->get();

        foreach ($encuesta as $e) {
            $idEncrypt  =   Crypt::encrypt($e->id);
            if ($id_encuesta != null) {
                if ($e->id == $id_encuesta) {
                    $idEncuesta = $idEncrypt;
                }
            }
            $encuestas[$idEncrypt] = $e->nombre;
        }
        return view('preguntas.index', ['encuestas' => $encuestas, "idEncuesta" => $idEncuesta, "arrayPermisos" => $arrayPermisos]);
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
        $this->validate($request, [
                'pregunta'            =>  'required',
                'pregunta_tipo_id'  =>  'required'
        ]);

        $encuesta_id        =   Crypt::decrypt($request->encuesta_id);
        $pregunta_tipo      =   Crypt::decrypt($request->pregunta_tipo_id);

        $total  = Encuestaspreguntas::where('encuesta_id', '=', $encuesta_id)->where('pregunta_tipo_id', '<>', 4)->count();
        $total  =   $total+1;

        $encuestaPreguntas = new Encuestaspreguntas();
        $encuestaPreguntas->encuesta_id       = $encuesta_id;
        $encuestaPreguntas->pregunta_tipo_id  = $pregunta_tipo;
        $encuestaPreguntas->pregunta      = $request->pregunta;
        $encuestaPreguntas->orden         = $total;
        $encuestaPreguntas->save();

        $this->configLastQuestion($encuesta_id);

        return response()->json([
            'success'   =>  true,
            'encuesta'  =>  $request->encuesta_id,
            'message'   =>  'record inserted'
        ], 200);
    }

    public function show($id)
    {
        $tipo    =   array("" => "-- Seleccione --");
        $tipos   =  Tipopreguntas::where('activo', '=', 1)->get();

        foreach ($tipos as $t) {
            $tipo[Crypt::encrypt($t->id)] = $t->nombre;
        }

        $id         =   Crypt::decrypt($id);
        $preguntas  =   array();
        return view('preguntas.create', ['id' => $id, 'preguntas' => $preguntas, 'tipoPreguntas' => $tipo]);
    }

    public function edit($id)
    {
        $tipo    =   array("" => "-- Seleccione --");
        $tipos   =  Tipopreguntas::where('activo', '=', 1)->get();

        foreach ($tipos as $t) {
            $tipo[$t->id] = $t->nombre;
        }

        $id         =   Crypt::decrypt($id);
        $pregunta   =   Encuestaspreguntas::active()->with('tipoPreguntas')->find($id);

        return view('preguntas.edit', ['pregunta' => $pregunta, 'tipoPreguntas' => $tipo]);
    }

    public function update(Request $request, $id)
    {
        $id         =   Crypt::decrypt($id);
        $pregunta   =   Encuestaspreguntas::findOrFail($id);

        $this->validate($request, [
            'pregunta'              =>  'required',
            'pregunta_tipo_id'      =>  'required'
        ]);


        $pregunta->pregunta         =   $request->pregunta;
        $pregunta->pregunta_tipo_id =   $request->pregunta_tipo_id;
        $pregunta->save();

        return response()->json([
            'success'   =>  true,
            'encuesta'  =>  Crypt::encrypt($pregunta->encuesta_id),
            'message'   =>  'record updated'
        ], 200);
    }

    public function destroy($id, Request $request )
    {
        $id             =   Crypt::decrypt($id);
        $pregunta       =   encuestasPreguntas::find($id);

        if ( $request->ajax() ) {
            $pregunta->activo = '0';
            $pregunta->save();

            return response()->json([
                'success'   =>  true,
                'encuesta'  =>  Crypt::encrypt($pregunta->encuesta_id),
                'message'   =>  'record deleted'
            ], 200);
        }
        return response()->json([
            'success'   =>  false
        ], 200);
    }

    public function loadList($id)
    {
        $id         =   Crypt::decrypt($id);
        $preguntas  = Encuestaspreguntas::active()->with('tipoPreguntas')->where('encuesta_id', '=', $id)->where('pregunta_tipo_id', '<>', 4)->orderBy('orden', 'asc')->get();
        $arrayPermisos              =   $this->getPermits();
        $arrayPermisosRespuestas    =   $this->getPermitsAnswers();
        return view('preguntas.question', ['preguntas' => $preguntas, 'arrayPermisos' => $arrayPermisos, 'arrayPermisosRespuestas' => $arrayPermisosRespuestas]);
    }

    public function order(Request $request)
    {
        if ($request->isMethod('post')) {
            $json               =   json_decode($request->json);
            $encuestaPreguntas  =   new Encuestaspreguntas();
            $o = 1;
            foreach ( $json as $j) {
                $idPregunta         =   Crypt::decrypt($j->id);
                $pregunta           =   $encuestaPreguntas::find($idPregunta);
                $idEncuesta         =   $pregunta->encuesta_id;
                $pregunta->orden    = $o;
                $pregunta->save();
                $o++;
            }
        }
        $this->configLastQuestion($idEncuesta);

        return response()->json([
            'success' => true,
            'message' => 'record ordered'
        ], 200);
    }
    private function getPermits() {
        $idTipoUsuario  =   Auth::user()->idTipoUsuario;
        $seccion        =   9;
        $arrayPermisos  =   array();

        $permisos   =   Permisosperfiles::where('tipousuarios_id', '=', $idTipoUsuario)->where('cat_secciones_id', '=', $seccion)->get();

        if (!empty($permisos) && count($permisos) > 0 ) {
            foreach ($permisos as $p) {
                $arrayPermisos[]    =   $p->cat_permisos_id;
            }
        }
        return $arrayPermisos;
    }
    private function getPermitsAnswers() {
        $idTipoUsuario  =   Auth::user()->idTipoUsuario;
        $seccion        =   14;
        $arrayPermisos  =   array();

        $permisos   =   Permisosperfiles::where('tipousuarios_id', '=', $idTipoUsuario)->where('cat_secciones_id', '=', $seccion)->get();

        if (!empty($permisos) && count($permisos) > 0 ) {
            foreach ($permisos as $p) {
                $arrayPermisos[]    =   $p->cat_permisos_id;
            }
        }
        return $arrayPermisos;
    }
    private function configLastQuestion($idEncuesta) {
        $lastQuestion   =   Encuestaspreguntas::where('encuesta_id', '=', $idEncuesta)->where('pregunta_tipo_id', '=', '4')->get();
        $total          =   Encuestaspreguntas::where('encuesta_id', '=', $idEncuesta)->where('pregunta_tipo_id', '<>', 4)->count();
        $total          =   $total+1;

        if (!empty($lastQuestion) && count($lastQuestion) > 0) {
            $pregunta   =   Encuestaspreguntas::findOrFail($lastQuestion[0]->id);
            $pregunta->orden         =   $total;
            $pregunta->save();
        } else {
            $preguntas = new Encuestaspreguntas();
            $preguntas->encuesta_id         =       $idEncuesta;
            $preguntas->pregunta_tipo_id    =       "4";
            $preguntas->pregunta            =       "Fin de la encuesta";
            $preguntas->orden               =       $total;
            $preguntas->save();
        }
    }
}