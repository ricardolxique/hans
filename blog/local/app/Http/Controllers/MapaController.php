<?php

namespace App\Http\Controllers;
use App\Encuestasfotos;
use Illuminate\Http\Request;

use App\Encuestas;
use App\Encuestascontestadas;
use App\Encuestasindicadores;
use App\Encuestasrespuestas;
use App\Encuestascontestadasrespuestas;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class MapaController extends Controller
{

    public function index()
    {
        $encuestas  =   array("" => "-- Seleccione una encuesta--");
        $encuesta   =   Encuestas::where('activo', '=', 1)->get();
        foreach ($encuesta as $e) {
            $encuestas[Crypt::encrypt($e->id)] = $e->nombre;
        }

        return view('mapas.index', ['encuestas' => $encuestas]);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id, Request $request)
    {
        
    }
    public function getMarkers(Request $request) {
        header("Content-type: application/xml");

        $encuesta               =   Crypt::decrypt($request->encuesta);
        $encuestas              =   Encuestascontestadas::active()->validada()->where("encuestas_id", "=", $encuesta)->get();
        
        echo '<markers>';
        foreach($encuestas as $a) {
            echo '<marker ';
            echo 'id="'.Crypt::encrypt($a->id).'" ';
            echo 'lat="'. $a->latitud. '" ';
            echo 'lng="'. $a->longitud. '" ';
            echo '/>';
        }
        echo '</markers>';
        exit;
    }

    public function loadIndicadores(Request $request) {
        $encuesta               =   Crypt::decrypt($request->encuesta);
        $encuestaspreguntas     =   Encuestasindicadores::with("encuestasPreguntas")->where('encuesta_id', '=', $encuesta)->get();
        $indicadores            =   array();
        if (!empty($encuestaspreguntas) && count($encuestaspreguntas) > 0) {
            foreach ($encuestaspreguntas as $ep) {
                $encuestasrespuesta = Encuestasrespuestas::where('encuesta_pregunta_id', '=', $ep->encuestasPreguntas->id)->get();
                $indicadores[] = array(
                    "id_pregunta" => Crypt::encrypt($ep->encuestasPreguntas->id),
                    "pregunta" => $ep->encuestasPreguntas->pregunta,
                    "respuestas" => $encuestasrespuesta
                );
            }
        }
        return view('mapas.filters', ['indicadores' => $indicadores]);
    }

    public function loadFiltros(Request $request) {
        if (!empty($request->filtros)) {
            $colores    =   array( "03304f", "065d99", "509599", "507199", "995450", "5d555b", "99065d", "673664", "366752", "675236", "d63e18");

            $markers    =   array();
            $color      =   array();
            $encuesta   =   $request->encuesta;
            $filtros    =   $request->filtros;
            foreach ($filtros as $filtro) {
                list($pregunta, $respuesta) = explode("__", $filtro);
                $pre = Crypt::decrypt($pregunta);
                $res = Crypt::decrypt($respuesta);
                $result = Encuestascontestadasrespuestas::with('Encuestascontestadas')->with('Encuestasrespuestas')->where('encuestas_preguntas_id', '=', $pre)->where('encuestas_respuestas_id', '=', $res)->get();

                foreach ($result as $r) {
                    if (!array_key_exists($r->encuestas_respuestas_id, $color)) {
                        $numero_color = count($color);
                        $color[$r->encuestas_respuestas_id] = $colores[$numero_color];
                        $colorArray[$colores[$numero_color]]  =   $r->Encuestasrespuestas->respuesta;
                    }
                    $resultado[] = array(
                        "id"                =>  Crypt::encrypt($r->encuestas_contestadas_id),
                        "respuesta"         =>  $r->Encuestasrespuestas->respuesta,
                        "color"             =>  $color[$r->encuestas_respuestas_id],
                        "lat"               =>  $r->Encuestascontestadas->latitud,
                        "lng"               =>  $r->Encuestascontestadas->longitud
                    );
                }
                $request->session()->put('colores', $colorArray);
            }
            header("Content-type: text/xml");
            echo '<markers>';
            foreach($resultado as $d) {
                echo '<marker ';
                echo 'id="'.$d['id'].'" ';
                echo 'titulo="'. $d['respuesta']. '" ';
                echo 'color="'. $d['color']. '" ';
                echo 'lat="'. $d['lat']. '" ';
                echo 'lng="'. $d['lng']. '" ';
                echo '/>';
            }
            echo '</markers>';
        }
    }

    public function loadInfoLegend(Request $request) {
        $colores    =   $request->session()->get('colores');
        return view('mapas.loadLegend', ['colores' => $colores]);
    }

    public function getInfoEncuestas(Request $request) {
        $id             =   Crypt::decrypt($request->id);
        $encuesta       =   array();
        $encuestaContestada     =   Encuestascontestadas::active()->validada()->find($id);
        $encuestaContestadaPreguntas = $this->getPreguntas($encuestaContestada->id);
        $fotos                  =   Encuestasfotos::active()->validado()->where("encuestas_contestadas_id", "=", $id)->get();

        return view('mapas.loadData', [
            'encuesta'      =>  $encuesta,
            'preguntas'     =>  $encuestaContestadaPreguntas,
            'fotos'         =>  $fotos
        ]);
    }
    private function getPreguntas($id_encuesta_contestada) {
        $arrayPreguntas = array();
        $preguntas  =   Encuestascontestadasrespuestas::active()->with("Encuestaspreguntas")->with("Encuestasrespuestas")->where("encuestas_contestadas_id", "=", $id_encuesta_contestada)->groupBy("encuestas_preguntas_id")->get();
        if (!empty($preguntas) && count($preguntas) > 0 ) {
            foreach ($preguntas as $p) {
                switch ($p->Encuestaspreguntas->pregunta_tipo_id) {
                    case 1:
                        $respuesta      =   $p->texto_abierta;
                        break;
                    case 2:
                        $respuestas   =     Encuestascontestadasrespuestas::active()->with("Encuestasrespuestas")->where("encuestas_contestadas_id", "=", $id_encuesta_contestada)->where("encuestas_preguntas_id", "=", $p->encuestas_preguntas_id)->get();
                        if (!empty($respuestas) && count($respuestas) > 0) {
                            foreach ($respuestas as $re) {
                                $resp[]     =   $re->Encuestasrespuestas->respuesta;
                            }
                            $respuesta  =   implode(", ", $resp);
                        } else {
                            $respuesta  =   "";
                        }
                        break;
                    case 3:
                        $respuestaResultado     =   Encuestasrespuestas::active()->find($p->encuestas_respuestas_id);
                        $respuesta              =   $respuestaResultado->respuesta;
                        break;
                }
                $arrayPreguntas[]    =   array(
                    "pregunta"      =>  $p->Encuestaspreguntas->pregunta,
                    "respuesta"    =>  $respuesta
                );

            }
        }
        return $arrayPreguntas;
    }
}
