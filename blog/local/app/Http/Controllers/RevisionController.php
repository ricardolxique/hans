<?php

namespace App\Http\Controllers;

use App\Encuestas;
use App\Encuestascontestadas;
use App\Encuestascontestadasrespuestas;
use App\Encuestasrespuestas;
use App\Encuestasfotos;
use App\Permisosperfiles;
use Auth;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Crypt;

class RevisionController extends Controller
{

    public function index()
    {

    }
    public function show($id, Request $request)
    {
        $request->session()->forget('idsEncuesta');
        $idEncuesta                 =   Crypt::decrypt($id);
        $moduloEncuestaSupervision  =   17;
        $arrayPermisosSupervision   =   $this->getPermits($moduloEncuestaSupervision);
        $encuesta                   =   Encuestas::active()->find($idEncuesta);
        $request->session()->put('idEncuestaRevision', $idEncuesta);

        $encuestas  =   $this->getEncuestasContestadas($idEncuesta, 2, 3, $request);

        return view('revision.index', [
            'encuesta'              =>  $encuesta,
            'encuestas'             =>  $encuestas,
            'arrayPermisosSupervision'  =>  $arrayPermisosSupervision
        ]);
    }
    public function loadQuestion(Request $request) {
        $div    =      $request->div;
        if ($request->session()->has('idEncuestaRevision')) {
            $idEncuesta     =   $request->session()->get('idEncuestaRevision');
            $encuestas      =   $this->getEncuestasContestadas($idEncuesta, 2, 1, $request);
            
            return view('revision.load_encuesta', [
                'encuestas'             =>  $encuestas,
                'div'                   =>  $div
            ]);
        }
    }
    private function getEncuestasContestadas($idEncuesta, $estado, $limite, $request) {
        $idsFinales     =   array();
        $encuestas      =   array();
        $inQuery    =   "";

        if ($request->session()->has('idsEncuesta')) {
            $idsPasados     =   $request->session()->get('idsEncuesta');
            if (!empty($idsPasados) && count($idsPasados) > 0) {
                foreach ($idsPasados as $ds) {
                    $ids[] = Crypt::decrypt($ds);
                }

                $inQuery = "and ec.id NOT IN (" . implode(",", $ids) . ") ";
            }
        }

        $encuestasContestadas       =   DB::select('call getEncuestasContestadas(?, ?, ?, ?)',array($idEncuesta, $estado, $limite, $inQuery));

        if (count($encuestasContestadas) > 0) {
            foreach ($encuestasContestadas as $ec) {
                $encuestas[] = array(
                    "id_encuesta_contestada"    =>  Crypt::encrypt($ec->id_encuesta_contestada),
                    "fecha_emision"             =>  $ec->fecha_emision,
                    "nombre_usuario"            =>  $ec->nombre_usuario,
                    "foto"                      =>  $ec->foto,
                    "latitud"                   =>  $ec->latitud,
                    "longitud"                  =>  $ec->longitud,
                    "preguntas"                 =>  $this->getPreguntas($ec->id_encuesta_contestada)
                );
                $idsFinales[]      =   Crypt::encrypt($ec->id_encuesta_contestada);
            }
            if ($request->session()->has('idsEncuesta')) {
                $idsSaved           =   $request->session()->get('idsEncuesta');
                $idsFinales         =   array_merge($idsSaved, $idsFinales);
            }
            $request->session()->put('idsEncuesta', $idsFinales);
        }
        return $encuestas;
    }
    private function getPreguntas($id_encuesta_contestada) {
        $arrayPreguntas = array();
        $preguntas  =   Encuestascontestadasrespuestas::active()->with("Encuestaspreguntas")->with("Encuestasrespuestas")->where("encuestas_contestadas_id", "=", $id_encuesta_contestada)->groupBy("encuestas_preguntas_id")->get();
        if (!empty($preguntas) && count($preguntas) > 0 ) {
            foreach ($preguntas as $p) {
                switch ($p->Encuestaspreguntas->pregunta_tipo_id) {
                    case 1:
                        $respuesta      =   $p->texto_abierta;
                        break;
                    case 2:
                        $respuestas   =     Encuestascontestadasrespuestas::active()->with("Encuestasrespuestas")->where("encuestas_contestadas_id", "=", $id_encuesta_contestada)->where("encuestas_preguntas_id", "=", $p->encuestas_preguntas_id)->get();
                        if (!empty($respuestas) && count($respuestas) > 0) {
                            foreach ($respuestas as $re) {
                                $resp[]     =   $re->Encuestasrespuestas->respuesta;
                            }
                            $respuesta  =   implode(", ", $resp);
                        } else {
                            $respuesta  =   "";
                        }
                        break;
                    case 3:
                        $respuestaResultado     =   Encuestasrespuestas::active()->find($p->encuestas_respuestas_id);
                        $respuesta              =   $respuestaResultado->respuesta;
                        break;
                }
                $arrayPreguntas[]    =   array(
                    "pregunta"      =>  $p->Encuestaspreguntas->pregunta,
                    "respuesta"    =>  $respuesta
                );

            }
        }
        return $arrayPreguntas;
    }
    public function revisaEncuesta() {
        $idEncuestaContestada       =   Crypt::decrypt($_POST["idEncuesta"]);
        $estado                     =   ($_POST["estado"] == "1") ? "1" : "3";
        $status                     =   false;

        $fotos      =    Encuestasfotos::active()->where("estado", "=", "2")->where("encuestas_contestadas_id", "=", $idEncuestaContestada)->count();

        if ($fotos == 0) {
            $encuesta = Encuestascontestadas::active()->find($idEncuestaContestada);

            if (count($encuesta) > 0) {
                if ($encuesta->estado == 2) {
                    $encuesta->estado = $estado;
                    $encuesta->save();
                    $msg = "Encuesta validada correctamente";
                    $status = true;
                } else {
                    $msg = "Esta encuesta ya se encuentra validada.";
                }
            } else {
                $msg = "Hemos tenido un contratiempo, intentelo nuevamente.";
            }
        } else {
            $msg = "Al parecer no ha validado las fotos de esta encuesta    .";
        }

        return response(['msg' => $msg, 'status' => $status]);
    }
    public function loadFoto(Request $request) {
        $idEncuestaContestada       =   Crypt::decrypt($request->idEncuesta);
        $encuestasFotos             =   Encuestasfotos::active()->where('encuestas_contestadas_id', '=', $idEncuestaContestada)->get();

        return view('revision.loadFotos', [
            'encuestasFotos'        =>  $encuestasFotos
        ]);
    }
    public function revisaGaleria() {
        $idFoto         =   Crypt::decrypt($_POST["idFoto"]);
        $estado         =   ($_POST["estado"] == "1") ? "1" : "3";
        $status         =   false;

        $fotos   =   Encuestasfotos::active()->find($idFoto);

        if (count($fotos) > 0) {
            if ($fotos->estado == 2) {
                $fotos->estado   =   $estado;
                $fotos->save();
                $msg = "Imagen validada correctamente";
                $status     =   true;
            } else {
                $msg    =   "Esta imagen ya fue validada.";
            }
        } else {
            $msg    =   "Hemos tenido un contratiempo, intentelo nuevamente.";
        }

        return response(['msg' => $msg, 'status' => $status]);
    }
    private function getPermits($seccion) {
        $idTipoUsuario  =   Auth::user()->idTipoUsuario;
        $arrayPermisos  =   array();

        $permisos   =   Permisosperfiles::where('tipousuarios_id', '=', $idTipoUsuario)->where('cat_secciones_id', '=', $seccion)->get();

        if (!empty($permisos) && count($permisos) > 0 ) {
            foreach ($permisos as $p) {
                $arrayPermisos[]    =   $p->cat_permisos_id;
            }
        }
        return $arrayPermisos;
    }
}