<?php

namespace App\Http\Controllers;

use App\Encuestas;
use App\Encuestaspreguntas;
use App\Encuestasindicadores;
use Auth;
use App\Permisosperfiles;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Crypt;

class IndicadoresController extends Controller
{
    public function index()
    {
        $encuestas  =   array("" => "-- Seleccione una encuesta--");
        $encuesta   =   Encuestas::where('activo', '=', 1)->get();

        $arrayPermisos  =   $this->getPermits();

        foreach ($encuesta as $e) {
            $encuestas[Crypt::encrypt($e->id)] = $e->nombre;
        }
        return view('indicadores.index', ['encuestas' => $encuestas, 'arrayPermisos' => $arrayPermisos]);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'pregunta'            =>  'required'
        ]);
        $usuario_id         =   Auth::user()->id;
        $encuesta_id        =   Crypt::decrypt($request->encuesta_id);
        $pregunta_id        =   Crypt::decrypt($request->pregunta);

        $total  = encuestasIndicadores::where('encuesta_id', '=', $encuesta_id)->count();

        if ($total < 5) {
            $encuestasIndicadores = new encuestasIndicadores();
            $encuestasIndicadores->encuesta_id      = $encuesta_id;
            $encuestasIndicadores->pregunta_id      = $pregunta_id;
            $encuestasIndicadores->user_created     = $usuario_id;
            $encuestasIndicadores->save();

            return response()->json([
                'success'   =>  true,
                'encuesta'  =>  $request->encuesta_id,
                'message'   =>  'record inserted'
            ], 200);
        } else {
            return response()->json([
                'success'   =>  false,
                'message'   =>  'record limit'
            ], 200);
        }
    }

    public function show($id)
    {
        $id             =   Crypt::decrypt($id);

        $pregunta               =   array("" => "-- Seleccione --");
        $encuestaspreguntas     =   Encuestaspreguntas::active()->where('encuesta_id', '=', $id)->where('pregunta_tipo_id', '<>', '1')->get();


        foreach ($encuestaspreguntas as $ep) {
            $preguntas              =   encuestasIndicadores::where('pregunta_id', '=', $ep->id )->get();
            if (count($preguntas) == 0) {
                $pregunta[Crypt::encrypt($ep->id)] = $ep->pregunta;
            }
        }

        return view('indicadores.create', ['id' => $id, 'preguntas' => $pregunta]);
    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id, Request $request)
    {
        $id             =   Crypt::decrypt($id);
        $encuesta       =   encuestasIndicadores::find($id);
        $encuesta_id    =   $encuesta->encuesta_id;

        encuestasIndicadores::destroy($id);

        return response()->json([
            'success'   =>  true,
            'encuesta'  =>  Crypt::encrypt($encuesta_id),
            'message'   =>  'record delete'
        ], 200);
    }

    public function loadList($id)
    {
        $arrayPermisos  =   $this->getPermits();
        $id         =   Crypt::decrypt($id);
        $preguntas  =   encuestasIndicadores::with("encuestasPreguntas")->where('encuesta_id', '=', $id)->get();

        return view('indicadores.question', ['preguntas' => $preguntas, 'arrayPermisos' => $arrayPermisos]);
    }

    private function getPermits() {
        $idTipoUsuario  =   Auth::user()->idTipoUsuario;
        $seccion        =   7;
        $arrayPermisos  =   array();

        $permisos   =   Permisosperfiles::where('tipousuarios_id', '=', $idTipoUsuario)->where('cat_secciones_id', '=', $seccion)->get();

        if (!empty($permisos) && count($permisos) > 0 ) {
            foreach ($permisos as $p) {
                $arrayPermisos[]    =   $p->cat_permisos_id;
            }
        }
        return $arrayPermisos;
    }
}
