<?php

namespace App\Http\Controllers;

use App\Encuestas;
use App\Permisosperfiles;
use App\Encuestascontestadas;
use App\Encuestascontestadasrespuestas;
use App\Encuestasrespuestas;
use App\Encuestasfotos;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Crypt;

class EncuestasanalisisController extends Controller
{

    public function index()
    {
        $moduloEncuestaSeccion          =   19;
        $arrayPermisos                  =   $this->getPermits($moduloEncuestaSeccion);
        $encuestas                      =   Encuestas::with('clientes')->where('activo', '=', '1')->get();

        foreach ($encuestas as $e) {
            $arrayAvance    =   $this->getAvance($e->numeroEncuestas, $e->id);
            list($fechaInicio, $horaInicio)     =  explode(" ", $e->fechaInicio);
            list($fechaFin, $horaFin)           =  explode(" ", $e->fechaFin);
            $arrayEncuestas[]     =     array(
                    "id"        =>  Crypt::encrypt($e->id),
                    "encuesta"  =>  $e->nombre,
                    "numero"    =>  $e->numeroEncuestas,
                    "fechaInicio"   =>  $fechaInicio,
                    "fechaFin"      =>  $fechaFin,
                    "cliente"       =>  $e->clientes->empresa,
                    "porcentaje"    =>  $arrayAvance["porcentaje"],
                    "numeroEncuestas"   =>  $arrayAvance["numeroEncuestas"],
            );
        }
        return view('encuestasanalisis.index', [
            'arrayEncuestas'    =>  $arrayEncuestas,
            'arrayPermisos'     =>  $arrayPermisos
        ]);
    }

    public function encuestas($idEncuesta) {
        $id                 =   Crypt::decrypt($idEncuesta);
        $encuesta           =   Encuestas::active()->find($id);
        $encuestas          =   Encuestascontestadas::active()->validada()->where("encuestas_id", "=", $id)->get();
        $arrayEncuestas     =   array();

        if (count($encuestas) > 0) {
            foreach ($encuestas as $e) {
                $arrayEncuestas[] = array(
                    "id"        =>  Crypt::encrypt($e->id),
                    "fotos"     =>  $this->getFotos($e->id)
                );
            }
        }

        return view('encuestasanalisis.encuestas', [
            'arrayEncuestas'    =>  $arrayEncuestas,
            'encuesta'          =>  $encuesta
        ]);
    }

    public function encuesta(Request $request) {
        $id_encuesta_contestada         =   Crypt::decrypt($request->idEncuesta);
        $preguntas              =   Encuestascontestadasrespuestas::active()->with("Encuestaspreguntas")->with("Encuestasrespuestas")->where("encuestas_contestadas_id", "=", $id_encuesta_contestada)->groupBy("encuestas_preguntas_id")->get();
        $arrayPreguntas         =   array();

        if (!empty($preguntas) && count($preguntas) > 0 ) {
            foreach ($preguntas as $p) {
                switch ($p->Encuestaspreguntas->pregunta_tipo_id) {
                    case 1:
                        $respuesta      =   $p->texto_abierta;
                        break;
                    case 2:
                        $respuestas   =     Encuestascontestadasrespuestas::active()->with("Encuestasrespuestas")->where("encuestas_contestadas_id", "=", $id_encuesta_contestada)->where("encuestas_preguntas_id", "=", $p->encuestas_preguntas_id)->get();
                        if (!empty($respuestas) && count($respuestas) > 0) {
                            foreach ($respuestas as $re) {
                                $resp[]     =   $re->Encuestasrespuestas->respuesta;
                            }
                            $respuesta  =   implode(", ", $resp);
                        } else {
                            $respuesta  =   "";
                        }
                        break;
                    case 3:
                        $respuestaResultado     =   Encuestasrespuestas::active()->find($p->encuestas_respuestas_id);
                        $respuesta              =   $respuestaResultado->respuesta;
                        break;
                }
                $arrayPreguntas[]    =   array(
                    "pregunta"      =>  $p->Encuestaspreguntas->pregunta,
                    "respuesta"    =>  $respuesta
                );

            }
        }

        return view('encuestasanalisis.showEncuesta', [
            'arrayPreguntas'    =>  $arrayPreguntas
        ]);
    }

    public function galeria(Request $request) {
        $id         =   Crypt::decrypt($request->idEncuesta);
        $arrayFotos      =   Encuestasfotos::active()->validado()->where("encuestas_contestadas_id", "=", $id)->get();

        return view('encuestasanalisis.galeria', [
            'arrayFotos'    =>  $arrayFotos
        ]);
    }

    public function create() {
    }

    public function store(Request $request) {

    }

    public function show($id) {

    }

    public function edit($id) {

    }

    public function update(Request $request, $id) {

    }

    public function destroy($id, Request $request) {

    }

    private function getAvance($numero, $idEncuesta) {
        $numeroEncuestas    =   Encuestascontestadas::active()->validada()->where("encuestas_id", "=", $idEncuesta)->count();
        $arrayAvance        =   array();
        $arrayAvance["porcentaje"]      =   0;
        $arrayAvance["numeroEncuestas"] =   0;

        if (count($numeroEncuestas) > 0 && $numeroEncuestas != null) {
            $diferencia         =   $numero - $numeroEncuestas;

            if ($diferencia < 0) {
                $arrayAvance["porcentaje"]      =   100;
                $arrayAvance["numeroEncuestas"] =   $numeroEncuestas;
            } else {
                $arrayAvance["porcentaje"]      =   number_format(($numero * 100) / $numeroEncuestas, 2);
                $arrayAvance["numeroEncuestas"] =   $numeroEncuestas;
            }
        }

        return $arrayAvance;
    }

    private function getPermits($seccion) {
        $idTipoUsuario  =   Auth::user()->idTipoUsuario;
        $arrayPermisos  =   array();

        $permisos   =   Permisosperfiles::where('tipousuarios_id', '=', $idTipoUsuario)->where('cat_secciones_id', '=', $seccion)->get();

        if (!empty($permisos) && count($permisos) > 0 ) {
            foreach ($permisos as $p) {
                $arrayPermisos[]    =   $p->cat_permisos_id;
            }
        }
        return $arrayPermisos;
    }

    private function getFotos($idEncuestaContestada) {
        $resultado  =   false;
        $fotos  =  Encuestasfotos::active()->validado()->where("encuestas_contestadas_id", "=", $idEncuestaContestada)->count();

        if ($fotos > 0) {
            $resultado = true;
        }
        return $resultado;
    }
}
