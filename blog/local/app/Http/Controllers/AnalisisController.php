<?php

namespace App\Http\Controllers;

use App\Encuestas;
use App\Encuestaspreguntas;
use DB;
use Illuminate\Http\Request;
use Auth;
use App\Perfiles;
use App\Permisosperfiles;

use App\Http\Requests;

use Illuminate\Support\Facades\Crypt;

class AnalisisController extends Controller
{

    public function index()
    {
        $encuestas  =   array("" => "-- Seleccione una encuesta--");
        $encuesta   =   Encuestas::where('activo', '=', 1)->get();
        $arrayPermisos  =   $this->getPermits();
        foreach ($encuesta as $e) {
            $encuestas[Crypt::encrypt($e->id)] = $e->nombre;
        }
        return view('analisis.index', ['encuestas' => $encuestas]);
    }

    public function create()
    {
    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {

    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id, Request $request)
    {
    }
    public function loadQuestions(Request $request) {
        $encuesta   =   Crypt::decrypt($request->encuesta);
        $preguntas  =   Encuestaspreguntas::active()->finPregunta()->with('tipoPreguntas')->where('encuesta_id', '=', $encuesta)->where('pregunta_tipo_id', "<>", "1")->orderBy('orden', 'asc')->get();
        
        $pregunta  =   array("" => "-- Seleccione una pregunta--");
        foreach ($preguntas as $p) {
            $pregunta[Crypt::encrypt($p->id)] = $p->pregunta;
        }
        return view('analisis.questions', ['preguntas' => $pregunta]);
    }
    public function loadDataInfo(Request $request) {
        $result         =   false;
        $total          =   array();
        $respuestas     =   array();
        $pregunta_id    =   Crypt::decrypt($request->pregunta);
        $resultado      =   DB::select('call selectResultadosPregunta(?)',array($pregunta_id));
        $preguntaResult       =   encuestasPreguntas::active()->find($pregunta_id);


        if (!empty($resultado) && $resultado != null) {
            $result     =   true;
            $pregunta   =   $preguntaResult->pregunta;

            foreach ($resultado as $r) {
                $total[]        =   intval($r->total);
                $respuestas[]   =   $r->respuesta;
            }
        }

        return response()->json([
            'result'        =>  $result,
            'total'         =>  $total,
            'respuestas'    =>  $respuestas,
            'pregunta'      =>  $pregunta
        ], 200);
    }
    private function getPermits() {
        $idTipoUsuario  =   Auth::user()->idTipoUsuario;
        $seccion        =   10;
        $arrayPermisos  =   array();

        $permisos   =   Permisosperfiles::where('tipousuarios_id', '=', $idTipoUsuario)->where('cat_secciones_id', '=', $seccion)->get();

        if (!empty($permisos) && count($permisos) > 0 ) {
            foreach ($permisos as $p) {
                $arrayPermisos[]    =   $p->cat_permisos_id;
            }
        }
        return $arrayPermisos;
    }
}
