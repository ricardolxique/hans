<?php

namespace App\Http\Controllers;

use App\Operacionesusuarios;
use App\Operacionescoordinadores;
use App\Usuarios;
use App\Permisosperfiles;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Crypt;

class OperacionesusuariosController extends Controller
{
    public function index()
    {
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'usuario_id'                    => 'required',
            'encuestas_asignadas'           => 'required|numeric'
        ]);

        $coordinador_id     =   Crypt::decrypt($request->operacion_coordinador_id);
        $usuario_id         =   Crypt::decrypt($request->usuario_id);
        $encuestas          =   $request->encuestas_asignadas;

        $operacion = new Operacionesusuarios();
        $operacion->operacion_coordinador_id        =   $coordinador_id;
        $operacion->usuario_id                      =   $usuario_id;
        $operacion->encuestas_asignadas             =   $encuestas;
        $operacion->save();

        return response()->json([
            'success'       =>  true,
            'coordinador'   =>  Crypt::encrypt($coordinador_id),
            'message'       =>  'record saved   '
        ], 200);
    }

    public function show($id)
    {
        $coordinador_id         =       Crypt::decrypt($id);
        $coordinador            =       Operacionescoordinadores::find($coordinador_id);
        $usuarios_arreglo       =       Usuarios::active()->where('idTipoUsuario', '=', 3)->get();
        $operadores             =       array("" => "-- Seleccione --");
        $usuarios               =       array();
        $arrayPermisos          =       $this->getPermits();

        foreach($usuarios_arreglo as $ua) {
            $existe          =   Operacionesusuarios::active()->where('operacion_coordinador_id', '=', $coordinador_id)->where('usuario_id', '=', $ua->id)->get();
            if (count($existe) == 0) {
                $operadores[Crypt::encrypt($ua->id)] = $ua->nombre." ".$ua->apellidos;
            } else {
                $usuarios[] = array(
                    "id"            =>  $existe[0]->id,
                    "nombre"        =>  $ua->nombre." ".$ua->apellidos,
                    "asignadas"     =>  $existe[0]->encuestas_asignadas
                );
            }
        }
        return view('operacion.index', ['coordinador' => $coordinador, 'operadores' => $operadores, 'usuarios' => $usuarios, 'arrayPermisos' => $arrayPermisos]);
    }

    public function edit($id)
    {
        $id_usuario     =   Crypt::decrypt($id);
        $usuario        =   Operacionesusuarios::find($id_usuario);
        return view('operacion.edit', ['usuario' => $usuario]);

    }

    public function update(Request $request, $id)
    {
        $id         =   Crypt::decrypt($id);
        $usuario    =   Operacionesusuarios::find($id);

        $this->validate($request, [
            'encuestas_asignadas'           => 'required|numeric'
        ]);

        $usuario->encuestas_asignadas       =   $request->encuestas_asignadas;
        $usuario->save();

        return response()->json([
            'success'   =>  true,
            'message'   =>  'record updated'
        ], 200);
    }

    public function destroy($id, Request $request)
    {
        $id_usuario     =   Crypt::decrypt($id);
        $operacion      =   Operacionesusuarios::find($id_usuario);

        if ( $request->ajax() ) {
            $operacion->activo = '0';
            $operacion->save();

            return response()->json([
                'success'   =>  true,
                'message'   =>  'record deleted'
            ], 200);
        }
        return response(['msg' => 'Fallo la eliminación del usuario', 'status' => 'failed']);
    }
    private function getPermits() {
        $idTipoUsuario  =   Auth::user()->idTipoUsuario;
        $seccion        =   16;
        $arrayPermisos  =   array();

        $permisos   =   Permisosperfiles::where('tipousuarios_id', '=', $idTipoUsuario)->where('cat_secciones_id', '=', $seccion)->get();

        if (!empty($permisos) && count($permisos) > 0 ) {
            foreach ($permisos as $p) {
                $arrayPermisos[]    =   $p->cat_permisos_id;
            }
        }
        return $arrayPermisos;
    }
}
