<?php

namespace App\Http\Controllers;

use App\Encuestas;
use App\Clientes;
use App\Permisosperfiles;
use App\Cattipoestudios;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Crypt;

class EncuestasController extends Controller
{

    public function index()
    {
        $moduloEncuestaSeccion          =   8;
        $moduloEncuestaCoordinadores    =   15;
        $moduloEncuestaSupervision      =   17;
        $arrayPermisos                  =   $this->getPermits($moduloEncuestaSeccion);
        $arrayPermisosCoordinadores     =   $this->getPermits($moduloEncuestaCoordinadores);
        $arrayPermisosSupervision       =   $this->getPermits($moduloEncuestaSupervision);
        $encuestas = Encuestas::with('clientes')->where('activo', '=', '1')->get();

        return view('encuestas.index', [
                        'encuestas' => $encuestas,
                        'arrayPermisos' => $arrayPermisos,
                        'arrayPermisosCoordinadores' => $arrayPermisosCoordinadores,
                        'arrayPermisosSupervision' => $arrayPermisosSupervision
        ]);
    }

    public function create()
    {
        $cliente                =   array("" => "-- Seleccione --");
        $arrayTipoEstudios      =   array("" => "-- Seleccione --");
        $clientes               =   Clientes::active()->get();
        $resultTipoEstudios     =   Cattipoestudios::active()->get();

        foreach ($resultTipoEstudios as $t) {
            $arrayTipoEstudios[Crypt::encrypt($t->id)] = $t->nombre;
        }

        foreach ($clientes as $c) {
            $cliente[$c->id] = $c->empresa;
        }

        return view('encuestas.create', [
                        'clientes' => $cliente
                        ,'arrayTipoEstudios' => $arrayTipoEstudios
        ]);
    }

    public function store(Request $request)
    {
        $id_usuario =   Auth::user()->id;
        $this->validate($request, [
            'nombre'            => 'required'
            ,'numeroEncuestas'   => 'required'
            ,'fechaInicio'       => 'required'
            ,'fechaFin'          => 'required'
            ,'observaciones'     => 'required'
            ,'idCliente'         => 'required'
            ,'tipo_estudio_id'         => 'required'
        ]);

        $encuesta = new Encuestas;
        $encuesta->nombre           = $request->nombre;
        $encuesta->numeroEncuestas  = $request->numeroEncuestas;
        $encuesta->fechaInicio      = $request->fechaInicio;
        $encuesta->fechaFin         = $request->fechaFin;
        $encuesta->observaciones    = $request->observaciones;
        $encuesta->idCliente        = $request->idCliente;
        $encuesta->tipo_estudio_id        = Crypt::decrypt($request->tipo_estudio_id);
        $encuesta->save();
        $request->session()->flash('status', 'Encuesta agregada correctamente!');

        return redirect()->back();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $moduloEncuestaSeccion          =   8;
        $id             =   Crypt::decrypt($id);
        $arrayTipoEstudios      =   array("" => "-- Seleccione --");
        $cliente        =   array("" => "-- Seleccione --");
        $clientes       =   Clientes::active()->get();
        $arrayPermisos  =   $this->getPermits($moduloEncuestaSeccion);
        $resultTipoEstudios     =   Cattipoestudios::active()->get();

        foreach ($resultTipoEstudios as $t) {
            $arrayTipoEstudios[$t->id] = $t->nombre;
        }

        foreach ($clientes as $c) {
            $cliente[$c->id] = $c->empresa;
        }

        $encuesta = Encuestas::findOrFail($id);

        return view('encuestas.edit', [
                    'encuesta' => $encuesta
                    ,'clientes' => $cliente
                    ,'arrayPermisos' => $arrayPermisos
                    ,'arrayTipoEstudios' => $arrayTipoEstudios
        ]);
    }

    public function update(Request $request, $id)
    {
        $id         =   Crypt::decrypt($id);
        $encuesta   =   Encuestas::find($id);

        $this->validate($request, [
            'nombre'            => 'required'
            ,'numeroEncuestas'   => 'required'
            ,'fechaInicio'       => 'required'
            ,'fechaFin'          => 'required'
            ,'observaciones'     => 'required'
            ,'idCliente'         => 'required'
            ,'tipo_estudio_id'         => 'required'
        ]);

        $encuesta->nombre           = $request->nombre;
        $encuesta->numeroEncuestas  = $request->numeroEncuestas;
        $encuesta->fechaInicio      = $request->fechaInicio;
        $encuesta->fechaFin         = $request->fechaFin;
        $encuesta->observaciones    = $request->observaciones;
        $encuesta->idCliente        = $request->idCliente;
        $encuesta->tipo_estudio_id        = $request->tipo_estudio_id;
        $encuesta->save();

        $request->session()->flash('status', 'Encuesta actualizada correctamente!');

        return redirect()->back();
    }

    public function destroy($id, Request $request)
    {
        $id         =   Crypt::decrypt($id);
        $encuesta   =   Encuestas::find($id);

        if ( $request->ajax() ) {
            $encuesta->activo = '0';
            $encuesta->save();

            return response(['msg' => 'Encuesta eliminada', 'status' => 'success']);
        }
    }

    private function getPermits($seccion) {
        $idTipoUsuario  =   Auth::user()->idTipoUsuario;
        $arrayPermisos  =   array();

        $permisos   =   Permisosperfiles::where('tipousuarios_id', '=', $idTipoUsuario)->where('cat_secciones_id', '=', $seccion)->get();

        if (!empty($permisos) && count($permisos) > 0 ) {
            foreach ($permisos as $p) {
                $arrayPermisos[]    =   $p->cat_permisos_id;
            }
        }
        return $arrayPermisos;
    }
}
