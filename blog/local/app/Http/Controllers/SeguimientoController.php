<?php

namespace App\Http\Controllers;

use App\Encuestas;
use App\Operacionescoordinadores;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Crypt;

class SeguimientoController extends Controller
{

    public function index()
    {
        $encuestas  =   array("" => "-- Seleccione una encuesta--");
        $encuesta   =   Encuestas::where('activo', '=', 1)->get();
        foreach ($encuesta as $e) {
            $encuestas[Crypt::encrypt($e->id)] = $e->nombre;
        }
        return view('seguimiento.index', ['encuestas' => $encuestas]);
    }

    public function create()
    {
    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {

    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id, Request $request)
    {
    }
    
    public function loadSeguimiento(Request $request) {
        $encuesta_id        =   Crypt::decrypt($request->encuesta);
        $coordinadores      =   Operacionescoordinadores::with("usuarios")->active()->where('encuesta_id', '=', $encuesta_id)->get();
        $resultados         =   DB::select('call getTotalEncuestasContestadasPorOperador(?)', array($encuesta_id));

        return view('seguimiento.resultados', ['resultados' => $resultados, 'coordinadores' => $coordinadores]);
    }
}
