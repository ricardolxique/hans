<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Encuestaspreguntas;
use App\Encuestasrespuestas;
use App\Permisosperfiles;
use Auth;
use App\Http\Requests;

use Illuminate\Support\Facades\Crypt;

class RespuestasController extends Controller
{

    public function index(Request $request)
    {

    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'respuesta'            =>  'required'
        ]);

        $pregunta_id      =   Crypt::decrypt($request->encuesta_pregunta_id);

        $total  = Encuestasrespuestas::where('encuesta_pregunta_id', '=', $pregunta_id)->count();
        $total  =   $total+1;

        $encuestaPreguntas = new Encuestasrespuestas();
        $encuestaPreguntas->encuesta_pregunta_id  = $pregunta_id;
        $encuestaPreguntas->respuesta     = $request->respuesta;
        $encuestaPreguntas->salto         = $request->salto;
        $encuestaPreguntas->orden         = $total;
        $encuestaPreguntas->save();

        return response()->json([
            'success'   =>  true,
            'pregunta'  =>  $request->encuesta_pregunta_id,
            'message'   =>  'record inserted'
        ], 200);
    }

    public function show($id)
    {
        $preguntaId     =   Crypt::decrypt($id);
        $respuestas     =   Encuestasrespuestas::active()->with('encuestaPreguntas')->with('respuestaSaltos')->where('encuesta_pregunta_id', '=', $preguntaId)->orderBy('orden', 'asc')->get();

        return view('respuestas.lista', ['respuestas' => $respuestas]);
    }

    public function edit($id)
    {
        $respuestaId        =   Crypt::decrypt($id);
        $respuesta          =   Encuestasrespuestas::active()->with('encuestaPreguntas')->find($respuestaId);
        $listPreguntas      =   Encuestaspreguntas::active()->with('tipoPreguntas')->where('encuesta_id', '=', $respuesta->encuestaPreguntas->encuesta_id)->orderBy('orden', 'asc')->get();

        $preguntas    =   array("" => "-- Sin salto --");

        foreach ($listPreguntas as $lp) {
            if ($respuesta->encuestaPreguntas->id != $lp->id) {
                $preguntas[$lp->id] = $lp->pregunta;
            }
        }

        return view('respuestas.edit', ['respuesta' => $respuesta, 'listPreguntas' => $preguntas]);
    }

    public function update(Request $request, $id)
    {
        $id             =   Crypt::decrypt($id);
        $respuesta      =   Encuestasrespuestas::findOrFail($id);

        $this->validate($request, [
            'respuesta'              =>  'required',
        ]);


        $respuesta->respuesta       =   $request->respuesta;
        $respuesta->salto           =   $request->salto;
        $respuesta->save();

        return response()->json([
            'success'   =>  true,
            'pregunta'  =>  Crypt::encrypt($respuesta->encuesta_pregunta_id),
            'message'   =>  'record updated'
        ], 200);
    }

    public function destroy($id, Request $request )
    {
        $id             =   Crypt::decrypt($id);
        $respuesta      =   Encuestasrespuestas::find($id);
        if ( $request->ajax() ) {
            $respuesta->activo = '0';
            $respuesta->save();

            return response()->json([
                'success'   =>  true,
                'pregunta'  =>  Crypt::encrypt($respuesta->encuesta_pregunta_id),
                'message'   =>  'record deleted'
            ], 200);
        }
        return response()->json([
            'success'   =>  false
        ], 200);
    }

    public function loadList($id)
    {
        $arrayPermisos  =   $this->getPermits();
        $preguntaId     =   Crypt::decrypt($id);
        $pregunta       =   Encuestaspreguntas::active()->with('tipoPreguntas')->find($preguntaId);
        $listPreguntas      =   Encuestaspreguntas::active()->finPregunta()->with('tipoPreguntas')->where('encuesta_id', '=', $pregunta->encuesta_id)->orderBy('orden', 'asc')->get();
        $respuestas     =   Encuestasrespuestas::active()->with('respuestaSaltos')->with('encuestaPreguntas')->where('encuesta_pregunta_id', '=', $preguntaId)->orderBy('orden', 'asc')->get();

        $preguntas    =   array("" => "-- Sin salto --");

        foreach ($listPreguntas as $lp) {
            if ($pregunta->id != $lp->id) {
                $preguntas[$lp->id] = $lp->pregunta;
            }
        }

        return view('respuestas.index', ['respuestas' => $respuestas, 'pregunta' => $pregunta, 'listPreguntas' => $preguntas, 'arrayPermisos' => $arrayPermisos]);
    }

    public function backPreguntas (Request $request)
    {
        $idEncuesta =   $request->dd;
        $request->session()->put('idPreguntaLoad', $idEncuesta);
        return response()->json([
            'result'    => true,
        ], 200); ;
    }

    public function order(Request $request)
    {
        if ($request->isMethod('post')) {
            $json               =   json_decode($request->json);
            $Encuestasrespuestas = new Encuestasrespuestas();
            $o = 1;
            foreach ( $json as $j) {
                $idRespuesta            =   Crypt::decrypt($j->id);
                $respuesta              =   $Encuestasrespuestas::find($idRespuesta);
                $respuesta->orden       =   $o;
                $respuesta->save();
                $o++;
            }
        }
        return response()->json([
            'success' => true,
            'message' => 'record ordered'
        ], 200);
    }
    private function getPermits() {
        $idTipoUsuario  =   Auth::user()->idTipoUsuario;
        $seccion        =   14;
        $arrayPermisos  =   array();

        $permisos   =   Permisosperfiles::where('tipousuarios_id', '=', $idTipoUsuario)->where('cat_secciones_id', '=', $seccion)->get();

        if (!empty($permisos) && count($permisos) > 0 ) {
            foreach ($permisos as $p) {
                $arrayPermisos[]    =   $p->cat_permisos_id;
            }
        }
        return $arrayPermisos;
    }
}