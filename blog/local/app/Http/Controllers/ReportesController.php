<?php

namespace App\Http\Controllers;

use App\Encuestas;
use App\Encuestaspreguntas;
use App\Encuestascontestadas;
use App\Encuestascontestadasrespuestas;
use App\Encuestasrespuestas;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Crypt;

class ReportesController extends Controller
{

    public function index()
    {
        $encuestas  =   array("" => "-- Seleccione una encuesta--");
        $encuesta   =   Encuestas::where('activo', '=', 1)->get();
        foreach ($encuesta as $e) {
            $encuestas[Crypt::encrypt($e->id)] = $e->nombre;
        }
        return view('reportes.index', ['encuestas' => $encuestas]);
    }

    public function loadPreguntas(Request $request)
    {
        $id         =   Crypt::decrypt($request->id_encuesta);
        $preguntas  =   Encuestaspreguntas::active()->finPregunta()->with('tipoPreguntas')->where('encuesta_id', '=', $id)->orderBy('orden', 'asc')->get();

        return view('reportes.question', ['preguntas' => $preguntas]);
    }
    
    public function generaReportes(Request $request) {
        if (isset($request->encuesta_id) && count($request->preguntas) > 0 && $request->preguntas != "") {
            $request->session()->forget('keyFile');
            $encuesta_id    =    Crypt::decrypt($request->encuesta_id);
            $pregunta       =    $request->preguntas;

            foreach($pregunta as $pr) {
                $preguntaArray[]    =   Crypt::decrypt($pr);
            }

            $preguntas              =   Encuestaspreguntas::active()->FinPregunta()->with('tipoPreguntas')->whereIn('id', $preguntaArray)->get();
            $respuestas             =   Encuestasrespuestas::active()->whereIn('encuesta_pregunta_id', $preguntaArray)->get();
            $encuestasContestadas   =   Encuestascontestadas::validada()->where("encuestas_id", "=", $encuesta_id)->get();

            $listaPregunta          =   array();
            $listaPreguntaDos       =   array();

            foreach($preguntas as $p) {
                $listaPregunta[]    =   $p->pregunta;
            }

            foreach($preguntas as $p) {
                if ($p->pregunta_tipo_id == 2) {
                    $respuestasMultiples    =   $this->getPreguntasRespuestasMultiples($p->id);
                    $listaPreguntaDos       =   array_merge($listaPreguntaDos, $respuestasMultiples);
                } else {
                    $listaPreguntaDos[]   =   $p->id;
                }
            }

            $filenameXLS                =   $this->generateExcel($encuestasContestadas, $listaPregunta, $preguntas);
            $fileNameSAV                =   $this->generateSAV($encuestasContestadas, $listaPreguntaDos);
            $fileNameSAVPreguntas       =   $this->generateSAVPreguntas($preguntas);
            $fileNameSAVRespuestas      =   $this->generateSAVRespuestas($respuestas);

            $pathZip                    =    $this->downloadReporteSAV($fileNameSAV, $fileNameSAVPreguntas, $fileNameSAVRespuestas);

            return response()->json([
                'result'        =>  true,
                'linkExcel'     =>  "excel/reports/".$filenameXLS.".xls",
                'linkSAV'       =>  $pathZip
            ], 200);

        } else {
            return response()->json([
                'result'    =>  false,
                'message'   =>  'error 540'
            ], 200);
        }
    }
    public function downloadReporte(Request $request) {
        if ($request->session()->has('xlsFile')) {
            $filename = $request->session()->get('xlsFile');

            if ($filename != null || !empty($filename)) {
                $filename = Crypt::decrypt($filename);
                $path = "excel/reports/" . $filename.".xls";
                if (file_exists($path)) {
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename="' . basename($path) . '"');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($path));
                    readfile($path);
                    exit;
                } else {
                    return redirect('reportes');
                }
            }
        } else {
            return redirect('reportes');
        }
    }
    private function downloadReporteSAV($filenameSAV, $filenameSAVPreguntas, $filenameSAVRespuestas) {
        $nombre     = "SAV-".rand(999, 999999).".zip";
        $pathZIP    = "excel/zip/".$nombre;
        $pathSAV    = "excel/reports/" . $filenameSAV.".xls";
        $pathSAVP   = "excel/reports/" . $filenameSAVPreguntas.".xls";
        $pathSAVR   = "excel/reports/" . $filenameSAVRespuestas.".xls";
        $zipper = new \Chumper\Zipper\Zipper;
        $zipper->make($pathZIP)->add(
                    array(
                        $pathSAV,
                        $pathSAVP,
                        $pathSAVR
                    ));
        return $pathZIP;
    }
    private function generateExcel($encuestasContestadas, $listaPregunta, $preguntas) {
        $filename               =   "ReporteXLS_".date("y-m-d")."_".rand(999, 999999);
        $resultPreguntas[0]     =   $listaPregunta;
        $i = 1;
        foreach ($encuestasContestadas as $ec) {
            $resultPreguntas[$i][]  =   $ec->id;
            foreach ($preguntas as $p) {
                $tipoPregunta           =   $p->pregunta_tipo_id;
                $encuestaContestadaId   =   $ec->id;
                $encuestaPreguntaId     =   $p->id;

                $respuesta              =   $this->getRespuesta($tipoPregunta, $encuestaContestadaId, $encuestaPreguntaId);
                $resultPreguntas[$i][]  =   $respuesta;
            }
            $i++;
        }

        Excel::create($filename, function($excel) use($resultPreguntas) {
            $excel->sheet('Resultados Estudio', function($sheet) use($resultPreguntas) {
                $sheet->fromArray($resultPreguntas);
            });
        })->store('xls', storage_path('../../excel/reports'));

        return $filename;
    }
    private function generateSAVPreguntas($preguntas) {
        $filename               =   "CatalogoSAVPreguntas_".date("y-m-d")."_".rand(999, 999999);
        $resultPreguntas        =   array();
        $i = 1;
        foreach ($preguntas as $p) {
            $resultPreguntas[$i][]  =   $p->id;
            $resultPreguntas[$i][]  =   $p->pregunta;
            $i++;
        }

        Excel::create($filename, function($excel) use($resultPreguntas) {
            $excel->sheet('Catalogo de preguntas', function($sheet) use($resultPreguntas) {
                $sheet->fromArray($resultPreguntas);
            });
        })->store('xls', storage_path('../../excel/reports'));

        return $filename;
    }

    private function generateSAVRespuestas($respuestas) {
        $filename               =   "CatalogoSAVRespuestas_".date("y-m-d")."_".rand(999, 999999);
        $resultArray            =   array();
        $i = 1;
        foreach ($respuestas as $r) {
            $resultArray[$i][]  =   $r->id;
            $resultArray[$i][]  =   $r->respuesta;
            $i++;
        }

        Excel::create($filename, function($excel) use($resultArray) {
            $excel->sheet('Catalogo de respuestas', function($sheet) use($resultArray) {
                $sheet->fromArray($resultArray);
            });
        })->store('xls', storage_path('../../excel/reports'));

        return $filename;
    }

    private function generateSAV($encuestasContestadas, $listaPreguntaDos) {
        $filename               =   "ReporteSAV_".date("y-m-d")."_".rand(999, 999999);
        $resultPreguntasSAV[0]  =   $listaPreguntaDos;
        $i = 1;
        foreach ($encuestasContestadas as $ec) {
            $encuestaContestadaId   =   $ec->id;
            foreach ($listaPreguntaDos as $p) {
                $preguntaIdDos  =   explode(".", $p);
                if (count($preguntaIdDos) > 1) {
                    $idPreguntaDos      =   $preguntaIdDos[0];
                    $idRespuestaDos     =   $preguntaIdDos[1];
                    $respuestas         =   Encuestascontestadasrespuestas::active()->with("Encuestasrespuestas")->where("encuestas_contestadas_id", "=", $encuestaContestadaId)->where("encuestas_preguntas_id", "=", $idPreguntaDos)->where("encuestas_respuestas_id", "=", $idRespuestaDos)->get();
                } else {
                    $idPreguntaDos      =   $preguntaIdDos[0];
                    $respuestas         =   Encuestascontestadasrespuestas::active()->with("Encuestasrespuestas")->where("encuestas_contestadas_id", "=", $encuestaContestadaId)->where("encuestas_preguntas_id", "=", $idPreguntaDos)->get();
                }
                if (count($respuestas) > 0) {
                    if ($respuestas[0]->encuestas_respuestas_id  == 0) {
                        $resultPreguntasSAV[$i][] = $respuestas[0]->texto_abierta;
                    } else {
                        $resultPreguntasSAV[$i][] = $respuestas[0]->encuestas_respuestas_id;
                    }
                } else {
                    $resultPreguntasSAV[$i][]   =   "";
                }
            }
            $i++;
        }

        Excel::create($filename, function($excel) use($resultPreguntasSAV) {
            $excel->sheet('Reporte SAV', function($sheet) use($resultPreguntasSAV) {
                $sheet->fromArray($resultPreguntasSAV);
            });
        })->store('xls', storage_path('../../excel/reports'));

        return $filename;
    }
    private function getPreguntasRespuestasMultiples($idPregunta) {
        $respuestasArray    =   array();
        $respuestas   =  Encuestasrespuestas::active()->where("encuesta_pregunta_id", "=", $idPregunta)->get();
        if (count($respuestas) > 0) {
            foreach($respuestas as $r) {
                $respuestasArray[]  =   $idPregunta.".".$r->id;
            }
        }
        return $respuestasArray;
    }
    private function getRespuesta($tipoPregunta, $encuestaContestadaId, $encuestaPreguntaId) {
        $respuesta  = "";

        if ($tipoPregunta > 0) {
            switch ($tipoPregunta) {
                case 1:
                    $respuestas = Encuestascontestadasrespuestas::active()->where("encuestas_contestadas_id", "=", $encuestaContestadaId)->where("encuestas_preguntas_id", "=", $encuestaPreguntaId)->get();
                    if (!empty($respuestas) && count($respuestas) > 0) {
                        $respuesta = $respuestas[0]->texto_abierta;
                    }
                    break;
                case 2:
                    $respuestas = Encuestascontestadasrespuestas::active()->with("Encuestasrespuestas")->where("encuestas_contestadas_id", "=", $encuestaContestadaId)->where("encuestas_preguntas_id", "=", $encuestaPreguntaId)->get();
                    if (!empty($respuestas) && count($respuestas) > 0) {
                        foreach ($respuestas as $re) {
                            $resp[] = $re->Encuestasrespuestas->respuesta;
                        }
                        $respuesta = implode(", ", $resp);
                    }
                    break;
                case 3:
                    $respuestas = Encuestascontestadasrespuestas::active()->with("Encuestasrespuestas")->where("encuestas_contestadas_id", "=", $encuestaContestadaId)->where("encuestas_preguntas_id", "=", $encuestaPreguntaId)->get();
                    if (!empty($respuestas) && count($respuestas) > 0) {
                        $respuestaResultado = Encuestasrespuestas::active()->find($respuestas[0]->encuestas_respuestas_id);
                        $respuesta = $respuestaResultado->respuesta;
                    }
                    break;
            }
        }
        return $respuesta;
    }
}
