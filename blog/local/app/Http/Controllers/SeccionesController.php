<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Catmodulos;

use App\Catsecciones;

use App\Http\Requests;

use Illuminate\Support\Facades\Crypt;

class SeccionesController extends Controller
{

    public function create(Request $request)
    {
        $id_modulo  =    $request->session()->get('idModulo');

        return view('secciones.create', ['id_modulo' => $id_modulo]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre'            =>  'required',
            'url'               =>  'required',
            'cat_catalogos_id'  =>  'required'
        ]);

        $catalogo_id      =   Crypt::decrypt($request->cat_catalogos_id);

        $catSecciones           =   new Catsecciones();
        $catSecciones->nombre   =   $request->nombre;
        $catSecciones->url      =   $request->url;
        $catSecciones->cat_catalogos_id         =   $catalogo_id;
        $catSecciones->save();

        $request->session()->flash('status', 'Sección agregada correctamente!');

        return redirect()->back();
    }

    public function show(Request $request, $id)
    {
        $cat_modulo_id      =   Crypt::decrypt($id);
        $modulo             =   Catmodulos::active()->find($cat_modulo_id);
        $secciones          =   Catsecciones::active()->where('cat_catalogos_id', '=', $cat_modulo_id)->get();
        $request->session()->put('idModulo', $id);

        return view('secciones.index', ['secciones' => $secciones, 'modulo' => $modulo]);
    }

    public function edit($id)
    {
        $seccion_id         =   Crypt::decrypt($id);
        $seccion            =   Catsecciones::active()->find($seccion_id);

        return view('secciones.edit', ['seccion' => $seccion]);
    }

    public function update(Request $request, $id)
    {
        $id             =   Crypt::decrypt($id);
        $seccion        =   Catsecciones::findOrFail($id);

        $this->validate($request, [
            'nombre'            =>  'required',
            'url'               =>  'required',
            'cat_catalogos_id'  =>  'required'
        ]);


        $seccion->nombre            =   $request->nombre;
        $seccion->url               =   $request->url;
        $seccion->save();

        $request->session()->flash('status', 'Sección actualizada correctamente!');

        return redirect()->back();
    }

    public function destroy($id, Request $request )
    {
        $id             =   Crypt::decrypt($id);
        $seccion        =   Catsecciones::find($id);
        if ( $request->ajax() ) {
            $seccion->activo = '0';
            $seccion->save();

            return response(['msg' => 'Sección eliminada', 'status' => 'success', 'cat' => Crypt::encrypt($seccion->cat_catalogos_id)]);
        }
        return response(['msg' => 'Fallo la eliminación de la sección', 'status' => 'failed']);
    }
}