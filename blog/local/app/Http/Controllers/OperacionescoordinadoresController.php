<?php

namespace App\Http\Controllers;

use App\Encuestas;
use App\Operacionescoordinadores;
use App\Usuarios;
use App\Permisosperfiles;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Crypt;

class OperacionescoordinadoresController extends Controller
{

    public function index()
    {
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
        $encuesta_id    =   Crypt::decrypt($request->encuesta_id);
        $usuario_id     =   Crypt::decrypt($request->usuario_id);

        $this->validate($request, [
            'usuario_id'            => 'required'
        ]);
        
        $encuesta = new Operacionescoordinadores();
        $encuesta->usuario_id       = $usuario_id;
        $encuesta->encuesta_id      = $encuesta_id;
        $encuesta->save();

        return response()->json([
            'success'   =>  true,
            'encuesta'  =>  Crypt::encrypt($encuesta_id),
            'message'   =>  'record updated'
        ], 200);
    }

    public function show($id)
    {

        $encuesta_id            =       Crypt::decrypt($id);
        $encuesta               =       Encuestas::find($encuesta_id);
        $coordinadores          =       array("" => "-- Seleccione --");
        $usuarios_arreglo       =       Usuarios::active()->where('idTipoUsuario', '=', 2)->get();
        $usuarios               =       array();

        $arrayPermisos          =       $this->getPermits();
        $arrayPermisosEncuestadores =   $this->getPermitsEncuestadores();

        foreach($usuarios_arreglo as $ua) {
            $existe          =   Operacionescoordinadores::active()->where('encuesta_id', '=', $encuesta_id)->where('usuario_id', '=', $ua->id)->get();
            if (count($existe) == 0) {
                $coordinadores[Crypt::encrypt($ua->id)] = $ua->nombre." ".$ua->apellidos;
            } else {
                $usuarios[] = array(
                            "id"        =>  $existe[0]->id,
                            "nombre"    =>  $ua->nombre." ".$ua->apellidos
                );
            }
        }
        return view('asignacion.create', ['id' => $encuesta_id, 'coordinadores' => $coordinadores, 'usuarios' => $usuarios, 'encuesta' => $encuesta, 'arrayPermisos' => $arrayPermisos, 'arrayPermisosEncuestadores' => $arrayPermisosEncuestadores]);
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id, Request $request)
    {
        $id_coordinador =   Crypt::decrypt($id);
        $coordinador    =   Operacionescoordinadores::find($id_coordinador);
        $encuesta_id    =   $coordinador->encuesta_id;

        if ( $request->ajax() ) {
            $coordinador->activo = '0';
            $coordinador->save();

            return response()->json([
                'success'   =>  true,
                'encuesta'  =>  Crypt::encrypt($encuesta_id),
                'message'   =>  'record deleted'
            ], 200);
        }
        return response(['msg' => 'Fallo la eliminación del coordinador', 'status' => 'failed']);
    }

    public function loadList($id)
    {
        $encuesta_id            =   Crypt::decrypt($id);
        $usuarios               =   Operacionescoordinadores::with("usuarios")->active()->where('encuesta_id', '=', $encuesta_id)->get();

        return view('asignacion.load', ['id' => $encuesta_id, 'usuarios' => $usuarios]);
    }
    private function getPermits() {
        $idTipoUsuario  =   Auth::user()->idTipoUsuario;
        $seccion        =   15;
        $arrayPermisos  =   array();

        $permisos   =   Permisosperfiles::where('tipousuarios_id', '=', $idTipoUsuario)->where('cat_secciones_id', '=', $seccion)->get();

        if (!empty($permisos) && count($permisos) > 0 ) {
            foreach ($permisos as $p) {
                $arrayPermisos[]    =   $p->cat_permisos_id;
            }
        }
        return $arrayPermisos;
    }
    private function getPermitsEncuestadores() {
        $idTipoUsuario  =   Auth::user()->idTipoUsuario;
        $seccion        =   16;
        $arrayPermisos  =   array();

        $permisos   =   Permisosperfiles::where('tipousuarios_id', '=', $idTipoUsuario)->where('cat_secciones_id', '=', $seccion)->get();

        if (!empty($permisos) && count($permisos) > 0 ) {
            foreach ($permisos as $p) {
                $arrayPermisos[]    =   $p->cat_permisos_id;
            }
        }
        return $arrayPermisos;
    }
}
