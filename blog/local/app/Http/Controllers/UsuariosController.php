<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Usuarios;

use App\Perfiles;
use App\Permisosperfiles;
use Auth;
use App\Http\Requests;
use Illuminate\Support\Facades\Crypt;

class UsuariosController extends Controller
{
    public function index()
    {
        $idTipoUsuario  =   Auth::user()->idTipoUsuario;
        $usuarios   =   Usuarios::with('perfiles')->where('activo', '=', '1')->where('idTipoUsuario', '>=', $idTipoUsuario)->get();
        $arrayPermisos  =   $this->getPermits();

        return view('usuarios.index', ['usuarios' => $usuarios, "arrayPermisos" => $arrayPermisos]);
    }

    public function create()
    {
        $perfiles   =   array("" => "-- Seleccione --");
        $perfil = Perfiles::where('activo', '=', 1)->get();
        foreach ($perfil as $p) {
            $perfiles[$p->id] = $p->nombre;
        }

        return view('usuarios.create', ['perfiles' => $perfiles]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre'        => 'required',
            'email'         => 'email|required',
            'telefono'      => 'required',
            'username'      => 'required|unique:usuarios',
            'password'      => 'required',
            'password_repeat'   => 'required|same:password',
            'idTipoUsuario'     => 'required|not_in:0'
        ]);

        $usuario = new Usuarios;
        $usuario->nombre    = $request->nombre;
        $usuario->apellidos = $request->apellidos;
        $usuario->email     = $request->email;
        $usuario->telefono  = $request->telefono;
        $usuario->username  = $request->username;
        $usuario->password  = sha1($request->password);
        $usuario->idTipoUsuario  = $request->idTipoUsuario;
        $usuario->save();

        $request->session()->flash('status', 'Usuario agregado correctamente!');

        return redirect()->back();
    }

    public function show($id)
    {
        //
    }

    public function changePassword($id)
    {
        $id     =   Crypt::decrypt($id);
        $usuario = Usuarios::findOrFail($id);

        return view('usuarios.change', ['usuario' => $usuario]);
    }

    public function edit($id)
    {
        $id         =   Crypt::decrypt($id);
        $perfiles   =   array("" => "-- Seleccione --");
        $perfil     =   Perfiles::where('activo', '=', 1)->get();

        $arrayPermisos  =   $this->getPermits();

        foreach ($perfil as $p) {
            $perfiles[$p->id] = $p->nombre;
        }

        $usuario = Usuarios::findOrFail($id);

        return view('usuarios.edit', ['usuario' => $usuario, 'perfiles' => $perfiles, 'arrayPermisos' => $arrayPermisos]);
    }

    public function update(Request $request, $id)
    {
        $id     =   Crypt::decrypt($id);
        $usuario =  Usuarios::find($id);

        $this->validate($request, [
            'nombre'        => 'required',
            'email'         => 'email|required',
            'telefono'      => 'required',
            'username'      => 'required|unique:usuarios,username,'.$usuario->id,
            'idTipoUsuario'     => 'required|not_in:0'
        ]);


        $usuario->nombre    = $request->nombre;
        $usuario->apellidos = $request->apellidos;
        $usuario->email     = $request->email;
        $usuario->telefono  = $request->telefono;
        $usuario->username  = $request->username;
        $usuario->idTipoUsuario  = $request->idTipoUsuario;
        $usuario->save();

        $request->session()->flash('status', 'Usuario actualizado correctamente!');

        return redirect()->back();
    }

    public function updPassword(Request $request, $id)
    {
        $id     =   Crypt::decrypt($id);
        $usuario = Usuarios::find($id);

        $this->validate($request, [
            'password'          => 'required',
            'password_repeat'   => 'required|same:password',
        ]);

        $usuario->password    = sha1($request->password);
        $usuario->save();

        $request->session()->flash('status', 'Password actualizado correctamente!');

        return redirect()->back();
    }

    public function destroy($id, Request $request)
    {
        $id     =   Crypt::decrypt($id);
        $usuario = Usuarios::find($id);

        if ( $request->ajax() ) {
            $usuario->activo = '0';
            $usuario->save();

            return response(['msg' => 'Usuario eliminado', 'status' => 'success']);
        }
        return response(['msg' => 'Fallo la eliminación del usuario', 'status' => 'failed']);
    }

    private function getPermits() {
        $idTipoUsuario  =   Auth::user()->idTipoUsuario;
        $seccion        =   5;
        $arrayPermisos  =   array();

        $permisos   =   Permisosperfiles::where('tipousuarios_id', '=', $idTipoUsuario)->where('cat_secciones_id', '=', $seccion)->get();

        if (!empty($permisos) && count($permisos) > 0 ) {
            foreach ($permisos as $p) {
                $arrayPermisos[]    =   $p->cat_permisos_id;
            }
        }
        return $arrayPermisos;
    }
}
