<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use DB;
use App\Permisosmodulos;
use App\Permisosperfiles;
use App\Tiposusuarios;
use App\Http\Requests;

class AuthController extends Controller {
    public function showLogin() {
        if (Auth::check()) {
            return Redirect::to('/');
        }
        return View::make('auth/login');
    }
    public function postLogin(Request $request) {
        $userdata = array(
            'username' => Input::get('username'),
            'password'=> Input::get('password')
        );
        
        if (Auth::attempt($userdata, Input::get('remember-me', 0)))
        {
            $idTipoUsuario  =   Auth::user()->idTipoUsuario;
            $tipoDeUsuario  =   Tiposusuarios::find($idTipoUsuario);

            $modulos        =   Permisosmodulos::with('catModulos')->where('tipousuarios_id', '=', $idTipoUsuario)->where('mostrar', '=', '1')->get();
            foreach ($modulos as $m) {
                $idModulo   =   $m->catModulos->id;
                $secciones[$idModulo]       =       DB::table('permisos_perfiles')
                                                    ->join('cat_secciones', 'permisos_perfiles.cat_secciones_id', '=', 'cat_secciones.id')
                                                    ->select('cat_secciones.nombre', 'cat_secciones.url')
                                                    ->where('cat_secciones.cat_catalogos_id', '=', $idModulo)
                                                    ->where('permisos_perfiles.tipousuarios_id', '=', $idTipoUsuario)
                                                    ->where('permisos_perfiles.cat_permisos_id', '=', '1')
                                                    ->where('cat_secciones.activo', '=', '1')
                                                    ->where('cat_secciones.menu', '=', '1')
                                                    ->get();

            }
            $request->session()->put('tipoDeUsuario', $tipoDeUsuario);
            $request->session()->put('modulosMenu', $modulos);
            $request->session()->put('seccionesMenu', $secciones);
            return Redirect::to('/');
        }
        return Redirect::to('login')
                    ->with('mensaje_error', 'Tus datos son incorrectos')
                    ->withInput()
            ;
    }
    public function logOut()
    {
        Auth::logout();
        return Redirect::to('login')
                    ->with('mensaje_error', 'Tu sesión ha sido cerrada.')
            ;
    }
}