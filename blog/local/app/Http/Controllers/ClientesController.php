<?php

namespace App\Http\Controllers;

use App\Clientes;
use App\Estados;
use Auth;
use App\Permisosperfiles;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Crypt;

class ClientesController extends Controller
{

    public function index()
    {
        $clientes   =   Clientes::with('estados')->where('activo', '=', '1')->get();
        $arrayPermisos  =   $this->getPermits();

        return view('clientes.index', ['clientes' => $clientes, 'arrayPermisos' => $arrayPermisos]);
    }

    public function create()
    {
        $estado     =   array("" => "-- Seleccione --");
        $estados    =   Estados::all();

        foreach ($estados as $e) {
            $estado[$e->id] = $e->estado;
        }
        
        return view('clientes.create', ['estados' => $estado]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre'        => 'required',
            'apellidos'     => 'required',
            'numero_telefonico'     => 'required',
            'email'         => 'email|required',
            'empresa'       => 'required',
            'calle'         => 'required',
            'numero_interior'   => 'required',
            'numero_exterior'   => 'required',
            'colonia'               => 'required',
            'delegacion_municipio'  => 'required',
            'idEstado'              => 'required',
            'codigo_postal'         => 'numeric|required',
            'rfc'           => 'required',
        ]);

        $input = $request->all();

        Clientes::create($input);
        $request->session()->flash('status', 'Cliente agregado correctamente!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $id     =   Crypt::decrypt($id);
        $estado     =   array("" => "-- Seleccione --");
        $estados    =   Estados::all();

        foreach ($estados as $e) {
            $estado[$e->id] = $e->estado;
        }

        $cliente = Clientes::findOrFail($id);

        $arrayPermisos  =   $this->getPermits();

        return view('clientes.edit', ['cliente' => $cliente, 'estados' => $estado, 'arrayPermisos' => $arrayPermisos]);
    }

    public function update(Request $request, $id)
    {
        $id         =   Crypt::decrypt($id);
        $cliente    =   Clientes::find($id);

        $this->validate($request, [
            'nombre'        => 'required',
            'apellidos'     => 'required',
            'numero_telefonico'     => 'required',
            'email'         => 'email|required',
            'empresa'       => 'required',
            'calle'         => 'required',
            'numero_interior'   => 'required',
            'numero_exterior'   => 'required',
            'colonia'               => 'required',
            'delegacion_municipio'  => 'required',
            'idEstado'              => 'required',
            'codigo_postal'         => 'numeric|required',
            'rfc'           => 'required',
        ]);

        $input = $request->all();
        $cliente->fill($input)->save();

        $request->session()->flash('status', 'Cliente actualizado correctamente!');

        return redirect()->back();
    }

    public function destroy($id, Request $request)
    {
        $id         =   Crypt::decrypt($id);
        $cliente    =   Clientes::find($id);

        if ( $request->ajax() ) {
            $cliente->activo = '0';
            $cliente->save();

            return response(['msg' => 'Cliente eliminado', 'status' => 'success']);
        }
        return response(['msg' => 'Fallo la eliminación del cliente', 'status' => 'failed']);
    }

    private function getPermits() {
        $idTipoUsuario  =   Auth::user()->idTipoUsuario;
        $seccion        =   6;
        $arrayPermisos  =   array();

        $permisos   =   Permisosperfiles::where('tipousuarios_id', '=', $idTipoUsuario)->where('cat_secciones_id', '=', $seccion)->get();

        if (!empty($permisos) && count($permisos) > 0 ) {
            foreach ($permisos as $p) {
                $arrayPermisos[]    =   $p->cat_permisos_id;
            }
        }
        return $arrayPermisos;
    }
}
