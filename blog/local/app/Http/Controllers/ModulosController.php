<?php

namespace App\Http\Controllers;

use App\Catmodulos;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Crypt;

class ModulosController extends Controller
{

    public function index()
    {
        $modulos = Catmodulos::active()->get();

        return view('modulos.index', ['modulos' => $modulos]);
    }

    public function create()
    {
        return view('modulos.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'modulo'        =>  'required',
            'icon'          =>  'required'
        ]);
        $input = $request->all();

        Catmodulos::create($input);
        $request->session()->flash('status', 'Módulo agregado correctamente!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $id             =   Crypt::decrypt($id);
        $modulos        =   Catmodulos::findOrFail($id);

        return view('modulos.edit', ['modulos' => $modulos]);
    }

    public function update(Request $request, $id)
    {
        $id             =   Crypt::decrypt($id);
        $catModulos     =   Catmodulos::findOrFail($id);

        $this->validate($request, [
            'modulo'        =>  'required',
            'icon'          =>  'required'
        ]);

        $modulo         =   $request->modulo;

        $catModulos->modulo     =   $modulo;
        $catModulos->save();

        $request->session()->flash('status', 'Módulo actualizado correctamente!');

        return redirect()->back();
    }

    public function destroy($id, Request $request)
    {
        $id         =   Crypt::decrypt($id);
        $modulo     =   Catmodulos::find($id);

        if ( $request->ajax() ) {
            $modulo->activo = '0';
            $modulo->save();

            return response(['msg' => 'Módulo eliminado', 'status' => 'success']);
        }
        return response(['msg' => 'Fallo la eliminación del módulo', 'status' => 'failed']);
    }
}
