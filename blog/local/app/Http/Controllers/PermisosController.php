<?php

namespace App\Http\Controllers;

use App\Catmodulos;
use App\Catsecciones;
use App\Catpermisos;
use App\Perfiles;
use App\Tiposusuarios;
use App\Permisosperfiles;
use App\Permisosmodulos;
use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Crypt;

class PermisosController extends Controller
{

    public function index()
    {
        $modulo     =   array("" => "-- Seleccione --");
        $modulos    =   Catmodulos::active()->get();
        foreach ($modulos as $m) {
            $modulo[Crypt::encrypt($m->id)]   =   $m->modulo;
        }
        return view('permisos.index', ['modulo' => $modulo]);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {

    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id, Request $request)
    {

    }

    public function loadSecciones($id) {
        $modulo_id      =   Crypt::decrypt($id);
        $seccion        =   array("" => "-- Seleccione --");
        $secciones      =   Catsecciones::active()->where('cat_catalogos_id', '=', $modulo_id)->get();
        foreach ($secciones as $s) {
            $seccion[Crypt::encrypt($s->id)]   =   $s->nombre;
        }
        
        return view('permisos.secciones', ['seccion' => $seccion]);
    }

    public function loadPerfiles($id) {
        $seccion_id         =   Crypt::decrypt($id);
        $permisos           =   Catpermisos::active()->get();
        $perfiles           =   Tiposusuarios::active()->get();
        $permisos_perfiles  =   array();

        foreach ($perfiles as $p) {
            foreach($permisos as $pe) {
                $result     =   false;
                $result_permisos  =   Permisosperfiles::where("cat_secciones_id", "=", $seccion_id)->where("tipousuarios_id", "=", $p->id)->where("cat_permisos_id", "=", $pe->id)->get();
                if (count($result_permisos) > 0) {
                    $result     =   true;
                }
                $permisos_perfiles[]    =   array(
                    "perfil"    =>  $p->id,
                    "permiso"   =>  $pe->id,
                    "result"    =>  $result
                );
            }
        }

        return view('permisos.perfiles', ['permisos' => $permisos, 'perfiles' => $perfiles, 'permisos_perfiles' => $permisos_perfiles]);
    }

    public function changePermisos(Request $request) {
        $permiso    =   Crypt::decrypt($request->name);
        $perfil     =   Crypt::decrypt($request->value);
        $section    =   Crypt::decrypt($request->section);
        $active     =   $request->active;
        $result     =   false;
        $message    =   "record failed";
        $addPermisos  = false;
        $deletePermisos = 0;

        if($active == 20) {
            $permisosPerfiles = new Permisosperfiles();
            $permisosPerfiles->cat_secciones_id = $section;
            $permisosPerfiles->tipousuarios_id = $perfil;
            $permisosPerfiles->cat_permisos_id = $permiso;
            $addPermisos = $permisosPerfiles->save();
        } else {
            $deletePermisos = Permisosperfiles::where('cat_secciones_id', '=', $section)->where("tipousuarios_id", "=", $perfil)->where("cat_permisos_id", "=", $permiso)->delete();
        }

        $this->verificaPermisosModulos($section, $perfil);

        if ($addPermisos == true || $deletePermisos == 1) {
            $result     =   true;
            $message    =   "record saved";
        }

        return response()->json([
            'success' => $result,
            'message' => $message
        ], 200);
    }

    private function verificaPermisosModulos($section, $perfil) {
        $sectionResult  =   Catsecciones::active()->find($section);
        $catalogo_id    =   $sectionResult->cat_catalogos_id;
        $secciones      =   Catsecciones::active()->where('cat_catalogos_id', '=', $catalogo_id)->get();
        $vista          =   "0";

        if (!empty($secciones) && count($secciones) > 0) {
            foreach ($secciones as $s) {
                $cat_secciones[] = $s->id;
            }
            $buscaPermiso = Permisosperfiles::with('catSecciones')->whereIn('cat_secciones_id', $cat_secciones)->where('tipousuarios_id', '=', $perfil)->where('cat_permisos_id', '=', '1')->get();

            if (!empty($buscaPermiso) && count($buscaPermiso) > 0) {
                foreach ($buscaPermiso as $bp) {
                    if ($bp->catSecciones->menu > 0) {
                        $vista = "1";
                    }
                }
            }

            $buscaPermisoModulo   =   Permisosmodulos::where('cat_modulo_id', '=', $catalogo_id)->where('tipousuarios_id', '=', $perfil)->get();

            if (!empty($buscaPermisoModulo) && count($buscaPermisoModulo) > 0) {
                $idPermisoModulo            =   $buscaPermisoModulo[0]->id;
                $permisosModulos            =   Permisosmodulos::findOrFail($idPermisoModulo);
                $permisosModulos->mostrar   =   $vista;
                $permisosModulos->save();
            } else {
                $permisosModulos            = new Permisosmodulos();
                $permisosModulos->cat_modulo_id     =   $catalogo_id;
                $permisosModulos->tipousuarios_id   =   $perfil;
                $permisosModulos->mostrar           =   $vista;
                $permisosModulos->save();
            }
        }
    }
}
