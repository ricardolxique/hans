<?php

namespace App\Http\Controllers;

class AdminNewsController extends Controller {
    
    public function getList()
    {
        return 'index de noticias';
    }
    
    public function getDetails($id) 
    {
        return 'Lista de noticias. '. $id;
    }
    
    public function edit($id) 
    {
        return 'Accesdiendo a la noticias:  '.$id; 
    }
}