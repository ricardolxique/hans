<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class AdminNewsController extends Controller
{
    public function index() {
        return 'Index de noticias';
    }
    public function show($id) {
        return 'Datos -> '. $id;
    }
}