<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Perfiles;

use App\Http\Requests;

use Illuminate\Support\Facades\Crypt;

class PerfilesController extends Controller
{

    public function index()
    {
        $perfiles = Perfiles::where('activo', '=', 1)->get();

        return view('perfiles.index', ['perfiles' => $perfiles]);
    }

    public function create()
    {
        return view('perfiles.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required',
        ]);

        $input = $request->all();

        Perfiles::create($input);
        $request->session()->flash('status', 'Perfil agregado correctamente!');

        return redirect()->back();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $id     =   Crypt::decrypt($id);
        $perfil = Perfiles::findOrFail($id);

        return view('perfiles.edit', ['perfil' => $perfil]);
    }

    public function update(Request $request, $id)
    {
        $id     =   Crypt::decrypt($id);
        $perfil = Perfiles::findOrFail($id);

        $this->validate($request, [
            'nombre' => 'required',
        ]);

        $input = $request->all();

        $perfil->fill($input)->save();

        $request->session()->flash('status', 'Perfil actualizado correctamente!');

        return redirect()->back();
    }

    public function destroy($id, Request $request )
    {
        $id     =   Crypt::decrypt($id);
        $perfil = Perfiles::find($id);

        if ( $request->ajax() ) {
            $perfil->activo = '0';
            $perfil->save();

            return response(['msg' => 'Perfil eliminado', 'status' => 'success']);
        }
        return response(['msg' => 'Fallo la eliminación del perfil', 'status' => 'failed']);
    }
}