<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class AdministradorMiddleware
{
    
    public function handle($request, Closure $next)
    {
        $idTipoUsuario  =   Auth::user()->idTipoUsuario;
        if ($idTipoUsuario == 1 ) {
            return $next($request);
        }
        return redirect('/');
    }
}
