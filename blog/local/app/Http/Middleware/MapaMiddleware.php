<?php

namespace App\Http\Middleware;
use Auth;
use App\Permisosperfiles;
use Closure;

class MapaMiddleware
{
    public function handle($request, Closure $next)
    {
        $idTipoUsuario  =   Auth::user()->idTipoUsuario;
        $seccion        =   11;

        $resultado      =   Permisosperfiles::where('cat_secciones_id', '=', $seccion)->where('tipousuarios_id', '=', $idTipoUsuario)->where('cat_permisos_id', '=', '1')->get();
        if (count($resultado) > 0 && $resultado != null) {
            return $next($request);
        }
        return redirect('/');
    }
}
