<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catmodulos extends Model
{
    protected $table = 'cat_modulos';

    protected $fillable = [
        'id',
        'modulo',
        'icon'
    ];

    public function scopeActive($query)
    {
        return $query->where('activo', 1);
    }
}
