<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catpermisos extends Model
{
    protected $table = 'cat_permisos';

    protected $fillable = [
        'id',
        'permiso',
        'tipo_permiso'
    ];

    public function scopeActive($query)
    {
        return $query->where('activo', 1);
    }
}
