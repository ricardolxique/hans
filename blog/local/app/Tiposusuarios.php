<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tiposusuarios extends Model
{
    protected $table = 'tipousuarios';

    protected $fillable = [
        'id',
        'nombre',
        'activo'
    ];
    public function scopeActive($query)
    {
        return $query->where('activo', 1);
    }
}
