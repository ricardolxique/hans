<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cattipoestudios extends Model
{
    protected $table = 'cat_tipo_estudios';

    protected $fillable = [
        'id',
        'nombre',
        'activo'
    ];
    public function scopeActive($query)
    {
        return $query->where('activo', 1);
    }
}
