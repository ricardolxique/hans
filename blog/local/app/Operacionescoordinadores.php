<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operacionescoordinadores extends Model
{
    protected $table = 'operaciones_coordinadores';

    protected $fillable = [
        'usuario_id',
        'encuesta_id'
    ];

    public function encuestas()
    {
        return $this->belongsTo(Encuestas::class, 'encuesta_id');
    }

    public function usuarios()
    {
        return $this->belongsTo(Usuarios::class, 'usuario_id');
    }

    public function scopeActive($query)
    {
        return $query->where('activo', 1);
    }
}
