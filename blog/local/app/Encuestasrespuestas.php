<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Encuestasrespuestas extends Model
{
    protected $table = 'encuestarespuestas';

    protected $fillable = [
        'encuesta_pregunta_id',
        'respuesta',
        'orden',
        'salto',
        'activo',
    ];

    public function encuestaPreguntas()
    {
        return $this->belongsTo(Encuestaspreguntas::class, 'encuesta_pregunta_id');
    }
    public function respuestaSaltos()
    {
        return $this->belongsTo(Encuestaspreguntas::class, 'salto');
    }

    public function scopeActive($query)
    {
        return $query->where('activo', 1);
    }
}
