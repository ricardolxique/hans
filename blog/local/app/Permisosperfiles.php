<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permisosperfiles extends Model
{
    protected $table = 'permisos_perfiles';

    protected $fillable = [
        'cat_secciones_id',
        'tipousuarios_id',
        'cat_permisos_id'
    ];

    public function catSecciones()
    {
        return $this->belongsTo(Catsecciones::class, 'cat_secciones_id');
    }
    public function tipoUsuario()
    {
        return $this->belongsTo(Tiposusuarios::class, 'tipousuarios_id');
    }
    public function catPermisos()
    {
        return $this->belongsTo(Catpermisos::class, 'cat_permisos_id');
    }
}
