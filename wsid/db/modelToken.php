<?php
date_default_timezone_set('America/Mexico_City');
require_once("dbConexion.php");

class modelToken {
    private $_conn      =   NULL;
    public  $response   =   array();

    public function __construct() {
        $this->conectDB();
    }
    function __destruct() {
        $this->_conn = null;
    }
    private function conectDB() {
        $db     = new DBConexion();
        $dsn = 'mysql:dbname=' . $db->database . ';host=' . $db->server;
        try {
            $this->_conn = new PDO($dsn, $db->user, $db->passwd);
        } catch (PDOException $e) {
            echo 'Falló la conexión: ' . $e->getMessage();
        }
    }
    public function insertTokenUserSP($parametros) {
        $opt        =   $this->_conn->prepare("CALL appInsertaTokenSP (:token, :idUsername, :fechaCreacion)");
        $opt->bindValue(":idUsername", $parametros["idUsuario"], PDO::PARAM_STR);
        $opt->bindValue(":token", $parametros["token"], PDO::PARAM_STR);
        $opt->bindValue(":fechaCreacion", date("Y-m-d H:i:s"), PDO::PARAM_STR);
        $opt->execute();
        $result = $opt->fetchAll(PDO::FETCH_ASSOC);
        return  $result;
    }
    public function buscaTokenUserSP($parametros) {
        $opt        =   $this->_conn->prepare("CALL appBuscaTokenSP (:idUsername)");
        $opt->bindValue(":idUsername", $parametros["idUsuario"], PDO::PARAM_INT);
        $opt->execute();
        $result = $opt->fetchAll(PDO::FETCH_ASSOC);
        return  $result;
    }
    public function buscaTokenActivoSP($parametros) {
        $opt        =   $this->_conn->prepare("CALL appBuscaTokenActivoSP (:idUsername, :token)");
        $opt->bindValue(":idUsername", $parametros["idUsuario"], PDO::PARAM_INT);
        $opt->bindValue(":token", $parametros["token"], PDO::PARAM_STR);
        $opt->execute();
        $result = $opt->fetchAll(PDO::FETCH_ASSOC);
        return  $result;
    }
    public function actualizaTokenUserSP($parametros) {
        $opt        =   $this->_conn->prepare("CALL appActualizaTokenSP (:idUsername, :fechaModificacion)");
        $opt->bindValue(":idUsername", $parametros["idUsuario"], PDO::PARAM_INT);
        $opt->bindValue(":fechaModificacion", date("Y-m-d H:i:s"), PDO::PARAM_STR);
        $opt->execute();
        $result = $opt->fetchAll(PDO::FETCH_ASSOC);
        return  $result;
    }

}
