<?php
date_default_timezone_set('America/Mexico_City');
require_once("dbConexion.php");

class modelFotos {
    private $_conn      =   NULL;
    public  $response   =   array();

    public function __construct() {
        $this->conectDB();
    }
    function __destruct() {
        $this->_conn = null;
    }
    private function conectDB() {
        $db     = new DBConexion();
        $dsn = 'mysql:dbname=' . $db->database . ';host=' . $db->server;
        try {
            $this->_conn = new PDO($dsn, $db->user, $db->passwd);
        } catch (PDOException $e) {
            echo 'Falló la conexión: ' . $e->getMessage();
        }
    }
    public function insertaFotoEncuesta($parametros) {
        $opt        =   $this->_conn->prepare("CALL appInsertaFotoSP (:idEncuestaContestada, :nombre, :imagen, :fechaCreacion)");
        $opt->bindValue(":idEncuestaContestada", $parametros["idEncuestaContestada"], PDO::PARAM_INT);
        $opt->bindValue(":nombre", $parametros["nombre"], PDO::PARAM_STR);
        $opt->bindValue(":imagen", $parametros["imagen"], PDO::PARAM_STR);
        $opt->bindValue(":fechaCreacion", date("Y-m-d H:i:s"), PDO::PARAM_STR);
        $opt->execute();
        $result = $opt->fetchAll(PDO::FETCH_ASSOC);
        return  $result;
    }
    public function appBuscaFotos($parametros) {
        $opt        =   $this->_conn->prepare("CALL appBuscaFotosSP (:idEncuestaContestada)");
        $opt->bindValue(":idEncuestaContestada", $parametros["idEncuestaContestada"], PDO::PARAM_INT);
        $opt->execute();
        $result = $opt->fetchAll(PDO::FETCH_ASSOC);
        return  $result;
    }
    public function appActualizaFotos($parametros) {
        $opt        =   $this->_conn->prepare("CALL appActualizaFotosSP (:idEncuestaContestada)");
        $opt->bindValue(":idEncuestaContestada", $parametros["idEncuestaContestada"], PDO::PARAM_INT);
        $opt->execute();
        $result = $opt->fetchAll(PDO::FETCH_ASSOC);
        return  $result;
    }
}
