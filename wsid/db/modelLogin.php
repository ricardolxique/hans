<?php
require_once("dbConexion.php");

class modelLogin {
    private $_conn      =   NULL;
    public  $response   =   array();

    public function __construct() {
        $this->conectDB();
    }
    function __destruct() {
        $this->_conn = null;
    }
    private function conectDB() {
        $db     = new DBConexion();
        $dsn = 'mysql:dbname=' . $db->database . ';host=' . $db->server;
        try {
          $this->_conn = new PDO($dsn, $db->user, $db->passwd);
        } catch (PDOException $e) {
         echo 'Falló la conexión: ' . $e->getMessage();
        }
    }
    public function appLoginSP($parametros) {
        $opt        =   $this->_conn->prepare("CALL appLoginUsuarioSP (:username, :password)");
        $opt->bindValue(":username", $parametros["username"], PDO::PARAM_STR);
        $opt->bindValue(":password", $parametros["password"], PDO::PARAM_STR);
        $opt->execute();
        $result = $opt->fetchAll(PDO::FETCH_ASSOC);
        return  $result;
    }
    public function appLogOutSP($parametros) {
        $opt        =   $this->_conn->prepare("CALL appLogOutUsuarioSP (:idUsuario, :token)");
        $opt->bindValue(":idUsuario", $parametros["idUsuario"], PDO::PARAM_STR);
        $opt->bindValue(":token", $parametros["token"], PDO::PARAM_STR);
        $opt->execute();
        $result = $opt->fetchAll(PDO::FETCH_ASSOC);
        return  $result;
    }
}
