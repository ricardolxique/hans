<?php
date_default_timezone_set('America/Mexico_City');
require_once("dbConexion.php");

class modelEncuestas {
    private $_conn      =   NULL;
    public  $response   =   array();

    public function __construct() {
        $this->conectDB();
    }
    function __destruct() {
        $this->_conn = null;
    }
    private function conectDB() {
        $db     = new DBConexion();
        $dsn = 'mysql:dbname=' . $db->database . ';host=' . $db->server;
        try {
            $this->_conn = new PDO($dsn, $db->user, $db->passwd);
        } catch (PDOException $e) {
            echo 'Falló la conexión: ' . $e->getMessage();
        }
    }
    public function appSincronizaEncuestasSP($parametros) {
        $opt        =   $this->_conn->prepare("CALL appSincronizaEncuestasSP (:idUsuario)");
        $opt->bindValue(":idUsuario", $parametros["idUsuario"], PDO::PARAM_INT);
        $opt->execute();
        $result = $opt->fetchAll(PDO::FETCH_ASSOC);
        return  $result;
    }
    public function appSincronizaPreguntasSP($parametros) {
        $opt        =   $this->_conn->prepare("CALL appDescargaPreguntasSP (:idEncuesta)");
        $opt->bindValue(":idEncuesta", $parametros["idEncuesta"], PDO::PARAM_INT);
        $opt->execute();
        $result = $opt->fetchAll(PDO::FETCH_ASSOC);
        return  $result;
    }
    public function appSincronizaRespuestasSP($parametros) {
        $opt        =   $this->_conn->prepare("CALL appDescargaRespuestasSP (:idPregunta)");
        $opt->bindValue(":idPregunta", $parametros["idPregunta"], PDO::PARAM_INT);
        $opt->execute();
        $result = $opt->fetchAll(PDO::FETCH_ASSOC);
        return  $result;
    }
    public function appDescargaAvanceSP($parametros) {
        $opt        =   $this->_conn->prepare("CALL appDescargaAvanceEncuestaSP (:idEncuesta)");
        $opt->bindValue(":idEncuesta", $parametros["idEncuesta"], PDO::PARAM_INT);
        $opt->execute();
        $result = $opt->fetchAll(PDO::FETCH_ASSOC);
        return  $result;
    }
    public function appConsultaOperacionUsuarioId($parametros) {
        $opt        =   $this->_conn->prepare("CALL appConsultaOperacionUsuarioIdSP (:idUsuario, :idEncuesta)");
        $opt->bindValue(":idUsuario", $parametros["idUsuario"], PDO::PARAM_INT);
        $opt->bindValue(":idEncuesta", $parametros["idEncuesta"], PDO::PARAM_INT);
        $opt->execute();
        $result = $opt->fetchAll(PDO::FETCH_ASSOC);
        return  $result;
    }
    public function appConsultaEncuestaContestada($parametros) {
        $opt        =   $this->_conn->prepare("CALL appConsultaEncuestaContestadaSP (:idEncuestaContestada, :idPregunta)");
        $opt->bindValue(":idEncuestaContestada", $parametros["idEncuestaContestada"], PDO::PARAM_INT);
        $opt->bindValue(":idPregunta", $parametros["idPregunta"], PDO::PARAM_INT);
        $opt->execute();
        $result = $opt->fetchAll(PDO::FETCH_ASSOC);
        return  $result;
    }
    public function insertaEncuestaContestada($parametros) {
        $opt        =   $this->_conn->prepare("CALL appInsertaEncuestasContestadasSP (:operacionUsuarioId, :encuestaId, :emitidaMovil, :latitud, :longitud, :inicio_encuesta, :fin_encuesta, :foto, :fechaCreacion, :tokenApp)");
        $opt->bindValue(":operacionUsuarioId", $parametros["operacion_usuario_id"], PDO::PARAM_INT);
        $opt->bindValue(":encuestaId", $parametros["encuestas_id"], PDO::PARAM_INT);
        $opt->bindValue(":fechaCreacion", date("Y-m-d H:i:s"), PDO::PARAM_STR);
        $opt->bindValue(":emitidaMovil", $parametros["emitida_movil"], PDO::PARAM_STR);
        $opt->bindValue(":latitud", $parametros["latitud"], PDO::PARAM_STR);
        $opt->bindValue(":longitud", $parametros["longitud"], PDO::PARAM_STR);
        $opt->bindValue(":inicio_encuesta", $parametros["inicio_encuesta"], PDO::PARAM_STR);
        $opt->bindValue(":fin_encuesta", $parametros["fin_encuesta"], PDO::PARAM_STR);
        $opt->bindValue(":foto", $parametros["foto"], PDO::PARAM_INT);
        $opt->bindValue(":tokenApp", $parametros["tokenApp"], PDO::PARAM_STR);
        $opt->execute();
        $result = $opt->fetchAll(PDO::FETCH_ASSOC);
        return  $result;   
    }
    public function insertaRespuestasEncuestas($parametros) {
        $opt        =   $this->_conn->prepare("CALL appInsertaEncuestaContestadasRespuestasSP (:encuestaContestadaId, :encuestaPreguntaId, :encuestaRespuestaId, :textoAbierta, :fechaCreacion)");
        $opt->bindValue(":encuestaContestadaId", $parametros["idEncuestaContestada"], PDO::PARAM_INT);
        $opt->bindValue(":encuestaPreguntaId", $parametros["preguntaId"], PDO::PARAM_INT);
        $opt->bindValue(":encuestaRespuestaId", $parametros["respuestaId"], PDO::PARAM_INT);
        $opt->bindValue(":textoAbierta", $parametros["texto"], PDO::PARAM_STR);
        $opt->bindValue(":fechaCreacion", date("Y-m-d H:i:s"), PDO::PARAM_STR);
        $opt->execute();
        $result = $opt->fetchAll(PDO::FETCH_ASSOC);
        return  $result;
    }
    public function appConsultaEncuestaContestadaTokenApp($parametros) {
        $opt        =   $this->_conn->prepare("CALL appConsultaEncuestaContestadaTokenAppSP (:operacionUsuarioId, :encuestaId, :tokenApp)");
        $opt->bindValue(":operacionUsuarioId", $parametros["operacionUsuarioId"], PDO::PARAM_INT);
        $opt->bindValue(":encuestaId", $parametros["encuestaId"], PDO::PARAM_INT);
        $opt->bindValue(":tokenApp", $parametros["tokenApp"], PDO::PARAM_STR);
        $opt->execute();
        $result = $opt->fetchAll(PDO::FETCH_ASSOC);
        return  $result;
    }
}
