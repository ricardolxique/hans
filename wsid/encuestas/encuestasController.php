<?php
require("../db/modelEncuestas.php");
require("../db/modelFotos.php");
require_once("../config/configWs.php");
require_once("../utils/tokenController.php");

class encuestasController extends configWs {
    private $_metodo;
    private $_argumentos;

    public function __construct() {
        parent::__construct();
        $this->_encuestasModel          =   new modelEncuestas();
        $this->_fotosModel              =   new modelFotos();
        $this->_token                   =   new tokenController();
    }
    public function procesarLLamada() {
        $rawData        =   file_get_contents("php://input");
        /*
        $nombre_fichero =   'llamada_'.date("Y-m-d_H:i:s").'_.txt'; 
        $myfile         =   fopen($nombre_fichero, "w") or die("Unable to open file!");
        $txt            =   $rawData;
        fwrite($myfile, $txt);
        fclose($myfile);
        */
        if (isset($_REQUEST['url'])) {
            $url = explode('/', trim($_REQUEST['url']));
            $url = array_filter($url);
            $this->_metodo = strtolower(array_shift($url));
            $this->_argumentos = $url;
            $func = $this->_metodo;
            if ((int) method_exists($this, $func) > 0) {
                if (count($this->_argumentos) > 0) {
                    call_user_func_array(array($this, $this->_metodo), $this->_argumentos);
                } else {
                    call_user_func(array($this, $this->_metodo));
                }
            } else
                $this->mostrarRespuesta($this->convertirJson($this->devolverError(0)), 404);
        }
        $this->mostrarRespuesta($this->convertirJson($this->devolverError(0)), 404);
    }
    private function sincronizaEncuestas() {
        if ($_SERVER['REQUEST_METHOD'] != "POST") {
            $this->mostrarRespuesta($this->convertirJson($this->devolverError(1)), 405);
        }
        if (isset($this->datosPeticion['idUsuario'], $this->datosPeticion['token'])) {
            $parametros                 =   array();
            $parametros["idUsuario"]    =   $this->datosPeticion["idUsuario"];
            $parametros["token"]        =   $this->datosPeticion["token"];
            $validaToken                =   $this->_token->buscaTokenActivoSP($parametros);
            if ($validaToken) {
                $encuestasArray = $this->_encuestasModel->appSincronizaEncuestasSP($parametros);
                if (count($encuestasArray) > 0 && is_array($encuestasArray)) {
                    $resultArray       =   array();
                    foreach ($encuestasArray as $key => $ea) {
                        $resultArray["encuestas"][$key]["idEncuesta"]      =   $ea["idEncuesta"];
                        $resultArray["encuestas"][$key]["nombreEncuesta"]  =   utf8_encode($ea["nombre"]);
                        $resultArray["encuestas"][$key]["cliente"]         =   utf8_encode($ea["cliente"]);
                        $resultArray["encuestas"][$key]["preguntas"]       =       $this->obtienePreguntas($ea["idEncuesta"]);
                    }

                    $response               =   array();
                    $response["result"]     =   true;
                    $response["data"]       =   $resultArray;

                    $this->mostrarRespuesta($this->convertirJson($response), 200);
                } else {
                    $this->mostrarRespuesta($this->convertirJson($this->devolverError(6)), 400);
                }
            } else {
                $this->mostrarRespuesta($this->convertirJson($this->devolverError(5)), 400);
            }
        } else {
            $this->mostrarRespuesta($this->convertirJson($this->devolverError(3)), 400);
        }
    }
    private function insertaEncuestas() {
        if ($_SERVER['REQUEST_METHOD'] != "POST") {
            $this->mostrarRespuesta($this->convertirJson($this->devolverError(1)), 405);
        }
        $rawData        =   file_get_contents("php://input");
        $this->datosPeticion            =   json_decode($rawData);

        if (isset($this->datosPeticion->idUsuario, $this->datosPeticion->token, $this->datosPeticion->idEncuesta, $this->datosPeticion->encuesta)) {
            $parametros                 =   array();
            $parametros["idUsuario"]    =   $this->datosPeticion->idUsuario;
            $parametros["token"]        =   $this->datosPeticion->token;
            $parametros["idEncuesta"]   =   $this->datosPeticion->idEncuesta;
            $parametros["encuesta"]     =   $this->datosPeticion->encuesta;
            $respuestasArray            =   $this->datosPeticion->respuestas;
            $fotosArray                 =   $this->datosPeticion->encuesta->fotosArchivo;

            $validaToken                =   $this->_token->buscaTokenActivoSP($parametros);
            
            if ($validaToken) {
                $operacionUsuario       =   $this->_encuestasModel->appConsultaOperacionUsuarioId($parametros);
                if (count($operacionUsuario) > 0 && is_array($operacionUsuario)) {
                    $idOperacionUsuario                                     =   $operacionUsuario[0]["operacionUsuarioId"];
                    // Busca el tokenApp para saber si contamos con alguna inserción para esa encuesta.
                    $parametrosTokenApp         =   array();
                    $parametrosTokenApp["operacionUsuarioId"]   =   $idOperacionUsuario;
                    $parametrosTokenApp["encuestaId"]           =   $parametros["idEncuesta"];
                    $parametrosTokenApp["tokenApp"]             =   $parametros["encuesta"]->tokenApp;
                    $encuestaContestada         =   $this->_encuestasModel->appConsultaEncuestaContestadaTokenApp($parametrosTokenApp);

                    if (count($encuestaContestada) > 0 && $encuestaContestada != null) {
                        $idEncuestaContestada   =   $encuestaContestada[0]["idEncuestaContestada"];
                    } else {
                        $parametrosEncuestaContestada                           =   array();
                        $parametrosEncuestaContestada["operacion_usuario_id"]   =   $idOperacionUsuario;
                        $parametrosEncuestaContestada["encuestas_id"]           =   $parametros["idEncuesta"];
                        $parametrosEncuestaContestada["inicio_encuesta"]        =   $parametros["encuesta"]->inicio_encuesta;
                        $parametrosEncuestaContestada["fin_encuesta"]           =   $parametros["encuesta"]->fin_encuesta;
                        $parametrosEncuestaContestada["emitida_movil"]          =   $parametros["encuesta"]->emitida_movil;
                        $parametrosEncuestaContestada["latitud"]                =   $parametros["encuesta"]->latitud;
                        $parametrosEncuestaContestada["longitud"]               =   $parametros["encuesta"]->longitud;
                        $parametrosEncuestaContestada["foto"]                   =   $parametros["encuesta"]->foto;  
                        $parametrosEncuestaContestada["tokenApp"]               =   $parametros["encuesta"]->tokenApp;
                        $resultadoEncuestaContestada                            =   $this->_encuestasModel->insertaEncuestaContestada($parametrosEncuestaContestada);
                        
                        if (count($resultadoEncuestaContestada) > 0 && is_array($resultadoEncuestaContestada)) {
                            $idEncuestaContestada       =   $resultadoEncuestaContestada[0]["idOperacion"];
                        }
                    }           
                    if ($idEncuestaContestada > 0) {
                        $inserts        =   0;
                        $totalInserts   =   0;
                        $parametrosRespuestas       = array();
                        $parametrosRespuestas["idEncuestaContestada"] = $idEncuestaContestada;
                        if (is_array($respuestasArray) && count($respuestasArray) > 0) {    
                            foreach ($respuestasArray as $key => $ra) {
                                $parametrosBuscaEncuestaContestada  =   array();
                                $parametrosBuscaEncuestaContestada["idEncuestaContestada"]  =   $idEncuestaContestada;
                                $parametrosBuscaEncuestaContestada["idPregunta"]            =   $ra->idPregunta;

                                $buscaEncuestaContestada            =   $this->_encuestasModel->appConsultaEncuestaContestada($parametrosBuscaEncuestaContestada);

                                if (count($buscaEncuestaContestada) > 0 && $buscaEncuestaContestada != null) { // Si ya se inserto esa pregunta solo la contabiliza
                                    $inserts++;
                                } else { // Si no existe, inserta la pregunta.
                                    $respuestas     =   explode(",", $ra->idRespuesta);
                                    if (count($respuestas) > 0) {
                                        foreach ($respuestas as $k => $respuesta) {
                                            $parametrosRespuestas["preguntaId"]     =   $ra->idPregunta;
                                            $parametrosRespuestas["respuestaId"]    =   $respuesta;
                                            $parametrosRespuestas["texto"]          =   $ra->textoAbierta;
                                            $resultadoInsertaRespuestas             =   $this->_encuestasModel->insertaRespuestasEncuestas($parametrosRespuestas);
                                            if (count($resultadoInsertaRespuestas) > 0 && is_array($resultadoInsertaRespuestas)) {
                                                if ($resultadoInsertaRespuestas[0]["idEncuestaContestadaRespuesta"] > 0) {
                                                    $inserts++;
                                                }
                                            } else {
                                                $noInserts++;
                                            }
                                            $totalInserts++;
                                        }
                                    } 
                                }
                            }
                            $totalFotos         =   count($fotosArray);
                            $insertaFoto        =   0;
                            if (is_array($fotosArray) && count($fotosArray) > 0) {
                                // Busca si ya cuenta con fotos insertadas
                                $parametrosBuscaFotos               =   array();
                                $parametrosBuscaFotos["idEncuestaContestada"]   =   $idEncuestaContestada;
                                $buscaFotosEncuestas                            =   $this->_fotosModel->appBuscaFotos($parametrosBuscaFotos);

                                if (count($buscaFotosEncuestas) > 0 && $buscaFotosEncuestas != null) {
                                    // Si hay fotos en esa encuesta contestada las elimina logicamente
                                    $actualizaFotosEncuestas                            =   $this->_fotosModel->appActualizaFotos($parametrosBuscaFotos);
                                } 
                                foreach($fotosArray as $foto) {
                                    $parametrosFoto                             =   array();
                                    $parametrosFoto["idEncuestaContestada"]     =   $idEncuestaContestada;
                                    $parametrosFoto["nombre"]                   =   $foto->nombreFoto;
                                    $parametrosFoto["imagen"]                   =   $this->procesaImagen($foto->foto);
                                    $fotoInsertada                              =   $this->_fotosModel->insertaFotoEncuesta($parametrosFoto);

                                    if (is_array($fotoInsertada) && count($fotoInsertada)) {
                                        $idFotoInsertada    =   $fotoInsertada[0]["idFoto"];
                                        if ($idFotoInsertada > 0) {
                                            $insertaFoto++;
                                        }
                                    }
                                }
                            }
                            if ($totalInserts == $inserts) {
                                if ($totalFotos == $insertaFoto) {
                                    $response                               =   array();
                                    $response["result"]                     =   true;
                                    $response["mensaje"]                    =   "Encuestas y fotos insertadas correctamente.";
                                    $this->mostrarRespuesta($this->convertirJson($response), 200);
                                } else {
                                    $this->mostrarRespuesta($this->convertirJson($this->devolverError(11)), 400);
                                }
                            } else {
                                $this->mostrarRespuesta($this->convertirJson($this->devolverError(10)), 400);
                            }   
                        } else {
                            $this->mostrarRespuesta($this->convertirJson($this->devolverError(12)), 400);
                        }
                    } else {
                        $this->mostrarRespuesta($this->convertirJson($this->devolverError(9)), 400);
                    }
                } else {
                    $this->mostrarRespuesta($this->convertirJson($this->devolverError(8)), 400);
                }
            } else {
                $this->mostrarRespuesta($this->convertirJson($this->devolverError(5)), 400);
            }
        } else {
            $this->mostrarRespuesta($this->convertirJson($this->devolverError(3)), 400);
        }
    }
    private function procesaImagen($foto) {
        $dir_to_save    =   "../../dev/surgical/";
        $archivo        =   date("YmdHis").rand(1,9999).".jpg";
        if (!is_dir($dir_to_save)) {
          mkdir($dir_to_save);
        }
        $archivoRuta    =   $dir_to_save.$archivo;
        file_put_contents($archivoRuta, base64_decode($foto));
        return $archivo;
    }
    private function obtienePreguntas($idEncuesta) {
        $preguntasResult         =   array();
        if($idEncuesta != null) {
            $parametros         =   array();
            $parametros["idEncuesta"]   =   $idEncuesta;
            $preguntasArray     =   $this->_encuestasModel->appSincronizaPreguntasSP($parametros);
            if (count($preguntasArray) > 0 && is_array($preguntasArray)) {
                foreach ($preguntasArray as $pa) {
                    $preguntasResult[]     =   array(
                        "idPregunta"        =>  $pa["idPregunta"],
                        "tipoPregunta"      =>  $pa["idTipoPregunta"],
                        "pregunta"          =>  utf8_encode($pa["pregunta"]),
                        "limite_caracteres" =>  $pa["limiteCaracteres"],
                        "orden"             =>  $pa["orden"],
                        "salto"             =>  (strlen($pa["salto"]) == 0 ) ? 0 : $pa["salto"],
                        "respuestas"        =>  $this->obtieneRespuestas($pa["idPregunta"])
                    );
                }
            }
        }
        return $preguntasResult;
    }
    private function obtieneRespuestas($idPregunta) {
        $respuestasResult         =   array();
        if($idPregunta != null) {
            $parametros                 =   array();
            $parametros["idPregunta"]   =   $idPregunta;
            $respuestasArray            =   $this->_encuestasModel->appSincronizaRespuestasSP($parametros);
            if (count($respuestasArray) > 0 && is_array($respuestasArray)) {
                foreach ($respuestasArray as $ra) {
                    $respuestasResult[]   = array(
                                "idRespuesta"   =>  $ra["idRespuesta"],
                                "respuesta"     =>  utf8_encode($ra["respuesta"]),
                                "orden"         =>  $ra["orden"],
                                "salto"         =>  (strlen($ra["salto"]) == 0 ) ? 0 : $ra["salto"] ,
                    );
                }
            }
        }
        if (count($respuestasResult) == 0) {
            $respuestasResult   =   array();
        }
        return $respuestasResult;
    }
}
$api = new encuestasController();
$api->procesarLLamada();
