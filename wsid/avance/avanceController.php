<?php
require("../db/modelEncuestas.php");
require_once("../config/configWs.php");
require_once("../utils/tokenController.php");

class encuestasController extends configWs {
    private $_metodo;
    private $_argumentos;

    public function __construct() {
        parent::__construct();
        $this->_encuestasModel          =   new modelEncuestas();
        $this->_token                   =   new tokenController();
    }
    public function procesarLLamada() {
        if (isset($_REQUEST['url'])) {
            $url = explode('/', trim($_REQUEST['url']));
            $url = array_filter($url);
            $this->_metodo = strtolower(array_shift($url));
            $this->_argumentos = $url;
            $func = $this->_metodo;
            if ((int) method_exists($this, $func) > 0) {
                if (count($this->_argumentos) > 0) {
                    call_user_func_array(array($this, $this->_metodo), $this->_argumentos);
                } else {
                    call_user_func(array($this, $this->_metodo));
                }
            } else
                $this->mostrarRespuesta($this->convertirJson($this->devolverError(0)), 404);
        }
        $this->mostrarRespuesta($this->convertirJson($this->devolverError(0)), 404);
    }
    private function avanceEncuestas() {
        if ($_SERVER['REQUEST_METHOD'] != "POST") {
            $this->mostrarRespuesta($this->convertirJson($this->devolverError(1)), 405);
        }
        if (isset($this->datosPeticion['idEncuesta'], $this->datosPeticion['token'])) {
            $parametros                 =   array();
            $parametros["idEncuesta"]   =   $this->datosPeticion["idEncuesta"];
            $parametros["idUsuario"]    =   $this->datosPeticion["idUsuario"];
            $parametros["token"]        =   $this->datosPeticion["token"];

            $validaToken                =   $this->_token->buscaTokenActivoSP($parametros);
            if ($validaToken) {
                $encuestasArray = $this->_encuestasModel->appDescargaAvanceSP($parametros);
                if (count($encuestasArray) > 0 && is_array($encuestasArray)) {
                    $resultArray       =   array();
                    foreach ($encuestasArray as $key => $ea) {
                        $resultArray["avance"][$key]["idUsuario"]              =   $ea["idUsuario"];
                        $resultArray["avance"][$key]["nombreUsuario"]          =   utf8_encode($ea["nombreUsuario"]);
                        $resultArray["avance"][$key]["encuestasAsignadas"]     =   $ea["encuestasAsignadas"];
                        $resultArray["avance"][$key]["encuestasRealizadas"]    =   ($ea["totalEncuestas"] == null) ? 0 : $ea["totalEncuestas"] ;
                    }

                    $response               =   array();
                    $response["result"]     =   true;
                    $response["data"]       =   $resultArray;
                    $this->mostrarRespuesta($this->convertirJson($response), 200);
                } else {
                    $this->mostrarRespuesta($this->convertirJson($this->devolverError(7)), 400);
                }
            } else {
                $this->mostrarRespuesta($this->convertirJson($this->devolverError(5)), 400);
            }
        } else {
            $this->mostrarRespuesta($this->convertirJson($this->devolverError(3)), 400);
        }
    }
}
$api = new encuestasController();
$api->procesarLLamada();
