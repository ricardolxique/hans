<?php
require_once("../db/modelToken.php");
require_once("secureDataEncrypt.php");

class tokenController {
    private $_db;
    private $_encrypt;

    public function __construct() {
        $this->_db          =   new modelToken();
        $this->_encrypt     =   new secureDataPast();
    }

    public function generaToken($idUsuario) {
        $response                   =   new stdClass();
        $parametros                 =   array();
        $response->result           =   false;
        $parametros["idUsuario"]    =   $idUsuario;
        $parametros["token"]        =   $this->_encrypt->encryptad($idUsuario.".".date("Y-m-d H:i:s"));

        $buscaToken                 =   $this->_db->buscaTokenUserSP($parametros);

        if (count($buscaToken) > 0 && is_array($buscaToken)) {
            $actualizaToken         =   $this->_db->actualizaTokenUserSP($parametros);
        }
        $result                     =   $this->_db->insertTokenUserSP($parametros);

        if ($result[0]["tokenId"] > 0) {
            $response->result       =   true;
            $response->token        =   $parametros["token"];
        }

        return $response;
    }
    public function buscaTokenActivoSP($parametros) {
        $response                   =   false;
        $buscaToken                 =   $this->_db->buscaTokenActivoSP($parametros);

        if (count($buscaToken) > 0 && $buscaToken != null) {
            $response   =   true;
        }
        return $response;
    }
}