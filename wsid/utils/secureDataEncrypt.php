<?php

class secureDataPast {
    private $_key = "Intenta entrar a la puerta de los 7 sellos de la bruja";
    private $_alg = MCRYPT_BLOWFISH;
    private $_mod = MCRYPT_MODE_CBC;

    public function __construct() {
        $this->_iv          =   mcrypt_create_iv(mcrypt_get_iv_size($this->_alg, $this->_mod), MCRYPT_DEV_URANDOM);
    }

    public function encryptad($str) {
        $encrypted_data = mcrypt_encrypt($this->_alg, $this->_key, $str, $this->_mod, $this->_iv);
        $cifr = base64_encode($encrypted_data);
        return $cifr;
    }
    public function desencryptad($str) {
        $encrypted_data = base64_decode($str);
        $result = mcrypt_decrypt($this->_alg, $this->_key, $encrypted_data, $this->_mod, $this->_iv);
        return $result;
    }
}