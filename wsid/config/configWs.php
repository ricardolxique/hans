<?php
require_once("../config/Rest.php");

class configWs extends Rest {
   private $_metodo;
   private $_argumentos;

    public function __construct() {
       parent::__construct();
    }
    public function procesarLLamada() {
      if (isset($_REQUEST['url'])) {
          $url = explode('/', trim($_REQUEST['url']));
          $url = array_filter($url);
          $this->_metodo = strtolower(array_shift($url));
          $this->_argumentos = $url;
          $func = $this->_metodo;
          if ((int) method_exists($this, $func) > 0) {
              if (count($this->_argumentos) > 0) {
                  call_user_func_array(array($this, $this->_metodo), $this->_argumentos);
                } else {
                  call_user_func(array($this, $this->_metodo));
                }
          } else
              $this->mostrarRespuesta($this->convertirJson($this->devolverError(0)), 404);
      }
       $this->mostrarRespuesta($this->convertirJson($this->devolverError(0)), 404);
    }
    protected function convertirJson($data) {
        //return json_encode($data, JSON_UNESCAPED_UNICODE);
        return json_encode($data);
    }
    protected function devolverError($id) {
        $errores = array(
            array('result' => false, "errorCode" => 100, "mensaje" => "Petición no encontrada."),
            array('result' => false, "errorCode" => 101, "mensaje" => "Petición no aceptada."),
            array('result' => false, "errorCode" => 102, "mensaje" => "Petición sin contenido."),
            array('result' => false, "errorCode" => 103, "mensaje" => "Error faltan datos."),
            array('result' => false, "errorCode" => 104, "mensaje" => "Login incorrecto."),
            array('result' => false, "errorCode" => 105, "mensaje" => "Token incorrecto o invalidado."),
            array('result' => false, "errorCode" => 106, "mensaje" => "No hay encuestas asignadas para este usuario."),
            array('result' => false, "errorCode" => 107, "mensaje" => "No contamos resultados de avance para esta encuesta."),
            array('result' => false, "errorCode" => 108, "mensaje" => "Este usuario no puede insertar resultados, consulte a su coordinador."),
            array('result' => false, "errorCode" => 109, "mensaje" => "Al parecer hemos tenido un inconveniente, intentelo mas tarde."),
            array('result' => false, "errorCode" => 110, "mensaje" => "No se pudieron insertar las encuestas, intentelo nuevamente."),
            array('result' => false, "errorCode" => 111, "mensaje" => "No se pudieron insertar las fotos, intentelo nuevamente."),
            array('result' => false, "errorCode" => 111, "mensaje" => "Esta encuesta no contiene respuestas."),
        );
        return $errores[$id];
    }
}
