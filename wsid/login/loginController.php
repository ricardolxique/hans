 <?php
require("../db/modelLogin.php");
require_once("../config/configWs.php");
require_once("../utils/tokenController.php");

class loginController extends configWs {
    private $_metodo;
    private $_argumentos;
    private $_db;

    public function __construct() {
        parent::__construct();
        $this->_loginModel          =   new modelLogin();
        $this->_token               =   new tokenController();
    }
    public function procesarLLamada() {
      if (isset($_REQUEST['url'])) {
          $url = explode('/', trim($_REQUEST['url']));
          $url = array_filter($url);
          $this->_metodo = strtolower(array_shift($url));
          $this->_argumentos = $url;
          $func = $this->_metodo;
          if ((int) method_exists($this, $func) > 0) {
              if (count($this->_argumentos) > 0) {
                  call_user_func_array(array($this, $this->_metodo), $this->_argumentos);
                } else {
                  call_user_func(array($this, $this->_metodo));
                }
          } else
              $this->mostrarRespuesta($this->convertirJson($this->devolverError(0)), 404);
      }
       $this->mostrarRespuesta($this->convertirJson($this->devolverError(0)), 404);
    }
    private function appLogin() {
        if ($_SERVER['REQUEST_METHOD'] != "POST") {
          $this->mostrarRespuesta($this->convertirJson($this->devolverError(1)), 405);
        }
        if (isset($this->datosPeticion['username'], $this->datosPeticion['password'])) {
            $parametros                 =   array();
            $parametros["username"]     =   $this->datosPeticion["username"];
            $parametros["password"]     =   $this->datosPeticion["password"];

            $result                     =   $this->_loginModel->appLoginSP($parametros);
            
            if (count($result) > 0 && is_array($result)) {
                $idUsuario              =   $result[0]["id"];
                $idTipoUsuario          =   $result[0]["idTipoUsuario"];
                $generaToken            =   $this->_token->generaToken($idUsuario);
                
                if ($generaToken->result) {
                    $response               =   array();
                    $response["result"]                 =   true;
                    $response["data"]["idUsuario"]      =   $idUsuario;
                    $response["data"]["token"]          =   $generaToken->token;
                    $response["data"]["idTipoUsuario"]  =   $idTipoUsuario;
                    $response["mensaje"]                =   "Usuario logueado correctamente.";
                    $this->mostrarRespuesta($this->convertirJson($response), 200);
                } else {
                    $this->mostrarRespuesta($this->convertirJson($this->devolverError(5)), 400);
                }
            } else {
                $this->mostrarRespuesta($this->convertirJson($this->devolverError(4)), 400);
            }
        } else {
         $this->mostrarRespuesta($this->convertirJson($this->devolverError(3)), 400);
        }
    }
    private function appLogOut() {
        if ($_SERVER['REQUEST_METHOD'] != "POST") {
            $this->mostrarRespuesta($this->convertirJson($this->devolverError(1)), 405);
        }
        if (isset($this->datosPeticion['idUsuario'], $this->datosPeticion['token'])) {
            $response                   =   array();
            $parametros                 =   array();
            $parametros["idUsuario"]    =   $this->datosPeticion["idUsuario"];
            $parametros["token"]        =   $this->datosPeticion["token"];

            $result                     =   $this->_loginModel->appLogOutSP($parametros);

            if (count($result) > 0 && is_array($result)) {
                $idToken              =   $result[0]["id"];
                $response["result"]                 =   true;
                $response["data"]["mensaje"]        =   "Usuario desconectado correctamente.";
                $this->mostrarRespuesta($this->convertirJson($response), 200);
            } else {
                $this->mostrarRespuesta($this->convertirJson($this->devolverError(3)), 400);
            }
        } else {
            
        }
    }
 }
 $api = new loginController();
 $api->procesarLLamada();
